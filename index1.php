<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<div class="cat-slider-block">
	<div class="container">
		<div class="row">
			<p class="cattitle">Каталог</h2>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

				<div class="row">
					<div class="col-lg-12">
						<div class="row links-3">
							<div class="col-lg-12">
								<div class="row cat-list">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><h2><a href="/catalog/zhenskiy-trikotazh/">Женский трикотаж</a></h2></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><h2><a href="/catalog/muzhskoy-trikotazh/">Мужской трикотаж</a></h2></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><h2><a href="/catalog/detskiy-trikotazh/">Детский трикотаж</a></h2></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="catalog-slider-block">
							<?
							$APPLICATION->IncludeComponent(
								"bitrix:news.list", 
								"slider-catalog", 
								array(
									"IBLOCK_TYPE" => "1c_catalog",
									"IBLOCK_ID" => "4",
									"NEWS_COUNT" => "10",
									"SORT_BY1" => "ACTIVE_FROM",
									"SORT_ORDER1" => "DESC",
									"SORT_BY2" => "SORT",
									"SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "arrFilterTrends",
									"FIELD_CODE" => array(
										0 => "NAME",
										1 => "DETAIL_PICTURE",
										2 => "",
									),
									"PROPERTY_CODE" => array(
										0 => "BRAND_REF",
										1 => "BREND",
										2 => "LINK",
										3 => "",
									),
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_SHADOW" => "Y",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"CACHE_TYPE" => "N",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"PREVIEW_TRUNCATE_LEN" => "120",
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"DISPLAY_PANEL" => "N",
									"SET_TITLE" => "N",
									"SET_STATUS_404" => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"ADD_SECTIONS_CHAIN" => "N",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => "",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
									"PAGER_SHOW_ALL" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"COMPONENT_TEMPLATE" => "slider-catalog",
									"SET_BROWSER_TITLE" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_LAST_MODIFIED" => "N",
									"INCLUDE_SUBSECTIONS" => "N",
									"DISPLAY_DATE" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "N",
									"MEDIA_PROPERTY" => "",
									"SEARCH_PAGE" => "/search/",
									"USE_RATING" => "N",
									"USE_SHARE" => "N",
									"PAGER_TITLE" => "Новости",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"SHOW_404" => "N",
									"MESSAGE_404" => ""
								),
								false
							);
							?>
						</div>	
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>