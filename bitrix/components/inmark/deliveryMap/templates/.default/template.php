<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
$APPLICATION->AddHeadScript("https://api-maps.yandex.ru/2.1/?lang=ru-RU");
?>


<script type="text/javascript">
    ymaps.ready(init);

	function init () {
		var myMap = new ymaps.Map('map',
			{ 	center: [<?echo $arParams[MAP_CENTER];?>],
			 	zoom: <?echo $arParams[MAP_ZOOM];?>,
			 	controls: ['zoomControl', 'searchControl', 'typeSelector',  'fullscreenControl'] },
			{ searchControlProvider: 'yandex#search' }),
			objectManager1 = new ymaps.ObjectManager({
				// Чтобы метки начали кластеризоваться, выставляем опцию.
				clusterize: true,
				// ObjectManager принимает те же опции, что и кластеризатор.
				gridSize: 60
			}),
			objectManager2 = new ymaps.ObjectManager({
				// Чтобы метки начали кластеризоваться, выставляем опцию.
				clusterize: true,
				// ObjectManager принимает те же опции, что и кластеризатор.
				gridSize: 32
			});
		// Чтобы задать опции одиночным объектам и кластерам,
		// обратимся к дочерним коллекциям ObjectManager.

		objectManager1.objects.options.set( {
				iconLayout: 'default#image',
				iconImageHref: '/bitrix/images/icons/logo-boxberry.png',
				iconImageSize: [30, 30],
				iconImageOffset: [-15, -15]
		});
		objectManager1.clusters.options.set('clusterIconColor', '#ee1750');
		myMap.geoObjects.add(objectManager1);


		objectManager2.objects.options.set( {
				iconLayout: 'default#image',
				iconImageHref: '/bitrix/images/icons/grastin_logo.png',
				iconImageSize: [30, 30],
				iconImageOffset: [-15, -15]
		});
		objectManager2.clusters.options.set('clusterIconColor', '#292929');
		myMap.geoObjects.add(objectManager2);


			points1 = {
				"type": "FeatureCollection",
				"features": [

					<?echo $arResult["POINTS"][0];?>
				]
			};
			myMap.geoObjects.add(objectManager1);
			objectManager1.add(points1);

			points2 = {
				"type": "FeatureCollection",
				"features": [

					<?echo $arResult["POINTS"][1];?>
				]
			};
			myMap.geoObjects.add(objectManager2);
			objectManager2.add(points2);

	}
</script>

<div id="map" style="width:<?echo $arParams[MAP_WIDTH];?>;height:<?echo $arParams[MAP_HEIGHT];?>"></div>

