<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"CACHE_TIME" => array(
			"DEFAULT" => 120
			),
		"MAP_WIDTH" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAP_WIDTH"),
			"TYPE" => "STRING",
			"DEFAULT" => "100%",
			),
		"MAP_HEIGHT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAP_HEIGHT"),
			"TYPE" => "STRING",
			"DEFAULT" => "400px",
			),
		"MAP_CENTER" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAP_CENTER"),
			"TYPE" => "STRING",
			"DEFAULT" => "55.76, 37.64",
			),
		"MAP_ZOOM" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAP_ZOOM"),
			"TYPE" => "STRING",
			"DEFAULT" => "4",
			),
	),
	
);
?>