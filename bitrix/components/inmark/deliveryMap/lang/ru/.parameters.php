<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS['MAP_WIDTH'] = "Ширина карты";
$MESS['MAP_HEIGHT'] = "Высота карты";
$MESS['MAP_CENTER'] = "Центр карты";
$MESS['MAP_ZOOM'] = "Приближение карты";

?>