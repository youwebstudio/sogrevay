<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"CACHE_TIME" => array(
			"DEFAULT" => 120
			),
		"INTRO_TEXT" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("INTRO_TEXT"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("INTRO_TEXT_DEFAULT"),
			),
		"DATA_URL" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DATA_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			),
		"DATA_IMAGE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DATA_IMAGE"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			),
		"DATA_SERVICES" => array(
			"PARENT" => "BASE", 
			"NAME" => GetMessage("DATA_SERVICES"),
			"TYPE" => "LIST",
			"VALUES" => Array(
				"vkontakte" => "vkontake",
				"facebook" => "facebook",
				"odnoklassniki" => "odnoklassniki",
				"moimir" => "moimir",
				"twitter" => "twitter",
				"lj" => "lj",
				"viber" => "viber",
				"whatsapp" => "whatsapp"
				),
			"MULTIPLE" => "Y",
			"DEFAULT" => Array("vkontakte", "facebook", "odnoklassniki", "twitter")
			),
		"DATA_TYPE" => array(
			"PARENT" => "BASE", 
			"NAME" => GetMessage("DATA_TYPES"),
			"TYPE" => "LIST",
			"VALUES" => Array(
				"icons" => "Только иконки",
				"counters" => "Счетчики",
				"menu" => "Иконки и меню",
				"small" => "Маленького размера"
				),
			"DEFAULT" => Array(
				"counters"
				)
			)
	),
);
?>