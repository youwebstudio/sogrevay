<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$MESS['DATA_URL'] = "URL страницы";
$MESS['DATA_IMAGE'] = "URL изображения";
$MESS['DATA_SERVICES'] = "Перечень социальных сетей";
$MESS['DATA_TYPES'] = "Тип отображения";
$MESS['INTRO_TEXT'] = "Текст";
$MESS['INTRO_TEXT_DEFAULT'] = "Хочу поделиться:";
?>