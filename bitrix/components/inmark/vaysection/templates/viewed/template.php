<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




<?
//prn(time());
//prn($arParams);
//echo $arResult["NAV_STRING"];




?>



<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick-theme.css"/>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick.min.js"></script>



<?if(count($arResult["ITEMS"]) > 0):?>
<p class="viewed-title">Вы смотрели</p>


	<?$num = 0;?>
	<div class="row">
		<div class="col-xs-12">
			<div class="viewed-wrapper">
				<?foreach($arResult["ITEMS"] as $GOOD_ID => $arGood):?>
					<?$num++;?>
					<div class="good-item">
						<div class="good-title">
							<a title="<?=$arResult["ITEMS"][$GOOD_ID]["NAME"]?>" href="<?=$arResult["ITEMS"][$GOOD_ID]["DETAIL_PAGE_URL"]?>">Арт.<?=$arResult["ITEMS"][$GOOD_ID]["PROPERTY_CML2_ARTICLE_VALUE"]?></a>
						</div>
						<div class="good-offers">
						
						
						
							<?$color_num = 0;?>
							<?$active = false;?>
							<?foreach($arResult["ITEMS"][$GOOD_ID]["COLORS"] as $arColor):?> 
							<?
								$this_active = false;
								$color_num++;
								if(!$active && $arColor["PICTURE"]["SRC"] != "default") 
								{
									$active = true;
									$this_active = true;
								}
								if(!$active && ($color_num == count($arResult["ITEMS"][$GOOD_ID]["COLORS"])))
								{
									$this_active = true;
								}
								?>
								<div class="good-offer-item<?if($this_active):?> active<?endif?>">
									<div class="img-block">
										<?if($arColor["PICTURE"]["SRC"] != "default"):?>
											<a title="<?=$arResult["ITEMS"][$GOOD_ID]["NAME"]?>" href="<?=$arResult["ITEMS"][$GOOD_ID]["DETAIL_PAGE_URL"]?>"><img style="width:100px; height:150px;" src="<?=$arColor["PICTURE"]["SRC"]?>"></a>
										<?else:?>
											<a title="<?=$arResult["ITEMS"][$GOOD_ID]["NAME"]?>" href="<?=$arResult["ITEMS"][$GOOD_ID]["DETAIL_PAGE_URL"]?>"><img style="width:100px; height:150px;" src="/upload/no_female_big.gif"></a>
										<?endif?>
									</div>
									<?foreach($arColor["SIZES"] as $arSize):?> 
										<?foreach($arSize as $arItem):?> 
											<div class="offer-price">
												<?if(isset($arItem["DISCOUNT_PRICE"])):?>
													<s><?=$arItem["PRICE"]?></s> <?=$arItem["DISCOUNT_PRICE"]?> руб.
												<?else:?>
													<?=$arItem["PRICE"]?> руб.
												<?endif?>
											</div>
											<?break;?>
										<?endforeach?>	
										<?break;?>
									<?endforeach?>
								</div>
							<?endforeach?>
						
						
						
						
						</div>
					</div>
				<?endforeach?>
			</div>
		</div>
	</div>
	
	
	
<?endif?>


