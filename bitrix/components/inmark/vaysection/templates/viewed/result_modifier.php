<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

//prn($GLOBALS[$arParams["FILTER_NAME"]]);
$arOrder = $GLOBALS[$arParams["FILTER_NAME"]]["ID"];
$arOrder = array_flip(array_reverse($arOrder));

$arNew = Array();
foreach($arResult["ITEMS"] as $arItem)
{
	$arNew[$arOrder[$arItem["ID"]]] = $arItem;
}
ksort($arNew);
$arResult["ITEMS"] = $arNew;
//prn($arNew[0]);
?>