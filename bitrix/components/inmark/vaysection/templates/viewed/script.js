$(document).ready( function() {
	$("div.viewed-wrapper").slick({
		/*lazyLoad: 'ondemand',*/
		fade: false,
		arrows: true,
		slidesToShow: 8,
		slidesToScroll: 8,
		infinite: false,
		responsive: [
			{
			  breakpoint: 1200,
			  settings: {
				slidesToShow: 6,
				slidesToScroll: 6,
				infinite: true,
				dots: true
			  }
			},
			{
			  breakpoint: 992,
			  settings: {
				slidesToShow: 5,
				slidesToScroll: 5
			  }
			},
			{
			  breakpoint: 680,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 4
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			}
		  ]
		});
});