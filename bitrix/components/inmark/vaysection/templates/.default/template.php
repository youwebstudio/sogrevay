<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




<?
prn(time());
//prn($arParams);
//echo $arResult["NAV_STRING"];
?>



<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick-theme.css"/>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick.min.js"></script>



<?if(count($arResult["ITEMS"]) > 0):?>



	<?if($arParams["DISPLAY_TOP_PAGER"] == "Y"):?>
		<?=$arResult["NAV_STRING"];?>
	<?endif?>


	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<div class="div-sort">
				<span class="sort-title">Сортировать:</span>
				<span class="sort-var">по цене</span>
				<a title="По возрастанию цены"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_MIN_PRICE")&&($arParams["ELEMENT_SORT_ORDER"] == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
				<a title="По убыванию цены"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_MIN_PRICE")&&($arParams["ELEMENT_SORT_ORDER"] == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a>
				<span class="sort-var">по артикулу</span>
				<a title="По возрастанию артикула"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_CML2_ARTICLE")&&($arParams["ELEMENT_SORT_ORDER"] == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
				<a title="По убыванию артикула"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_CML2_ARTICLE")&&($arParams["ELEMENT_SORT_ORDER"] == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a> 
			</div>
		</div>
		<?/*?>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="div-count">
				<span class="count-title">Показывать:</span>
				<a <?if($arParams["PAGE_ELEMENT_COUNT"]==20):?>class="active" <?endif?> class="<?=$sortedByCount20?>" href="<?=$APPLICATION->GetCurPageParam("", array("count"))?>">20</a>
				<a <?if($arParams["PAGE_ELEMENT_COUNT"]==40):?>class="active" <?endif?> href="<?=$APPLICATION->GetCurPageParam("count=40", array("count"))?>">40</a>
				<a <?if($arParams["PAGE_ELEMENT_COUNT"]==60):?>class="active" <?endif?> href="<?=$APPLICATION->GetCurPageParam("count=60", array("count"))?>">60</a>
			</div>
		</div>
		<?*/?>
	</div>
	
	
	


	<?$num = 0;?>
	<div class="row">
	<?foreach($arResult["ITEMS"] as $GOOD_ID => $arGood):?>
		<?$num++;?>
		<?//prn($num %4);?>
		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
			<div class="good-item">
				<div class="good-title">
					<a title="<?=$arResult["ITEMS"][$GOOD_ID]["NAME"]?>" href="<?=$arResult["ITEMS"][$GOOD_ID]["DETAIL_PAGE_URL"]?>"><?=$arResult["ITEMS"][$GOOD_ID]["NAME"]?></a>
				</div>
				<div class="good-offers">
					
					
					<?/*foreach($arResult["ITEMS"][$GOOD_ID]["COLORS"] as $arColor):?> 
					<div class="good-offer-item<?if($this_active):?> active<?endif?>">
						<?if($arColor["PICTURE"]["SRC"] != "default"):?>
						<img style="width:200px; height:300px; margin:0 auto;" src="<?=$arColor["PICTURE"]["SRC"]?>">
						<?else:?>
						<img style="width:200px; height:300px; margin:0 auto;" src="/upload/no_female_big.gif">
						<?endif?>
						<div class="color-name"><?=$arColor["NAME"]?></div>
						<ul class="size-list">
						<?foreach($arColor["SIZES"] as $arSize):?> 
							<?foreach($arSize as $arOffer):?> 
								<li><?=$arOffer["SIZE"]?></li>
							<?endforeach?>
						<?endforeach?>
						</ul>
					</div>
					<?endforeach*/?>
				
					<?$color_num = 0;?>
					<?$active = false;?>
					<?foreach($arResult["ITEMS"][$GOOD_ID]["COLORS"] as $arColor):?> 
					<?
						$this_active = false;
						$color_num++;
						if(!$active && $arColor["PICTURE"]["SRC"] != "default") 
						{
							$active = true;
							$this_active = true;
						}
						if(!$active && ($color_num == count($arResult["ITEMS"][$GOOD_ID]["COLORS"])))
						{
							$this_active = true;
						}
					?>
					<div class="good-offer-item<?if($this_active):?> active<?endif?>">
						<div class="img-block">
							<div class="color-count"><?=$color_num?>/<?=count($arResult["ITEMS"][$GOOD_ID]["COLORS"])?></div>
							<?if($arColor["PICTURE"]["SRC"] != "default"):?>
								<img style="width:200px; height:300px;" src="<?=$arColor["PICTURE"]["SRC"]?>">
							<?else:?>
								<img style="width:200px; height:300px;" src="/upload/no_female_big.gif">
							<?endif?>
						</div>
						<?foreach($arColor["SIZES"] as $arSize):?> 
							<?foreach($arSize as $arItem):?> 
								<div class="offer-price">
									<?if(isset($arItem["DISCOUNT_PRICE"])):?>
										<s><?=$arItem["PRICE"]?></s> <?=$arItem["DISCOUNT_PRICE"]?> руб.
									<?else:?>
										<?=$arItem["PRICE"]?> руб.
									<?endif?>
								</div>
								<?break;?>
							<?endforeach?>	
							<?break;?>
						<?endforeach?>
						<div class="color-name"><?=$arColor["NAME"]?></div>
						<ul class="size-list">
						<?foreach($arColor["SIZES"] as $arSize):?> 
							<?foreach($arSize as $arOffer):?> 
								<li class="<?=$arOffer["ID"]?>"><?=$arOffer["SIZE"]?></li>
							<?endforeach?>
						<?endforeach?>
						</ul>
					</div>
					<?endforeach?>
					
					
					
				</div>
			</div>
		</div>
	<?endforeach?>
	</div>


<?endif?>


