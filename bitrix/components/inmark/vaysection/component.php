<?
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
//use Bitrix\Currency\CurrencyTable;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_ID"] = (int)$arParams["IBLOCK_ID"];
$arParams["SECTION_ID"] = (int)$arParams["SECTION_ID"];

/*
if($arParams["SECTION_ID"] > 0 && $arParams["SECTION_ID"]."" != $arParams["~SECTION_ID"])
{
	if (CModule::IncludeModule("iblock"))
	{
		\Bitrix\Iblock\Component\Tools::process404(
			trim($arParams["MESSAGE_404"]) ?: GetMessage("CATALOG_SECTION_NOT_FOUND")
			,true
			,$arParams["SET_STATUS_404"] === "Y"
			,$arParams["SHOW_404"] === "Y"
			,$arParams["FILE_404"]
		);
	}
	return;
}
*/

// ELEMENT_SORT_TYPE

if(empty($arParams["FILTER_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global ${$arParams["FILTER_NAME"]};
	$arrFilter = ${$arParams["FILTER_NAME"]};
	/*
	if(!is_array($arrFilter))
		$arrFilter = array();
	elseif (isset($arrFilter['FACET_OPTIONS']) && count($arrFilter) == 1)
		unset($arrFilter['FACET_OPTIONS']);
		*/
}

if (empty($arParams["PAGER_PARAMS_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PAGER_PARAMS_NAME"]))
{
	$pagerParameters = array();
}
else
{
	$pagerParameters = $GLOBALS[$arParams["PAGER_PARAMS_NAME"]];
	if (!is_array($pagerParameters))
		$pagerParameters = array();
}


/*
$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["SET_BROWSER_TITLE"] = (isset($arParams["SET_BROWSER_TITLE"]) && $arParams["SET_BROWSER_TITLE"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_KEYWORDS"] = (isset($arParams["SET_META_KEYWORDS"]) && $arParams["SET_META_KEYWORDS"] === 'N' ? 'N' : 'Y');
$arParams["SET_META_DESCRIPTION"] = (isset($arParams["SET_META_DESCRIPTION"]) && $arParams["SET_META_DESCRIPTION"] === 'N' ? 'N' : 'Y');
$arParams["ADD_SECTIONS_CHAIN"] = (isset($arParams["ADD_SECTIONS_CHAIN"]) && $arParams["ADD_SECTIONS_CHAIN"]==="Y"); //Turn off by default
*/

$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
if(($arParams["PAGE_ELEMENT_COUNT"] == 20)||($arParams["PAGE_ELEMENT_COUNT"] == 40)||($arParams["PAGE_ELEMENT_COUNT"] == 60))
	$arParams["PAGE_ELEMENT_COUNT"] = $arParams["PAGE_ELEMENT_COUNT"];
else
	$arParams["PAGE_ELEMENT_COUNT"] = 20;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] == "Y";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = "el"; $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = "N"; //$arParams["PAGER_SHOW_ALL"] == "Y";

if ($arParams['DISPLAY_TOP_PAGER'] || $arParams['DISPLAY_BOTTOM_PAGER'])
{
	$arNavParams = array(
		"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["PAGE_ELEMENT_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
	$arParams['CACHE_GROUPS'] = 'Y';

$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

//$arParams["CACHE_TIME"] = 10;
//prn($arParams);

/*************************************************************************
			Processing of the Buy link
*************************************************************************/
/*

*/




//prn($arNavigation);
//prn($pagerParameters);
//prn($arrFilter);

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($arrFilter, ($arParams["CACHE_GROUPS"] === "N" ? false : $USER->GetGroups()), $arNavigation, $pagerParameters)))
{

	//prn($arrFilter);
	//prn($arNavParams);
	
	$arUserGroups = $USER->GetUserGroupArray();
	
	$arGoods = Array();
	$arPictures = Array();
	$arSort = Array();
	$arFilter = $arrFilter;
	$arSelect = Array(
		"ID",
		"NAME",
		"PROPERTY_MIN_PRICE",
		"DETAIL_PAGE_URL",
		"DETAIL_PICTURE",
		"PROPERTY_MORE_PHOTO",
		"PROPERTY_MORE_PHOTO_DESCRIPTION",
		"PROPERTY_CML2_ARTICLE"
	);
	
	$dbRes = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
	$dbRes->NavStart($arParams["PAGE_ELEMENT_COUNT"]);
	$dbRes->NavPrint();
	while($arRes = $dbRes->GetNext())
	{
		$arGoods[$arRes["ID"]] = $arRes;
		if($arRes["DETAIL_PICTURE"] > 0) $arPictures[] = $arRes["DETAIL_PICTURE"];
		$arPictures = array_merge($arPictures, $arRes["PROPERTY_MORE_PHOTO_VALUE"]);
		
		$arGoods[$arRes["ID"]]["DISCOUNTS"] = CCatalogDiscount::GetDiscountByProduct(
			$arRes["ID"],
			$arUserGroups,
			"N",
			1,
			SITE_ID
		);
		
		//prn($arGoods[$arRes["ID"]]);
		//prn(count($arGoods[$arRes["ID"]]["DISCOUNTS"])); 
	}
	
	$arResult["NAV_STRING"] = $dbRes->GetPageNavStringEx(
		$navComponentObject,
		"Товары",
		"vay"
		);
	
	
	
	
	$arPicts = Array();
	$dbFileRes = CFile::GetList(Array(), array("@ID" => implode(",", $arPictures)));
	while($arFileRes = $dbFileRes->GetNext())
	{
		$arPicts[$arFileRes["ID"]] = $arFileRes;
		$t = explode(" цвет ", $arFileRes["DESCRIPTION"]);
		if(count($t) > 1)
			$color = trim($t[1]);
		else 
			$color = trim($t[0]);
		$arPicts[$arFileRes["ID"]]["COLOR"] = trim(strtoupper($color));
	}
	
	
	
	
	$arOffers = Array();
	$arFilterOffers = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_OFFERS_ID"], 
		"ACTIVE" => "Y", 
		"PROPERTY_CML2_LINK" => array_keys($arGoods)
		);
	$arSelectOffers = Array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"ACTIVE",
		"CATALOG_GROUP_1",
		"PROPERTY_CML2_LINK",
		"PROPERTY_COLOR",
		"PROPERTY_SIZE"
		);
	$dbOfferRes = CIBlockElement::GetList(Array(), $arFilterOffers, false, false, $arSelectOffers);
	while($arOfferRes = $dbOfferRes->GetNext())
	{
		$arOffer = Array();
		$arOffer = Array(
			"ID" => $arOfferRes["ID"],
			"NAME" => $arOfferRes["NAME"],
			"ACTIVE" => $arOfferRes["ACTIVE"],
			"QUANTITY" => $arOfferRes["CATALOG_QUANTITY"],
			"PRICE" => $arOfferRes["CATALOG_PRICE_1"],
			"CURRENCY" => $arOfferRes["CATALOG_CURRENCY_1"],
			"SIZE" => $arOfferRes["PROPERTY_SIZE_VALUE"],
			"COLOR" => $arOfferRes["PROPERTY_COLOR_VALUE"]
			);
		$offer_color = trim(strtoupper($arOfferRes["PROPERTY_COLOR_VALUE"]));
		$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["NAME"] = $arOfferRes["PROPERTY_COLOR_VALUE"];
		if(empty($arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]][$offer_color]["PICTURE"]))
		{
			$found = false;
			for($i = 0; $i < count($arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"]); $i++)
			{
				if($offer_color == $arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["COLOR"])
				{
					$WIDTH = 200;
					$HEIGHT = 300;
					$file = CFile::ResizeImageGet($arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["ID"], array('width' => $WIDTH, 'height' => $HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["PICTURE"] = Array(
						"ID" => $arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["ID"],
						//"SRC" => "/upload/".$arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["SUBDIR"]."/".$arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["FILE_NAME"],
						"SRC" => $file["src"],
						"DESCRIPTION" => $arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["DESCRIPTION"]
						);
					$found = true;
					break;
				}
			}
			if(!$found)
				$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["PICTURE"] = Array("SRC" => "default");
		}
		
		//$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]][strtoupper($arOfferRes["PROPERTY_COLOR_VALUE"])]["PICTURE"] = Array();
		$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["SIZES"][$arOfferRes["PROPERTY_SIZE_VALUE"]][$arOfferRes["ID"]] = $arOffer;
		
		if(count($arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["DISCOUNTS"]) > 0)
		{
			$discountPrice = CCatalogProduct::CountPriceWithDiscount(
				$arOffer["PRICE"],
				"RUB",
				$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["DISCOUNTS"]
			);	
			$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["SIZES"][$arOfferRes["PROPERTY_SIZE_VALUE"]][$arOfferRes["ID"]]["DISCOUNT_PRICE"] =  $discountPrice;
			$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["SIZES"][$arOfferRes["PROPERTY_SIZE_VALUE"]][$arOfferRes["ID"]]["DISCOUNT_PROC"] =  "";
			//prn($discountPrice);
		}
		
		

	}
	
	
	
	$arResult["ITEMS"] = $arGoods;
	
	
	
	
	
	$this->SetResultCacheKeys(
		array(
		//"ID",
		//"NAV_CACHED_DATA",
		//$arParams["META_KEYWORDS"],
		//$arParams["META_DESCRIPTION"],
		//$arParams["BROWSER_TITLE"],
		//$arParams["BACKGROUND_IMAGE"],
		//"NAME",
		//"PATH",
		//"IBLOCK_SECTION_ID",
		//"IPROPERTY_VALUES",
		//"ITEMS_TIMESTAMP_X",
		//'BACKGROUND_IMAGE',
		//'USE_CATALOG_BUTTONS'
		)
	);
	//die("t4");
	$this->IncludeComponentTemplate();

	//if ($bCatalog && $boolNeedCatalogCache)
	//{
	//	CCatalogDiscount::ClearDiscountCache(array(
	//		'PRODUCT' => true,
	//		'SECTIONS' => true,
	//		'PROPERTIES' => true
	//	));
	//}
	
}






/*
$arTitleOptions = null;
if($USER->IsAuthorized())
{
	if(
		$APPLICATION->GetShowIncludeAreas()
		|| (is_object($INTRANET_TOOLBAR) && $arParams["INTRANET_TOOLBAR"]!=="N")
		|| $arParams["SET_TITLE"]
		|| isset($arResult[$arParams["BROWSER_TITLE"]])
	)
	{
		if(Loader::includeModule("iblock"))
		{
			$UrlDeleteSectionButton = "";
			if($arResult["IBLOCK_SECTION_ID"] > 0)
			{
				$rsSection = CIBlockSection::GetList(
					array(),
					array("=ID" => $arResult["IBLOCK_SECTION_ID"]),
					false,
					array("SECTION_PAGE_URL")
				);
				$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"]);
				$arSection = $rsSection->GetNext();
				$UrlDeleteSectionButton = $arSection["SECTION_PAGE_URL"];
			}

			if(empty($UrlDeleteSectionButton))
			{
				$url_template = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "LIST_PAGE_URL");
				$arIBlock = CIBlock::GetArrayByID($arParams["IBLOCK_ID"]);
				$arIBlock["IBLOCK_CODE"] = $arIBlock["CODE"];
				$UrlDeleteSectionButton = CIBlock::ReplaceDetailURL($url_template, $arIBlock, true, false);
			}

			$arReturnUrl = array(
				"add_section" => (
					strlen($arParams["SECTION_URL"])?
					$arParams["SECTION_URL"]:
					CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_PAGE_URL")
				),
				"delete_section" => $UrlDeleteSectionButton,
			);
			$buttonParams = array(
				'RETURN_URL' => $arReturnUrl,
				'CATALOG' => true
			);
			if (isset($arResult['USE_CATALOG_BUTTONS']))
				$buttonParams['USE_CATALOG_BUTTONS'] = $arResult['USE_CATALOG_BUTTONS'];
			$arButtons = CIBlock::GetPanelButtons(
				$arParams["IBLOCK_ID"],
				0,
				$arResult["ID"],
				$buttonParams
			);
			unset($buttonParams);

			if($APPLICATION->GetShowIncludeAreas())
				$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

			if(
				is_array($arButtons["intranet"])
				&& is_object($INTRANET_TOOLBAR)
				&& $arParams["INTRANET_TOOLBAR"]!=="N"
			)
			{
				$APPLICATION->AddHeadScript('/bitrix/js/main/utils.js');
				foreach($arButtons["intranet"] as $arButton)
					$INTRANET_TOOLBAR->AddButton($arButton);
			}

			if($arParams["SET_TITLE"] || isset($arResult[$arParams["BROWSER_TITLE"]]))
			{
				$arTitleOptions = array(
					'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_section"]["ACTION"],
					'PUBLIC_EDIT_LINK' => $arButtons["edit"]["edit_section"]["ACTION"],
					'COMPONENT_NAME' => $this->GetName(),
				);
			}
		}
	}
}
*/



//$this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);




if($arParams["SET_TITLE"])
{
	if ($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
		$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
	elseif(isset($arResult["NAME"]))
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
}

if ($arParams["SET_BROWSER_TITLE"] === 'Y')
{
	$browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arResult, $arParams["BROWSER_TITLE"]
		,$arResult["IPROPERTY_VALUES"], "SECTION_META_TITLE"
	);
	if (is_array($browserTitle))
		$APPLICATION->SetPageProperty("title", implode(" ", $browserTitle), $arTitleOptions);
	elseif ($browserTitle != "")
		$APPLICATION->SetPageProperty("title", $browserTitle, $arTitleOptions);
}

if ($arParams["SET_META_KEYWORDS"] === 'Y')
{
	$metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arResult, $arParams["META_KEYWORDS"]
		,$arResult["IPROPERTY_VALUES"], "SECTION_META_KEYWORDS"
	);
	if (is_array($metaKeywords))
		$APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
	elseif ($metaKeywords != "")
		$APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);
}

if ($arParams["SET_META_DESCRIPTION"] === 'Y')
{
	$metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arResult, $arParams["META_DESCRIPTION"]
		,$arResult["IPROPERTY_VALUES"], "SECTION_META_DESCRIPTION"
	);
	if (is_array($metaDescription))
		$APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
	elseif ($metaDescription != "")
		$APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);
}


if ($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["PATH"]) && is_array($arResult["PATH"]))
{
	foreach($arResult["PATH"] as $arPath)
	{
		if ($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
			$APPLICATION->AddChainItem($arPath["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arPath["~SECTION_PAGE_URL"]);
		else
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
	}
}

/*
if ($arParams["SET_LAST_MODIFIED"] && $arResult["ITEMS_TIMESTAMP_X"])
{
	Context::getCurrent()->getResponse()->setLastModified($arResult["ITEMS_TIMESTAMP_X"]);
}
*/

return $arResult["ID"];

