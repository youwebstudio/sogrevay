<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
		</div><!--//workarea-->

	<!--noindex-->
	<footer class="bx-footer">

	
		<div class="container footer-menu-block" style="">
		
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<span class="h3">Интернет-магазин<i class="fa fa-angle-down"></i></span>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
							"ROOT_MENU_TYPE" => "bottom",
							"MAX_LEVEL" => "1",
							"MENU_CACHE_TYPE" => "A",
							"CACHE_SELECTED_ITEMS" => "N",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
						),
						false
					);?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<span class="h3">Женский трикотаж<i class="fa fa-angle-down"></i></span>
					<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"bottom5", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "bottom5",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID" => "46",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "TEXT"
	),
	false
);?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<span class="h3">Мужской трикотаж<i class="fa fa-angle-down"></i></span>
					<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"bottom5", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "bottom5",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID" => "149",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "TEXT"
	),
	false
);?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<span class="h3">Детский трикотаж<i class="fa fa-angle-down"></i></span>
					<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"bottom5", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "bottom5",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID" => "52",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "TEXT"
	),
	false
);?>
				</div>
			</div>
		
		</div>
		
		<div class="container footer-info-block">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					<div class="footer-social-block">
						<span>Мы в соцсетях</span>
						<a rel="nofollow" target="_blank" href="https://vk.com/feminatrade"><i class="fa fa-vk"></i></a>
						<a rel="nofollow" target="_blank" href="https://www.facebook.com/Sogrevayru-106296856475317/"><i class="fa fa-facebook"></i></a>
						<a rel="nofollow" target="_blank" href="https://twitter.com/sogrevay_ru"><i class="fa fa-twitter"></i></a>
						<a rel="nofollow" target="_blank" href="https://ok.ru/feminatrade"><i class="fa fa-odnoklassniki"></i></a>
						<a rel="nofollow" target="_blank" href="https://www.instagram.com/sogrevay.ru/"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					<div class="footer-pay-block">
						<span>Мы принимаем оплату</span>
						<div></div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					<div class="footer-search-block">
						
						<?$APPLICATION->IncludeComponent(
							"bitrix:search.form",
							"footer",
							Array(
								"COMPONENT_TEMPLATE" => ".default",
								"PAGE" => "#SITE_DIR#search/",
								"USE_SUGGEST" => "N"
							)
						);?>
						
						<br/>
						
							
						<!-- Yandex.Metrika informer -->
						<?if($USER->isAdmin()):?>
						<a href="https://metrika.yandex.ru/stat/?id=38114470&amp;from=informer"
						target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/38114470/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
						style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:38114470,lang:'ru'});return false}catch(e){}" /></a>
						<?endif?>
						<!-- /Yandex.Metrika informer -->

						<!-- Yandex.Metrika counter -->
						<script type="text/javascript">
							(function (d, w, c) {
								(w[c] = w[c] || []).push(function() {
									try {
										w.yaCounter38114470 = new Ya.Metrika({
											id:38114470,
											clickmap:true,
											trackLinks:true,
											accurateTrackBounce:true,
											webvisor:true
										});
									} catch(e) { }
								});

								var n = d.getElementsByTagName("script")[0],
									s = d.createElement("script"),
									f = function () { n.parentNode.insertBefore(s, n); };
								s.type = "text/javascript";
								s.async = true;
								s.src = "https://mc.yandex.ru/metrika/watch.js";

								if (w.opera == "[object Opera]") {
									d.addEventListener("DOMContentLoaded", f, false);
								} else { f(); }
							})(document, window, "yandex_metrika_callbacks");
						</script>
						<noscript><div><img src="https://mc.yandex.ru/watch/38114470" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
						<!-- /Yandex.Metrika counter -->
						

						
						
						<br/><br/>
						
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
					<div class="footer-phones-block">
						<span>8 (800) 770 05 33</span><br>
						<span>8 (495) 502 90 99</span><br>
						<span>8 (905) 544 24 14</span>
						<?/*if(($_SESSION["GEO_DATA"]["CITY"] == "Москва")|| ($_SESSION["GEO_DATA"]["REGION"] == "Московская область")):?>
							<span>8 (495) 502 90 99</span><br/><span>8 (905) 544 24 14</span>
						<?else:?>
							<span>8 (800) 770 05 33</span><br/><span>8 (905) 544 24 14</span>
						<?endif*/?>
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="footer-copyright-block">
						<span>2016-<?=date("Y")?> &copy; Интернет-магазин трикотажа<br/>ООО &laquo;КУПИТРИКОТАЖ&raquo;</span>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="footer-addr-block">
						<span>105275, г.Москва, ул.Уткина, д.48</span>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="footer-logo-block text-right">
						<a class="btnUp" href="javascript:void(0)" data-role="eshopUpButton"><i class="fa fa-caret-up"></i> <?=GetMessage("FOOTER_UP_BUTTON")?></a>
					</div>
				</div>
			</div>
			
		</div>
	
		<?/*?><div class="container footer-vay-block"></div><?*/?>
		
		
		<?/*?>
		<div class="bx-footer-section container bx-center-section">
			<div class="col-sm-5 col-md-3 col-md-push-6">
				<h4 class="bx-block-title"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/about_title.php"), false);?></h4>
				<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
						"ROOT_MENU_TYPE" => "bottom",
						"MAX_LEVEL" => "1",
						"MENU_CACHE_TYPE" => "A",
						"CACHE_SELECTED_ITEMS" => "N",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(
						),
					),
					false
				);?>
			</div>
			<div class="col-sm-5 col-md-3">
				<h4 class="bx-block-title"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/catalog_title.php"), false);?></h4>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"bottom_menu", 
					array(
						"ROOT_MENU_TYPE" => "left",
						"MENU_CACHE_TYPE" => "Y",
						"MENU_CACHE_TIME" => "300",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(
						),
						"CACHE_SELECTED_ITEMS" => "N",
						"MAX_LEVEL" => "2",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "bottom_menu",
						"CHILD_MENU_TYPE" => "left"
					),
					false
				);?>
			</div>
			<div class="col-sm-5 col-md-3 col-md-push-3">
				<div style="padding: 20px;background:#eaeaeb">
					<?/*$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/sender.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "html",
						),
						false,
						Array('HIDE_ICONS' => 'Y')
					);*/?>
		<?/*?>
				</div>
				<div id="bx-composite-banner" style="padding-top: 20px"></div>
			</div>
			<div class="col-sm-5 col-md-3 col-md-pull-9">
				<div class="bx-inclogofooter">
					<div class="bx-inclogofooter-block">
						<a class="bx-inclogofooter-logo" href="<?=SITE_DIR?>">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo_mobile.php"), false);?>
						</a>
					</div>
					<div class="bx-inclogofooter-block">
						<div class="bx-inclogofooter-tel"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/telephone.php"), false);?></div>
						<div class="bx-inclogofooter-worktime"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/schedule.php"), false);?></div>
					</div>
				</div>
			</div>
		</div>
		<?*/?>
		<?/*?>
		<div class="bx-footer-bottomline">
			<div class="bx-footer-section container">
				<div class="col-sm-6"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/copyright.php"), false);?></div>
				<div class="col-sm-6 bx-up"><a href="javascript:void(0)" data-role="eshopUpButton"><i class="fa fa-caret-up"></i> <?=GetMessage("FOOTER_UP_BUTTON")?></a></div>
			</div>
		</div>
		<?*/?>


	</footer>
		
		
		
	<div class="col-xs-12 hidden-lg hidden-md hidden-sm">
		<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "fixed", array(
				"PATH_TO_BASKET" => SITE_DIR."personal/korzina/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" =>"Y",
				"POSITION_HORIZONTAL" => "center",
				"POSITION_VERTICAL" => "bottom",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login/",
				"PATH_TO_PROFILE" => SITE_DIR."personal/"
			),
			false,
			array()
		);?>
	</div>
	
	<!--/noindex-->	
		
</div> <!-- //bx-wrapper -->

<a class="callback1 fancybox-iframe-callme" href="/callme/" rel="nofollow"></a> 




<?
// всплывайка через определенный срок
if(!$USER->isAuthorized())
{
	//echo "<div style='background:black; color:#272727; text-align: center;'>s = ".$_SESSION["TIME_REG_POPUP"].", ";
	//echo "t = ".time()."</div>";
	$popup_interval = 300;
	if(!isset($_SESSION["TIME_REG_POPUP"]) || (time() > $_SESSION["TIME_REG_POPUP"]))
	{
		// всплывайка
		?>
		<script>
			$(document).ready(function() {
				setTimeout(function(){
					$.fancybox.open({
						'type'		: 'iframe',
						'href'		: '/reg_popup/',
						'padding' 	: 0,
						'maxWidth'	: 400,
						'maxHeight'	: 450,
						'tpl'		: {'wrap' : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin" style="margin:0 !important; border-radius:10px !important;"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'},
					});
				}, 5000);
			});
		</script>
		<?
		// устанваливаем новый TIME
		$_SESSION["TIME_REG_POPUP"] = time() + $popup_interval;
	}
}
?>




<script>
	BX.ready(function(){
		var upButton = document.querySelector('[data-role="eshopUpButton"]');
		BX.bind(upButton, "click", function(){
			var windowScroll = BX.GetWindowScrollPos();
			(new BX.easing({
				duration : 500,
				start : { scroll : windowScroll.scrollTop },
				finish : { scroll : 0 },
				transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
				step : function(state){
					window.scrollTo(0, state.scroll);
				},
				complete: function() {
				}
			})).animate();
		})
	});
</script>
</body>
</html>