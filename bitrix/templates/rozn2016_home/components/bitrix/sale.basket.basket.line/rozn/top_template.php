<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<div class="bx-hdr-profile">
	<?if ($arParams['SHOW_AUTHOR'] == 'Y'):?>
		<div class="bx-basket-block">
			<i class="fa fa-user"></i>
			<?if ($USER->IsAuthorized()):
				$name = trim($USER->GetFullName());
				if (! $name)
					$name = trim($USER->GetLogin());
				if (strlen($name) > 15)
					$name = substr($name, 0, 12).'...';
				?>
				<a href="<?=$arParams['PATH_TO_PROFILE']?>"><?=$name?></a>
				&nbsp;
				<a href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
			<?else:?>
				<a href="/login/"><?=GetMessage('TSB1_LOGIN')?></a>
				&nbsp;
				<a href="/reg/"><?=GetMessage('TSB1_REGISTER')?></a>
			<?endif?>
		</div>
	<?endif?>

	<div class="bx-basket-block">
	
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-icon"></a>
		<div class="basket-goods">
			<span class="title"><?=GetMessage('TSB1_CART')?></span><br/>
			<span class="summm"><?=$arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)']?></span>
		</div>
		<?/*?>
		<div class="basket-summ">
			<?=$arResult['TOTAL_PRICE']?>
		</div>
		<?*/?>
		<div class="clear:both;"></div>

	</div>
</div>
