<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if(count($arResult["ITEMS"]) > 0):?>

	<?
	CModule::IncludeModule("blog");

	$arPostID = Array();
	foreach($arResult["ITEMS"] as $key => $arItem)
	{
		$arPostID[] = $arItem["PROPERTIES"]["POST_ID"]["VALUE"];
	}
	//pra($arPostID);


	// найдем инфу о товарах
	$arExtra = Array();
	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 4, "PROPERTY_BLOG_POST_ID" => $arPostID), false, false, Array("ID", "NAME", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO", "DETAIL_PAGE_URL", "PROPERTY_BLOG_POST_ID", "PROPERTY_BLOG_COMMENTS_CNT"));
	while($arRes = $dbRes->GetNext())
	{
		if($arRes["DETAIL_PICTURE"] > 0)
		{
			$arExtra[$arRes["PROPERTY_BLOG_POST_ID_VALUE"]] = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], array('width'=>100, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arExtra[$arRes["PROPERTY_BLOG_POST_ID_VALUE"]]["NAME"] = $arRes["NAME"];
			$arExtra[$arRes["PROPERTY_BLOG_POST_ID_VALUE"]]["DETAIL_PAGE_URL"] = $arRes["DETAIL_PAGE_URL"];
		}
		elseif(is_array($arResult["PROPERTY_MORE_PHOTO_VALUE"]) && (count($arResult["PROPERTY_MORE_PHOTO_VALUE"])>0))
		{
			//prn("MORE");
		}
		else
		{
			//prn("NOFOTO");
		}
	}
	?>

	<div class="comment-list">
		<h2>Новые отзывы о товарах</h2>
		<?foreach($arResult["ITEMS"] as $arComment):?>
			<div class="comment-item">
				<div class="comment-prod">
					<a title="<?=$arComment["DISPLAY_PROPERTIES"]["PRODUCT"]["DISPLAY_VALUE"]?>" href="<?=$arComment["DISPLAY_PROPERTIES"]["URL"]["VALUE"]?>#comments"><img alt="" src="<?=$arExtra[$arComment["PROPERTIES"]["POST_ID"]["VALUE"]]["src"]?>"></a>
				</div>
				<div class="comment-post">
					<a class="comment-productname" title="<?=$arComment["DISPLAY_PROPERTIES"]["PRODUCT"]["DISPLAY_VALUE"]?>" href="<?=$arComment["DISPLAY_PROPERTIES"]["URL"]["VALUE"]?>#comments"><?=$arComment["DISPLAY_PROPERTIES"]["PRODUCT"]["DISPLAY_VALUE"]?></a>
					<span class="comment-authorname"><?=$arComment["DISPLAY_PROPERTIES"]["AUTHOR"]["DISPLAY_VALUE"]?></span>
					<?/*if($arComment["DISPLAY_PROPERTIES"]["AUTHOR"]["DISPLAY_VALUE"] > 0):?>
						<span class="comment-authorname"><?=$arComment["DISPLAY_PROPERTIES"]["AUTHOR"]["DISPLAY_VALUE"]?></span>
					<?else:?>
						<span class="comment-authorname"><?=$arComment["DISPLAY_PROPERTIES"]["AUTHOR"]["DISPLAY_VALUE"]?></span>
					<?endif*/?>
					<span class="comment-date"><?=$arComment["DATE_CREATE"]?></span><br>
					<a class="comment-text" href="<?=$arComment["DISPLAY_PROPERTIES"]["URL"]["VALUE"]?>#comments">
						<?if(strLen($arComment["DETAIL_TEXT"])>255):?>
							<?=substr($arComment["DETAIL_TEXT"], 0, 255)?>...
						<?else:?>
							<?=$arComment["DETAIL_TEXT"]?>
						<?endif?>
					</a>
				</div>
			</div>
		<?endforeach?>
	</div>
	
<?endif?>


