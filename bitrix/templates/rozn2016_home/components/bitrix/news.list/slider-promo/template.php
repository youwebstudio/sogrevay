<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick.css");
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick-theme.css");
$APPLICATION->addHeadScript(SITE_TEMPLATE_PATH."/assets/slick/slick.min.js");
?>


<div class="welcome-slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);">
			<?if($arItem["ID"] == 75106):?>
				<a style="position:relative; display:block; top:0; left:0; width:100%; height:100%;" href="/listopad2016/" title="Перейти к товарам акции"></a>
			<?endif?>
			<?if($arItem["ID"] == 165565):?>
				<a style="position:relative; display:block; top: 32%; left: 35%; width: 34%; height: 7%;" href="/zima-zhen/" title="Женский трикотаж по акции"></a>
				<a style="position:relative; display:block; top: 34%; left: 35%; width: 34%; height: 7%;" href="/zima-deti/" title="Детский трикотаж по акции"></a>
			<?endif?>
			<?if($arItem["ID"] == 165566):?>
				<a class="btn-zhen" href="/zima-zhen/" title="Женский трикотаж по акции">ЖЕНСКИЙ ТРИКОТАЖ</a>
				<a class="btn-det" href="/zima-deti/" title="Детский трикотаж по акции">ДЕТСКИЙ ТРИКОТАЖ</a>
				<span class="sroki">Срок проведения акции до 20.12.2016</span>
			<?endif?>
		</div>
	<?endforeach?>
</div>
<div class="VAYslogan1" style=""></div>
<?/*?>
	<div class="VAYslogan" style="">SOGREVAY - интернет-магазин российского трикотажа</div>

<div class="VAYlogo" style="background-image: url(<?=SITE_TEMPLATE_PATH."/images/VAYlogo.png"?>);"></div>
<?*/?>