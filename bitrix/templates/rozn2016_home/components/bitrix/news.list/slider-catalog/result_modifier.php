<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
$arDefaultParams = array(
	'TEMPLATE_THEME' => 'blue',
	'PRODUCT_DISPLAY_MODE' => 'N',
	'ADD_PICT_PROP' => '-',
	'LABEL_PROP' => '-',
	'OFFER_ADD_PICT_PROP' => '-',
	'OFFER_TREE_PROPS' => array('-'),
	'PRODUCT_SUBSCRIPTION' => 'N',
	'SHOW_DISCOUNT_PERCENT' => 'N',
	'SHOW_OLD_PRICE' => 'N',
	'ADD_TO_BASKET_ACTION' => 'ADD',
	'SHOW_CLOSE_POPUP' => 'N',
	'MESS_BTN_BUY' => '',
	'MESS_BTN_ADD_TO_BASKET' => '',
	'MESS_BTN_SUBSCRIBE' => '',
	'MESS_BTN_DETAIL' => '',
	'MESS_NOT_AVAILABLE' => '',
	'MESS_BTN_COMPARE' => ''
);
$arParams = array_merge($arDefaultParams, $arParams);


  


// ===========================================================================================
//   Подставим фото для торговых предложений из свойства MORE_PHOTO и DETAIL_PICTURE товара
//   (тело функции в init.php)
// ===========================================================================================
if(function_exists("setOfferPictures"))
	foreach($arResult["ITEMS"] as $keyItem => $arItem) $arResult["ITEMS"][$keyItem] = setOfferPictures($arItem);



// ===========================================================================================
//   Сортируем торговые предложения чтоб первым шло то, чья картинка стоит основной
//   (тело функции в init.php)
// ===========================================================================================
if(function_exists("sortOffersInItem"))
	foreach($arResult["ITEMS"] as $keyItem => $arItem) 	$arResult["ITEMS"][$keyItem] = sortOffersInItem($arItem);



	

?>