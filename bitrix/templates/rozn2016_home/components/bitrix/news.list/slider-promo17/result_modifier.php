<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult["ITEMS"] as $kItem => $arItem)
{
	//pra($arItem["PROPERTIES"]["BTN1_TEXT"]["VALUE"]);
	//pra($arItem["PROPERTIES"]);
	$arButtons = Array();
	if(!empty($arItem["PROPERTIES"]["BTN1_TEXT"]["VALUE"]) && !empty($arItem["PROPERTIES"]["BTN1_LINK"]["VALUE"])) 
		$arButtons[] = Array("NAME" => $arItem["PROPERTIES"]["BTN1_TEXT"]["VALUE"], "LINK" => $arItem["PROPERTIES"]["BTN1_LINK"]["VALUE"]);
	if(!empty($arItem["PROPERTIES"]["BTN2_TEXT"]["VALUE"]) && !empty($arItem["PROPERTIES"]["BTN2_LINK"]["VALUE"])) 
		$arButtons[] = Array("NAME" => $arItem["PROPERTIES"]["BTN2_TEXT"]["VALUE"], "LINK" => $arItem["PROPERTIES"]["BTN2_LINK"]["VALUE"]);
	if(!empty($arItem["PROPERTIES"]["BTN3_TEXT"]["VALUE"]) && !empty($arItem["PROPERTIES"]["BTN3_LINK"]["VALUE"])) 
		$arButtons[] = Array("NAME" => $arItem["PROPERTIES"]["BTN3_TEXT"]["VALUE"], "LINK" => $arItem["PROPERTIES"]["BTN3_LINK"]["VALUE"]);
	if(!empty($arItem["PROPERTIES"]["BTN4_TEXT"]["VALUE"]) && !empty($arItem["PROPERTIES"]["BTN4_LINK"]["VALUE"])) 
		$arButtons[] = Array("NAME" => $arItem["PROPERTIES"]["BTN4_TEXT"]["VALUE"], "LINK" => $arItem["PROPERTIES"]["BTN4_LINK"]["VALUE"]);
	if(!empty($arItem["PROPERTIES"]["BTN5_TEXT"]["VALUE"]) && !empty($arItem["PROPERTIES"]["BTN5_LINK"]["VALUE"])) 
		$arButtons[] = Array("NAME" => $arItem["PROPERTIES"]["BTN5_TEXT"]["VALUE"], "LINK" => $arItem["PROPERTIES"]["BTN5_LINK"]["VALUE"]);
	//pra($arButtons);
	$arResult["ITEMS"][$kItem]["BUTTONS"] = $arButtons;
}

//pra($arButtons);

?>