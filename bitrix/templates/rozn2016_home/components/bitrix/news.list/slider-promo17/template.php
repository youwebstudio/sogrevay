<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick.css");
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick-theme.css");
$APPLICATION->addHeadScript(SITE_TEMPLATE_PATH."/assets/slick/slick.min.js");

$val_opacity = intVal(trim($arParams["SLOGAN_OPACITY"]));
if($val_opacity > 0)
{
	if(($val_opacity < 1)||($val_opacity > 100)) $opacity = 1;
	else $opacity = $val_opacity/100;
}	
?>





<?
// Реализуем настройки 
$arStyle = Array();
$arSizes = Array(
	"LG" => "min-width: 1201px",
	"MD" => "max-width: 1200px",
	"SM" => "max-width: 992px",
	"XS" => "max-width: 768px",
	"XXS" => "max-width: 500px"
);
foreach($arResult["ITEMS"] as $arItem)
{
	for($i=1; $i<6; $i++)
	{
		$arStyle["XXS"][$arItem["ID"]][$i] = $arItem["PROPERTIES"]["BTN".$i."_STYLE_XXS"]["VALUE"]["TEXT"];
		$arStyle["XS"][$arItem["ID"]][$i] = $arItem["PROPERTIES"]["BTN".$i."_STYLE_XS"]["VALUE"]["TEXT"];
		$arStyle["SM"][$arItem["ID"]][$i] = $arItem["PROPERTIES"]["BTN".$i."_STYLE_SM"]["VALUE"]["TEXT"];
		$arStyle["MD"][$arItem["ID"]][$i] = $arItem["PROPERTIES"]["BTN".$i."_STYLE_MD"]["VALUE"]["TEXT"];
		$arStyle["LG"][$arItem["ID"]][$i] = $arItem["PROPERTIES"]["BTN".$i."_STYLE_LG"]["VALUE"]["TEXT"];
	}
	$arStyle["ALL"][$arItem["ID"]] = $arItem["PROPERTIES"]["BTNS_ALL_STYLE"]["VALUE"]["TEXT"];
}
$str = "";
/*
pra($arItem["PROPERTIES"]["BTNS_ALL_STYLE"]["VALUE"]);
if(strLen($arItem["PROPERTIES"]["BTNS_ALL_STYLE"]["VALUE"]["TEXT"])>0) 
{
	$str .= ".btn-slide {\n";	
	$str .= $arItem["PROPERTIES"]["BTNS_ALL_STYLE"]["VALUE"]["TEXT"];
	$str .= "}\n";
}
*/
//pra($arStyle);


foreach($arStyle["ALL"] as $slideID => $slideAllStyle)
{
	if(strLen(trim($slideAllStyle))>0) $str .= "a#slide-".$slideID.".btn-slide {\n".$slideAllStyle."\n}\n";
}
foreach($arSizes as $sizeID => $sizeCode)
{
	$str .= "@media (".$sizeCode.") {\n";
	foreach($arStyle[$sizeID] as $slideID => $arSlide)
		foreach($arSlide as $btnID => $style)
			if(strLen(trim($style))>0) $str .= "\ta#slide-".$slideID.".btn-slide.btn-".$btnID." {".$style."}\n";
	$str .= "}\n";
}

?>
<style><?=$str?></style>



<div class="welcome-slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?//pra($arItem["BUTTONS"]);?>
		<div style="position: relative; text-align: center;">
			<?if(!empty($arItem["PROPERTIES"]["WHOLE_LINK"]["VALUE"])):?><a href="<?=$arItem["PROPERTIES"]["WHOLE_LINK"]["VALUE"]?>"><?endif?>
			<img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>">
			<?if(!empty($arItem["PROPERTIES"]["WHOLE_LINK"]["VALUE"])):?></a><?endif?>
			<?if(count($arItem["BUTTONS"])>0):?>
				<?foreach($arItem["BUTTONS"] as $k => $arButton):?>
					<a id="slide-<?=$arItem["ID"]?>" class="btn-slide btn-<?=$k+1?>" href="<?=$arButton["LINK"]?>" title="<?=$arButton["NAME"]?>"><?=$arButton["NAME"]?></a>
				<?endforeach?>
			<?endif?>
			
			<?/*if($arItem["ID"] == 165565):?>
				<a class="btn-zhen" href="/fashion-feb/?for=zhen" title="Женский трикотаж по акции">ЖЕНСКИЙ ТРИКОТАЖ</a>
				<a class="btn-det" href="/fashion-feb/?for=deti" title="Детский трикотаж по акции">ДЕТСКИЙ ТРИКОТАЖ</a>
			<?endif*/?>
			
		</div>
	<?endforeach?>
</div>
<div class="VAYslogan1" style="opacity:<?=$opacity?>;">
	<img alt="SOGREVAY - интернет-магазин российского трикотажа от производителя" src="/upload/sogrevayslogan1.png">
</div>
<?/*?>
	<div class="VAYslogan" style="">SOGREVAY - интернет-магазин российского трикотажа</div>

<div class="VAYlogo" style="background-image: url(<?=SITE_TEMPLATE_PATH."/images/VAYlogo.png"?>);"></div>
<?*/?>

<?//pra($arResult["ITEMS"]);?>