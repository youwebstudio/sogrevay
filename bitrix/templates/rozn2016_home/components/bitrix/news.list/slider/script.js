$(document).ready(function(){
	$('.welcome-slider').slick({
		dots: true
	});
	
	$('.welcome-slider').on('setPosition', function(event, slick, direction){
		var w = $('.welcome-slider .slick-slide').width();
		var h = w*0.4;
		$('.welcome-slider .slick-slide').height(h);
	});
	
});