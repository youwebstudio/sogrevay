<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult) > 0):?>
	<div class="comment-list">
		<h2>Отзывы о товарах</h2>
		<?foreach($arResult as $arComment):?>
			<div class="comment-item">
				<div class="comment-prod">
					<a title="<?=$arComment["PRODUCT_INFO"]["NAME"]?>" href="<?=$arComment["PRODUCT_INFO"]["DETAIL_PAGE_URL"]?>#comments"><img alt="<?=$arComment["PRODUCT_INFO"]["NAME"]?>" src="<?=$arComment["PRODUCT_INFO"]["src"]?>"></a>
				</div>
				<div class="comment-post">
					<a class="comment-productname" title="<?=$arComment["PRODUCT_INFO"]["NAME"]?>" href="<?=$arComment["PRODUCT_INFO"]["DETAIL_PAGE_URL"]?>#comments"><?=$arComment["PRODUCT_INFO"]["NAME"]?></a>
					<?if($arComment["AUTHOR_ID"] > 0):?>
						<span class="comment-authorname"><?=$arComment["arUser"]["NAME"]?></span>
					<?else:?>
						<span class="comment-authorname"><?=$arComment["AUTHOR_NAME"]?></span>
					<?endif?>
					<span class="comment-date"><?=$arComment["DATE_CREATE"]?></span><br>
					<a class="comment-text" href="<?=$arComment["PRODUCT_INFO"]["DETAIL_PAGE_URL"]?>#comments"><?=$arComment["TEXT_FORMATED"]?></a>
				</div>
			</div>
		<?endforeach?>
	</div>
<?endif?>