<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


// выясним какие пользователи являются админами
$arAdminUsers = Array();
$arFilter = Array("GROUPS_ID" => Array(1)); 
$rsUsers = CUser::GetList(($by="ID"), ($order="asc"), $arFilter); // выбираем пользователей 
while($arUser = $rsUsers->GetNext()) {
	$arAdminUsers[] = $arUser["ID"];
}



// удалим посты от админов
$num = 0;
$arNew = Array();
$arPostID = Array();
$arUsers = Array();
foreach($arResult as $key => $arItem)
{
	if(in_array($arItem["AUTHOR_ID"], $arAdminUsers))
	{
		unset($arResult[$key]);
		continue;
	}
	$arNew[] = $arItem;
	$arPostID[] = $arItem["POST_ID"];
	$arUsers[] = $arItem["AUTHOR_ID"];
	$num++;
	if($num >= intVal($arParams["COUNT_WITHOUT_ADMINS"])) break;
}
$arResult = $arNew;





// найдем инфу о товарах
$arExtra = Array();
$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 4, "PROPERTY_BLOG_POST_ID" => $arPostID, ">PROPERTY_BLOG_COMMENTS_CNT" => 0), false, false, Array("ID", "NAME", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO", "DETAIL_PAGE_URL", "PROPERTY_BLOG_POST_ID", "PROPERTY_BLOG_COMMENTS_CNT"));
while($arRes = $dbRes->GetNext())
{
	if($arRes["DETAIL_PICTURE"] > 0)
	{
		$arExtra[$arRes["PROPERTY_BLOG_POST_ID_VALUE"]] = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], array('width'=>100, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$arExtra[$arRes["PROPERTY_BLOG_POST_ID_VALUE"]]["NAME"] = $arRes["NAME"];
		$arExtra[$arRes["PROPERTY_BLOG_POST_ID_VALUE"]]["DETAIL_PAGE_URL"] = $arRes["DETAIL_PAGE_URL"];
	}
	elseif(is_array($arResult["PROPERTY_MORE_PHOTO_VALUE"]) && (count($arResult["PROPERTY_MORE_PHOTO_VALUE"])>0))
	{
		//prn("MORE");
	}
	else
	{
		//prn("NOFOTO");
	}
}




// вносим в общий arResult
foreach($arResult as $key => $arItem)
{
	if(isset($arExtra[$arItem["POST_ID"]]))
		$arResult[$key]["PRODUCT_INFO"] = $arExtra[$arItem["POST_ID"]];
}



?>

