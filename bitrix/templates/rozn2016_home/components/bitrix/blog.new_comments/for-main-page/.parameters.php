<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule("blog"))
	return false;

$arTemplateParameters = array(
	"COUNT_WITHOUT_ADMINS" => array(
		"PARENT" => "VISUAL",
		"NAME" => "Количество результатов, выводимых на страницу (без сообщений администраторов)",
		"TYPE" => "STRING",
		"DEFAULT" => "5"
		),
	);
?>