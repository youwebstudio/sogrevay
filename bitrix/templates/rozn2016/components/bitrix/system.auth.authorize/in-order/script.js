$(document).ready( function() {
	
	// стилизуем checkbox
	$("input.cb").hide().after("<div class=\"cb\"></div>");
	$(":checked").next().addClass("checked");
	
	var check_flag = false; // странный баг с двойным срабатыванием клика на label (пропускаем каждое второе срабатывание)
	$(".bx-filter-param-label").click(function (event) {
		check_flag = !check_flag;
		if(check_flag)	{
			event.stopPropagation();
			var el = $(this).find("div.cb");
			if(el.hasClass("checked")) el.removeClass("checked");
			else el.addClass("checked");
			}
		});
	
	
});