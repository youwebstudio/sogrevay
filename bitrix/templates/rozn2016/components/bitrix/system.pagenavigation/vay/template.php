<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

//prn($arParams["NAV_RESULT"]->Fetch());
$sortedByCount20 = $_GET["count"] == 20 || empty($_GET["count"]) ? "active" : "";
$sortedByCount40 = $_GET["count"] == 40 ? "active" : "";
$sortedByCount60 = $_GET["count"] == 60 ? "active" : "";


if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$colorSchemes = array(
	"green" => "bx-green",
	"yellow" => "bx-yellow",
	"red" => "bx-red",
	"blue" => "bx-blue",
);
if(isset($colorSchemes[$arParams["TEMPLATE_THEME"]]))
{
	$colorScheme = $colorSchemes[$arParams["TEMPLATE_THEME"]];
}
else
{
	$colorScheme = "";
}
?>

<div class="bx-pagination <?=$colorScheme?>">
	<div class="bx-pagination-container row">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-left">
			<?if($arResult["NavRecordCount"]):?>
			<span class="rec-count">Товаров: <span><?=$arResult["NavRecordCount"]?></span></span>
			<?endif?>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
			<ul>
	<?if($arResult["bDescPageNumbering"] === true):?>

		<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<?if($arResult["bSavePage"]):?>
				<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
				<li class=""><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><span>1</span></a></li>
			<?else:?>
				<?if (($arResult["NavPageNomer"]+1) == $arResult["NavPageCount"]):?>
					<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
				<?else:?>
					<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
				<?endif?>
				<li class=""><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span>1</span></a></li>
			<?endif?>
		<?else:?>
				<li class="bx-pag-prev"><span><?echo GetMessage("round_nav_back")?></span></li>
				<li class="bx-active"><span>1</span></li>
		<?endif?>

		<?
		$arResult["nStartPage"]--;
		while($arResult["nStartPage"] >= $arResult["nEndPage"]+1):
		?>
			<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<li class="bx-active"><span><?=$NavRecordGroupPrint?></span></li>
			<?else:?>
				<li class=""><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><span><?=$NavRecordGroupPrint?></span></a></li>
			<?endif?>

			<?$arResult["nStartPage"]--?>
		<?endwhile?>

		<?if ($arResult["NavPageNomer"] > 1):?>
			<?if($arResult["NavPageCount"] > 1):?>
				<li class=""><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><span><?=$arResult["NavPageCount"]?></span></a></li>
			<?endif?>
				<li class="bx-pag-next"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><span><?echo GetMessage("round_nav_forward")?></span></a></li>
		<?else:?>
			<?if($arResult["NavPageCount"] > 1):?>
				<li class="bx-active"><span><?=$arResult["NavPageCount"]?></span></li>
			<?endif?>
				<li class="bx-pag-next"><span><?echo GetMessage("round_nav_forward")?></span></li>
		<?endif?>

	<?else:?>

		<?if ($arResult["NavPageNomer"] > 1):?>
			<?if($arResult["bSavePage"]):?>
				<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
				<li class=""><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><span>1</span></a></li>
			<?else:?>
				<?if ($arResult["NavPageNomer"] > 2):?>
					<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
				<?else:?>
					<li class="bx-pag-prev"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span><?echo GetMessage("round_nav_back")?></span></a></li>
				<?endif?>
				<li class=""><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span>1</span></a></li>
			<?endif?>
		<?else:?>
				<li class="bx-pag-prev"><span><?echo GetMessage("round_nav_back")?></span></li>
				<li class="bx-active"><span>1</span></li>
		<?endif?>

		<?
		$arResult["nStartPage"]++;
		while($arResult["nStartPage"] <= $arResult["nEndPage"]-1):
		?>
			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<li class="bx-active"><span><?=$arResult["nStartPage"]?></span></li>
			<?else:?>
				<li class=""><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><span><?=$arResult["nStartPage"]?></span></a></li>
			<?endif?>
			<?$arResult["nStartPage"]++?>
		<?endwhile?>

		<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<?if($arResult["NavPageCount"] > 1):?>
				<li class=""><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><span><?=$arResult["NavPageCount"]?></span></a></li>
			<?endif?>
				<li class="bx-pag-next"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><span><?echo GetMessage("round_nav_forward")?></span></a></li>
		<?else:?>
			<?if($arResult["NavPageCount"] > 1):?>
				<li class="bx-active"><span><?=$arResult["NavPageCount"]?></span></li>
			<?endif?>
				<li class="bx-pag-next"><span><?echo GetMessage("round_nav_forward")?></span></li>
		<?endif?>
	<?endif?>

	<?if ($arResult["bShowAll"]):?>
		<?if ($arResult["NavShowAll"]):?>
				<li class="bx-pag-all"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><span><?echo GetMessage("round_nav_pages")?></span></a></li>
		<?else:?>
				<li class="bx-pag-all"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><span><?echo GetMessage("round_nav_all")?></span></a></li>
		<?endif?>
	<?endif?>
			</ul>
			<div style="clear:both"></div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<div class="div-count-bottom">
				<span class="count-title">Показывать:</span>
				<a class="<?=$sortedByCount20?>" href="<?=$APPLICATION->GetCurPageParam("", array("count"))?>">20</a>
				<a class="<?=$sortedByCount40?>" href="<?=$APPLICATION->GetCurPageParam("count=40", array("count"))?>">40</a>
				<a class="<?=$sortedByCount60?>" href="<?=$APPLICATION->GetCurPageParam("count=60", array("count"))?>">60</a>
			</div>
		</div>
	</div>
</div>