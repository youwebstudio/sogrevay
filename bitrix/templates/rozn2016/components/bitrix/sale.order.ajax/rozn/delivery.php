<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?//pra($arResult["USER_VALS"]);?>
<div style="display:none;">
<?
//prn($arResult["USER_VALS"]);
//die('===========');
?>
</div>
<?
//pra($arResult["DELIVERY"]);
//prn($arResult["DELIVERY"]);
//die('===========');
?>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult["BUYER_STORE"]?>" />
<div class="bx_section delivery">
	<h4><?=GetMessage("SOA_TEMPL_DELIVERY")?></h4>
	<?
	if(!empty($arResult["DELIVERY"]))
	{
		$width = ($arParams["SHOW_STORES_IMAGES"] == "Y") ? 850 : 700;

		foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
		{
			if ($delivery_id !== 0 && intval($delivery_id) <= 0)
			{
				foreach ($arDelivery["PROFILES"] as $profile_id => $arProfile)
				{
					?>
					<div class="bx_block w100 vertical b_delivery">
						<div class="bx_element">

							<input
								type="radio"
								id="ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>"
								name="<?=htmlspecialcharsbx($arProfile["FIELD_NAME"])?>"
								value="<?=$delivery_id.":".$profile_id;?>"
								<?=$arProfile["CHECKED"] == "Y" ? "checked=\"checked\"" : "";?>
								onclick="submitForm();"
								/>

							<label for="ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>"<?if($arResult["USER_VALS"]["DELIVERY_ID"] == $delivery_id.":".$profile_id):?> class="active"<?endif?>>
							
							
							<hr><hr><hr>
								

								<?
								if (count($arDelivery["LOGOTIP"]) > 0):

									$arFileTmp = CFile::ResizeImageGet(
										$arDelivery["LOGOTIP"]["ID"],
										array("width" => "95", "height" =>"55"),
										BX_RESIZE_IMAGE_PROPORTIONAL,
										true
									);

									$deliveryImgURL = $arFileTmp["src"];
								else:
									$deliveryImgURL = $templateFolder."/images/logo-default-d.gif";
								endif;

								if($arDelivery["ISNEEDEXTRAINFO"] == "Y")
									$extraParams = "showExtraParamsDialog('".$delivery_id.":".$profile_id."');";
								else
									$extraParams = "";

								?>
								<div class="bx_logotype" onclick="BX('ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>').checked=true;<?=$extraParams?>submitForm();">
									<span style='background-image:url(<?=$deliveryImgURL?>);'></span>
								</div>

								<div class="bx_description">
								
									<strong onclick="BX('ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>').checked=true;<?=$extraParams?>submitForm();">
										<?=htmlspecialcharsbx($arProfile["TITLE"])/*." - ".htmlspecialcharsbx($arDelivery["TITLE"]);*/?>
									</strong>

									<span class="bx_result_price"><!-- click on this should not cause form submit -->
										<?
										if($arProfile["CHECKED"] == "Y" && doubleval($arResult["DELIVERY_PRICE"]) > 0):
										?>
											<div><?=GetMessage("SALE_DELIV_PRICE")?>:&nbsp;<b><?=$arResult["DELIVERY_PRICE_FORMATED"]?></b></div>
										<?
											if ((isset($arResult["PACKS_COUNT"]) && $arResult["PACKS_COUNT"]) > 1):
												echo GetMessage('SALE_PACKS_COUNT').': <b>'.$arResult["PACKS_COUNT"].'</b>';
											endif;
										else:
											$APPLICATION->IncludeComponent('bitrix:sale.ajax.delivery.calculator', '', array(
												"NO_AJAX" => $arParams["DELIVERY_NO_AJAX"],
												"DELIVERY" => $delivery_id,
												"PROFILE" => $profile_id,
												"ORDER_WEIGHT" => $arResult["ORDER_WEIGHT"],
												"ORDER_PRICE" => $arResult["ORDER_PRICE"],
												"LOCATION_TO" => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
												"LOCATION_ZIP" => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
												"CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
												"ITEMS" => $arResult["BASKET_ITEMS"],
												"EXTRA_PARAMS_CALLBACK" => $extraParams
											), null, array('HIDE_ICONS' => 'Y'));
										endif;
										?>
									</span>

									<?if (strlen($arProfile["DESCRIPTION"]) > 0):?>
										<p onclick="BX('ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>').checked=true;submitForm();">
											<?if (strlen($arProfile["DESCRIPTION"]) > 0):?>
												<?=nl2br($arProfile["DESCRIPTION"])?>
											<?else:?>
												<?=nl2br($arDelivery["DESCRIPTION"])?> 
											<?endif;?>
										</p>
									<?endif?>
								</div>
								
								
								
								
								
								<?if($profile_id == "boxberry"):?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									//prd($arResult["USER_VALS"]["DELIVERY_LOCATION"]);
									$arRes = $gr->getPostcode("boxberry", $arResult["USER_VALS"]["DELIVERY_LOCATION"]);
									$postcode = $arRes[0]["Id"];
									?>
									<?if(strLen($postcode) > 0):?>
										<input type="hidden" name="POSTCODE_ID" value="<?=$postcode?>"/>
									<?endif?>
									
									<?/*if($arResult["USER_VALS"]["DELIVERY_ID"] == "grastin:boxberry"):?>
										<span style="font-weight:normal; display:block; margin-top:5px;">Уточните адрес доставки:</span>
										<textarea rows="2" cols="30" name="ORDER_PROP_7" id="ORDER_PROP_7"><?=$arResult["USER_VALS"]["ORDER_PROP"][7]?></textarea>
									<?endif*/?>
								<?endif?>
							
								
								
								
								<?if($profile_id == "post"):?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									foreach($gr->GRASTIN_DATA["arDeliveryRegion"] as $arRegion)
									{
										if($arRegion["Name"] == "Почта") $regionID = $arRegion["Id"];
									}
									?>
									<?if(strLen($regionID) > 0):?>
										<input type="hidden" name="REGION_ID" value="<?=$regionID?>"/>
									<?endif?>
									
									<?if($arResult["USER_VALS"]["DELIVERY_ID"] == "grastin:post"):?>
										<span style="font-weight:normal; display:block; margin-top:5px;">Уточните адрес доставки (ОБЯЗАТЕЛЬНО УКАЗЫВАЙТЕ ИНДЕКС):</span>
										<textarea rows="2" cols="30" name="ORDER_PROP_7" id="ORDER_PROP_7" style="margin-left: 120px; margin-top: 7px;"><?=$arResult["USER_VALS"]["ORDER_PROP"][7]?></textarea>
									<?endif?>
									
									
								<?endif?>
								
								
								
								
								<?if($profile_id == "grastin"):?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									$ar = CSaleLocation::GetByID($arResult["USER_VALS"]["DELIVERY_LOCATION"], "ru"); 
									$city = $ar["CITY_NAME_LANG"];	
									
									foreach($gr->GRASTIN_DATA["arDeliveryRegion"] as $arRegion)
									{
										if($arRegion["Name"] == $city) $regionID = $arRegion["Id"];
									}
									?>
									<?if(strLen($regionID) > 0):?>
										<input type="hidden" name="REGION_ID" value="<?=$regionID?>"/>
									<?endif?>
									
									<?/*if($arResult["USER_VALS"]["DELIVERY_ID"] == "grastin:grastin"):?>
										<span style="font-weight:normal; display:block; margin-top:5px;">Уточните адрес доставки:</span>
										<textarea rows="2" cols="30" name="ORDER_PROP_7" id="ORDER_PROP_7"><?=$arResult["USER_VALS"]["ORDER_PROP"][7]?></textarea>
									<?endif*/?>
									
								<?endif?>
								
								
								
								
								<?if($profile_id == "boxberrySelfpickup"):?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									$arPostamats = $gr->getPostamats($profile_id, $arResult["USER_VALS"]["DELIVERY_LOCATION"]);
									?>
									<?if(count($arPostamats)>0):?>
										<div class="bx_extra">
											<select class="e-postamat-select" size="1">
												<?foreach($arPostamats as $arP):?>
													<option value="<?=$arP["Id"]?>"<?if($arP["Id"] == $arResult["USER_VALS"]["ORDER_PROP"][20]):?> selected<?endif?>>
														<?=$arP["Name"]?>
														<?/*if(strLen($arP["schedule"])>0):?> (<?=$arP["schedule"]?>)<?endif*/?>
													</option>
												<?endforeach?>
											</select>
										</div>	
									<?endif?>
								<?endif?>
								
								
								
								<?if($profile_id == "grastinSelfpickup"):?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									$arPVZ = $gr->getGrastinPVZ($arResult["USER_VALS"]["DELIVERY_LOCATION"]);
									?>
									<?if(count($arPVZ)>0):?>
										<div class="bx_extra">
											<select class="e-postamat-select" size="1">
												<?foreach($arPVZ as $arP):?>
													<option value="<?=$arP["id"]?>"<?if($arP["Id"] == $arResult["USER_VALS"]["ORDER_PROP"][20]):?> selected<?endif?>>
														<?=$arP["Name"]?>
													</option>
												<?endforeach?>
											</select>
										</div>	
									<?endif?>
								<?endif?>
								
								
								

							</label>
							
							
						

						</div>
					</div>
					<?
				} // endforeach
			}
			else // stores and courier
			{
				if (count($arDelivery["STORE"]) > 0)
					$clickHandler = "onClick = \"fShowStore('".$arDelivery["ID"]."','".$arParams["SHOW_STORES_IMAGES"]."','".$width."','".SITE_ID."')\";";
				else
					$clickHandler = "onClick = \"BX('ID_DELIVERY_ID_".$arDelivery["ID"]."').checked=true;submitForm();\"";
				?>
					<div class="bx_block w100 vertical b_delivery">

						<div class="bx_element">

							<input type="radio"
								id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
								name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
								value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
								onclick="submitForm();"
								/>

							<label for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>"<?/*?> <?=$clickHandler?><?*/?><?if($arResult["USER_VALS"]["DELIVERY_ID"] == $arDelivery["ID"]):?> class="active"<?endif?>>

								<?//prn($arDelivery["ID"])?>
								<?//prn($arResult["USER_VALS"]["DELIVERY_ID"])?>
							
								<?
								if (count($arDelivery["LOGOTIP"]) > 0):

									$arFileTmp = CFile::ResizeImageGet(
										$arDelivery["LOGOTIP"]["ID"],
										array("width" => "95", "height" =>"55"),
										BX_RESIZE_IMAGE_PROPORTIONAL,
										true
									);

									$deliveryImgURL = $arFileTmp["src"];
								else:
									$deliveryImgURL = $templateFolder."/images/logo-default-d.gif";
								endif;
								?>

								<div class="bx_logotype"><span style='background-image:url(<?=$deliveryImgURL?>);'></span></div>

								<div class="bx_description">
									<div class="name"><strong><?= htmlspecialcharsbx($arDelivery["OWN_NAME"])?></strong></div>
									<span class="bx_result_price">
										<?
										if (strlen($arDelivery["PERIOD_TEXT"])>0)
										{
											echo $arDelivery["PERIOD_TEXT"];
											?><br /><?
										}
										?>
										<?=GetMessage("SALE_DELIV_PRICE");?>: <b><?=$arDelivery["PRICE_FORMATED"]?></b><br />
									</span>
									<p>
										<?
										if (strlen($arDelivery["DESCRIPTION"])>0)
											echo $arDelivery["DESCRIPTION"]."<br />";

										if (count($arDelivery["STORE"]) > 0):
										?>
											<span id="select_store"<?if(strlen($arResult["STORE_LIST"][$arResult["BUYER_STORE"]]["TITLE"]) <= 0) echo " style=\"display:none;\"";?>>
												<span class="select_store"><?=GetMessage('SOA_ORDER_GIVE_TITLE');?>: </span>
												<span class="ora-store" id="store_desc"><?=htmlspecialcharsbx($arResult["STORE_LIST"][$arResult["BUYER_STORE"]]["TITLE"])?></span>
											</span>
										<?
									endif;
									?> 
									</p>
								</div>

								
								
								<?if($arDelivery["ID"] == 38): // boxberry ?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
																		
									$arRes = $gr->getPostcode("boxberry", $arResult["USER_VALS"]["DELIVERY_LOCATION_BCODE"]);
									$postcode = $arRes[0]["Id"];
									?>
									<?if(strLen($postcode) > 0):?>
										<input type="hidden" name="POSTCODE_ID" value="<?=$postcode?>"/>
									<?endif?>
									
									<?/*if($arResult["USER_VALS"]["DELIVERY_ID"] == "grastin:boxberry"):?>
										<span style="font-weight:normal; display:block; margin-top:5px;">Уточните адрес доставки:</span>
										<textarea rows="2" cols="30" name="ORDER_PROP_7" id="ORDER_PROP_7"><?=$arResult["USER_VALS"]["ORDER_PROP"][7]?></textarea>
									<?endif*/?>
								<?endif?>
							
								
								
								
								<?if($arDelivery["ID"] == 42):   // post ?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									foreach($gr->GRASTIN_DATA["arDeliveryRegion"] as $arRegion)
									{
										if($arRegion["Name"] == "Почта") $regionID = $arRegion["Id"];
									}
									?>
									<?if(strLen($regionID) > 0):?>
										<input type="hidden" name="REGION_ID" value="<?=$regionID?>"/>
									<?endif?>
									
									<?if($arResult["USER_VALS"]["DELIVERY_ID"] == 42):?>
										<span style="font-weight:normal; display:block; margin-top:5px;">Уточните адрес доставки (ОБЯЗАТЕЛЬНО УКАЗЫВАЙТЕ ИНДЕКС):</span>
										<textarea rows="2" cols="30" name="ORDER_PROP_7" id="ORDER_PROP_7" style="margin-left: 120px; margin-top: 7px;"><?=$arResult["USER_VALS"]["ORDER_PROP"][7]?></textarea>
									<?endif?>
									
									
								<?endif?>
								
								
								
								
								<?if($arDelivery["ID"] == 40): // grastin ?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									$ar = CSaleLocation::GetByID($arResult["USER_VALS"]["DELIVERY_LOCATION"], "ru"); 
									$city = $ar["CITY_NAME_LANG"];	
									
									
									foreach($gr->GRASTIN_DATA["arDeliveryRegion"] as $arRegion)
									{
										if($arRegion["Name"] == $city) $regionID = $arRegion["Id"];
									}
									?>
									<?if(strLen($regionID) > 0):?>
										<input type="hidden" name="REGION_ID" value="<?=$regionID?>"/>
									<?endif?>
									
									<?/*if($arResult["USER_VALS"]["DELIVERY_ID"] == "grastin:grastin"):?>
										<span style="font-weight:normal; display:block; margin-top:5px;">Уточните адрес доставки:</span>
										<textarea rows="2" cols="30" name="ORDER_PROP_7" id="ORDER_PROP_7"><?=$arResult["USER_VALS"]["ORDER_PROP"][7]?></textarea>
									<?endif*/?>
									
								<?endif?>
								
								
								
								
								<?if($arDelivery["ID"] == 39): 	// boxberrySelfpickup ?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									$arPostamats = $gr->getPostamats("boxberrySelfpickup", $arResult["USER_VALS"]["DELIVERY_LOCATION"]);
									?>
									
									<?if(count($arPostamats)>0):?>
										<div class="bx_extra">
											<select class="e-postamat-select" size="1">
												<?foreach($arPostamats as $arP):?>
													<option value="<?=$arP["Id"]?>"<?if($arP["Id"] == $arResult["USER_VALS"]["ORDER_PROP"][20]):?> selected<?endif?>>
														<?=$arP["Name"]?>
														<?/*if(strLen($arP["schedule"])>0):?> (<?=$arP["schedule"]?>)<?endif*/?>
													</option>
												<?endforeach?>
											</select>
										</div>	
									<?endif?>
								<?endif?>
								
								
								
								<?if($arDelivery["ID"] == 41): // grastinSelfpickup ?>
									<?
									$gr = new Cgrastin();
									$gr->getGrastinData();
									$arPVZ = $gr->getGrastinPVZ($arResult["USER_VALS"]["DELIVERY_LOCATION"]);
									?>
									<?if(count($arPVZ)>0):?>
										<div class="bx_extra">
											<?//pra($arPVZ);?>
											<select class="e-postamat-select" size="1">
												<?foreach($arPVZ as $arP):?>
													<option value="<?=$arP["id"]?>"<?if($arP["id"] == $arResult["USER_VALS"]["ORDER_PROP"][20]):?> selected<?endif?>>
														<?=$arP["Name"]?>
													</option>
												<?endforeach?>
											</select>
										</div>	
									<?endif?>
								<?endif?>
								
								
								
								
								
								
								
								
								
								
								
								
								
								
							</label>

						<div class="clear"></div>
					</div>
				</div>
				<?
			}
		}
	}
	else // если нет возможных вариантов
	{
		?><div class="no-region" style="border-left: 4px solid #D12A2D;"><p style="display:block; padding-left:20px;">Возможно мы не сможем доставить заказ в выбранный Вами регион.<br/>Вы можете:</p>
		<ul>
			<li>уточнить Ваш регион</li>
			<li>выбрать более крупный ближайший город</li>
			<li>связаться с менеджером для оформления заказа в ручном режиме.</li>
		</ul>
		</div><?
	}
?>
<?//time();?>
<div class="clear"></div>
</div>