<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
//global $USER;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}




// установим prev и next в заголовки страницы
$PAGEN = $arResult["NAV_RESULT"]->PAGEN;
$SIZEN = $arResult["NAV_RESULT"]->SIZEN;
$REC_COUNT = $arResult["NAV_RESULT"]->NavRecordCount;
if($REC_COUNT > $SIZEN) 
{
	$MIN_PAGE = 1;
	$MAX_PAGE = ceil($REC_COUNT/$SIZEN);
	if(($PAGEN >= $MIN_PAGE) && ($PAGEN <= $MAX_PAGE))
	{
		$PROTOCOL = $_SERVER["HTTP_HTTPS"] == "YES" ? "https" : "http";
		if($PAGEN > $MIN_PAGE) 
		{
			if($PAGEN == 2) $prev_link = '<link rel="prev" href="'.$PROTOCOL.'://'.$_SERVER["SERVER_NAME"].$APPLICATION->GetCurPageParam("", array("PAGEN_1")).'">';
			else $prev_link = '<link rel="prev" href="'.$PROTOCOL.'://'.$_SERVER["SERVER_NAME"].$APPLICATION->GetCurPageParam("PAGEN_1=".($PAGEN-1), array("PAGEN_1")).'">';
			$APPLICATION->AddHeadString($prev_link, true);
		}	
		if($PAGEN < $MAX_PAGE) 
		{
			$next_link = '<link rel="next" href="'.$PROTOCOL.'://'.$_SERVER["SERVER_NAME"].$APPLICATION->GetCurPageParam("PAGEN_1=".($PAGEN+1), array("PAGEN_1")).'">';
			$APPLICATION->AddHeadString($next_link, true);
		}
	}
	//$APPLICATION->AddHeadString('<link rel="data" href="'.$MIN_PAGE.' - '.$PAGEN.' - '.$MAX_PAGE.' ('.$REC_COUNT.')">', true);	
}




// добавляем номер страницы в title при паджинации
if($PAGEN>1) 
{
	//$title = 
}





?>