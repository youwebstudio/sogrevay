<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
$APPLICATION->RestartBuffer();
//CJSCore::Init(array("fx", "jquery"));
?>

<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/rozn2016/assets/jquery.maskedinput.min.js"></script>
<link href="<?=SITE_TEMPLATE_PATH?>/components/bitrix/form.result.new/template1/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
var __cs = __cs || [];
__cs.push(["setCsAccount", "yhOyyh8FD21L3IVwy8ySH6nTMYpiquEZ"]);
__cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
</script>
<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
<script>
$(document).ready( function() {
	$("input[name=form_text_2]").mask("8 (999) 999-99-99");
	$('form[name="SIMPLE_FORM_1"]').on('submit',function(){
		
		var credentials = Comagic.getCredentials();
    	for (field in credentials) {
        	if (credentials.hasOwnProperty(field)){
            	$('[name='+field+']').val(credentials[field]);
        	}
    	}
	});
});
</script>

<?if ($arResult["isFormErrors"] == "Y"):?>
	<?//echo $arResult["FORM_ERRORS_TEXT"];?>
<?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>


<?/*?>
<table>
<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
?>
	<tr>
		<td><?

if ($arResult["isFormTitle"])
{
?>
	<h3><?=$arResult["FORM_TITLE"]?></h3>
<?
} //endif ;

	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>

			<p><?=$arResult["FORM_DESCRIPTION"]?></p>
		</td>
	</tr>
	<?
} // endif
	?>
</table>
<br />
<?
*/

/***********************************************************************************
						form questions
***********************************************************************************/
?>
<h3><?=$arResult["FORM_TITLE"]?></h3>
<table class="form-table data-table" style="position: relative;">
	<?/*?><thead>
		<tr>
			<th colspan="2">&nbsp;</th>
		</tr>
	</thead>
	<?*/?>
	<tbody>
	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
		{
			echo $arQuestion["HTML_CODE"];
		}
		else
		{
	?>
		<tr>
			<td>
				<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
				<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
				<?endif;?>
				<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
				<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
			</td>
			<td style="text-align: right;">
				<?=$arQuestion["HTML_CODE"]?>
			</td>
		</tr>
	<?
		}
	} //endwhile
	?>
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<tr>
			<th colspan="2"><b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b></th>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" /></td>
		</tr>
		<tr>
			<td><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></td>
			<td><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></td>
		</tr>
<?
} // isUseCaptcha
?>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="2">
				<input onclick="if($('input[name=form_text_2]').val() != '') {yaCounter38114470.reachGoal('send', 'event', 'zayavka', 'phone');} return true;" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
				<?if ($arResult["F_RIGHT"] >= 15):?>
				&nbsp;<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />
				<?endif;?>
				&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />
			</th>
		</tr>
	</tfoot>
</table>
	<input type="hidden" name='site_key'/>
    <input type="hidden" name='visitor_id'/>
    <input type="hidden" name='hit_id'/>
    <input type="hidden" name='session_id'/>
    <input type="hidden" name='consultant_server_url'/>
<p class="comments">
<?=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")?>
</p>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>

<?if ($arResult["isFormErrors"] == "Y"):?>
	<script>
	$("input[name=form_text_2]").css("background", "rgb(255, 253, 175)").css("border", "1px solid red");
	</script>
	<!--background: ;-->
<?endif;?>

   

<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter38114470 = new Ya.Metrika({
						id:38114470,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/38114470" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

<?die();?>