$(document).ready(function(){
    $('.scrollbar-light').scrollbar();
		
	recalc_viewed();
	
	function recalc_viewed() {
		var item_width = $(".viewed-item").height();
		$(".viewed-wrapper").height(item_width+30);
		var wrapper_width = $(".viewed-wrapper").width();
		if(wrapper_width > 1139) {
			var new_wrapper_height = 360;
			var new_item_height = 325;
			//var items_limit = 5;
		};
		if((wrapper_width > 939) && ((wrapper_width < 1140))) { 
			var new_wrapper_height = 310;
			var new_item_height = 270;
			//var items_limit = 4;
		};
		if((wrapper_width > 719) && ((wrapper_width < 939))) {
			var new_wrapper_height = 290;
			var new_item_height = 255;
			//var items_limit = 4;
		};
		if(wrapper_width<720) {
			var new_wrapper_height = 30 + wrapper_width/2;
			var new_item_height = wrapper_width/2 - 10;
			//var items_limit = 3;
			};
		var items_ex = $(".viewed-item").size();
		//alert(items_ex);
		//alert(items_limit);
		//if(items_ex <= items_limit) 
		//	$(".scroll-element.scroll-x").hide();
		//else 
		//	$(".scroll-element.scroll-x").show();
		$(".viewed-wrapper").height(new_wrapper_height);
		$(".viewed-item").height(new_item_height);
	}; 

	$(window).resize(function(){	
		recalc_viewed();
		});

});
