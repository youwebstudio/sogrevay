<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//prn($GLOBALS["myErr"]); 
//prn($_POST);
//prn($arResult["arUser"]);

if(strLen(strVal($GLOBALS["myErr"])) > 0)
{
	$arResult["arUser"]["LOGIN"] = strVal($_POST["LOGIN"]);
	$arResult["arUser"]["EMAIL"] = strVal($_POST["EMAIL"]);
	$arResult["arUser"]["NAME"] = strVal($_POST["NAME"]);
	$arResult["arUser"]["LAST_NAME"] = strVal($_POST["LAST_NAME"]);
	$arResult["arUser"]["PERSONAL_PHONE"] = strVal($_POST["PERSONAL_PHONE"]);
	$arResult["arUser"]["PERSONAL_BIRTHDAY"] = strVal($_POST["PERSONAL_BIRTHDAY"]);
	$arResult["arUser"]["UF_SUBSCRIBE"] = strVal($_POST["UF_SUBSCRIBE"]);
	//$arResult["arUser"]["NEW_PASSWORD"] = strVal($_POST["PERSONAL_BIRTHDAY"]);
	//$arResult["arUser"]["PERSONAL_BIRTHDAY"] = strVal($_POST["PERSONAL_BIRTHDAY"]);
}

?>


<?=ShowError($arResult["strProfileError"]);?>


<?if(($arResult['DATA_SAVED'] == 'Y') && (isset($_REQUEST["save"])) ):?>
	<div class='alert alert-success' style='width:50%'>
	<?ShowNote(GetMessage('PROFILE_DATA_SAVED'));?>
	</div>
<?endif?>

<div class="bx_profile bx_<?=$arResult["THEME"]?>">
	<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>" />
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		<input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
		<input type="hidden" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />

		<h2><?=GetMessage("LEGEND_PROFILE")?></h2>
		
		<span class="bx-authform-starrequired">*</span>
		<strong><?=GetMessage('NAME')?></strong><br/>
		<input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /><br><br>

		<strong><?=GetMessage('LAST_NAME')?></strong><br/>
		<input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /><br><br>
		
		<span class="bx-authform-starrequired">*</span>
		<strong><?=GetMessage('EMAIL')?></strong><br/>
		<input type="text" name="EMAIL" maxlength="50" value="<?=$arResult["arUser"]["EMAIL"]?>" /><br><br>
		
		<strong><?=GetMessage('PERSONAL_PHONE')?></strong><br/>
		<input type="text" name="PERSONAL_PHONE" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" /><br><br>
		
		<?/*?><span class="bx-authform-starrequired">*</span><?*/?>
		<strong><?=GetMessage('PERSONAL_BIRTHDAY')?></strong>
		<small>(<?=GetMessage("DATE_FORMAT")?>)</small>
		<?$APPLICATION->IncludeComponent(
			'bitrix:main.calendar',
			'',
			array(
				'SHOW_INPUT' => 'N',
				'FORM_NAME' => 'regform',
				'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
				'SHOW_TIME' => 'N'
			),
			null,
			array("HIDE_ICONS"=>"Y")
		);?><br/>
		<input type="text" name="PERSONAL_BIRTHDAY" maxlength="50" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" /><br><br>
		
		
		<?/*?>
		<strong><?=GetMessage('SECOND_NAME')?></strong><br/>
		<input type="text" name="SECOND_NAME" maxlength="50"  value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /><br><br>
		
		<h2><?=GetMessage("MAIN_PSWD")?></h2>
		<?*/?>
		
		<strong><?=GetMessage('NEW_PASSWORD_REQ')?></strong><br/>
		<input type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" /> <br><br>

		<strong><?=GetMessage('NEW_PASSWORD_CONFIRM')?></strong><br/>
		<input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" /> <br><br>

		<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
			<h2><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></h2>
			<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
				<strong><?=$arUserField["EDIT_FORM_LABEL"]?><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?></strong><br/>
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y")
				);?>
				<br/>
			<?endforeach;?>
		<?endif;?>

		<input name="save" value="<?=GetMessage("MAIN_SAVE")?>" class="btn btn-primary" type="submit">
	</form>
	
	<br/><span class="bx-authform-starrequired">*</span> - поля, обязательные для заполнения
	
</div>
<br>
<?
/*
if($arResult["SOCSERV_ENABLED"])
{
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
		false
	);
}
*/
?>
