<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<p>Для отмены заказа обратитесь к менеджеру интернет-магазина по телефонам:</p>
<ul>
<li>8 (495) 502 90 99</li>
<li>8 (905) 544 24 14</li>
</ul>
<br><br>

<?
/*
$APPLICATION->IncludeComponent(
	"bitrix:sale.personal.order.cancel",
	"",
	array(
		"PATH_TO_LIST" => $arResult["PATH_TO_LIST"],
		"PATH_TO_DETAIL" => $arResult["PATH_TO_DETAIL"],
		"SET_TITLE" =>$arParams["SET_TITLE"],
		"ID" => $arResult["VARIABLES"]["ID"],
	),
	$component
);
*/
?>
