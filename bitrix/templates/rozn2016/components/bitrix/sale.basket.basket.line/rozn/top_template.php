<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<div class="bx-hdr-profile">
	<!--noindex-->
	<?if ($arParams['SHOW_AUTHOR'] == 'Y'):?>
		<div class="bx-basket-block">
			<i class="fa fa-user"></i>
			<?if ($USER->IsAuthorized()):
				$name = trim($USER->GetFullName());
				if (! $name)
					$name = trim($USER->GetLogin());
				if (strlen($name) > 15)
					$name = substr($name, 0, 12).'...';
				?>
				<a href="<?=$arParams['PATH_TO_PROFILE']?>"><?=$name?></a>
				&nbsp;
				<a rel="nofollow" href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
			<?else:?>
				<a rel="nofollow" href="/login/"><?=GetMessage('TSB1_LOGIN')?></a>
				&nbsp;
				<a rel="nofollow" href="/reg/"><?=GetMessage('TSB1_REGISTER')?></a>
			<?endif?>
		</div>
	<?endif?>

	
	<?if($USER->isAdmin()):?>
	<div class="bx-basket-block">
		<a rel="nofollow" href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-icon"></a>
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-title"><?=GetMessage('TSB1_CART')?></a>
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-goods"><span class="summm"><?=$arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)']?></span></a>
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-summ"><?=$arResult['TOTAL_PRICE']?></a>
		<div class="clear:both;"></div>
	</div>
	<?else:?>
	<div class="bx-basket-block">
		<a rel="nofollow" href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-icon"></a>
		<div class="basket-title"><?=GetMessage('TSB1_CART')?></div>
		<div class="basket-goods"><span class="summm"><?=$arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)']?></span></div>
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="basket-summ"><?=$arResult['TOTAL_PRICE']?></a>
		<div class="clear:both;"></div>
	</div>
	<?endif?>
	<!--/noindex-->
</div>
