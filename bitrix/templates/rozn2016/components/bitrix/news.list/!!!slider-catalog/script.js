$(document).ready(function(){
	$('.catalog-slider').slick({
		infinite:false,
		dots: true,
		slidesToShow: 2,
		slidesToScroll: 2
		//fade: true//,
		//autoplay: true,
		//autoplaySpeed: 4000
	});
	
	$('.catalog-slider').on('setPosition', function(event, slick, direction){
		var w = $('.catalog-slider .slick-slide').width();
		var h = w*1.5;
		$('.catalog-slider .slick-slide').height(h);
		if(w<=240) {
			$('.logo-block').width(w-10); 
			}
		/*
		if(w>200) {
			$('.logo-block').width(w);
			$('.logo-block .name')
				.css('padding-left', '5px')
				.css('font-size', '11px');
			}
		*/
	});
	
	
});