<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick.css");
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick-theme.css");
$APPLICATION->addHeadScript(SITE_TEMPLATE_PATH."/assets/slick/slick.min.js");
?>

<div class="catalog-slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$collection = "female";
		if(in_array($arItem["PROPERTIES"]["BRAND_REF"]["VALUE"], Array("ВЕСНУШКИ", "VAYKIDS"))) $collection = "kids";
		
		$file = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>533, 'height'=>533), BX_RESIZE_IMAGE_PROPORTIONAL, true, Array("name" => "sharpen",  "precision" => 200));
		
		?>
		<div style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);">
			<div class="grayscale img" style="background-image: url(<?=$file["src"]?>);"></div>
			<div class="logo-block <?=$collection?>">
				<div class="name"><?=$arItem["NAME"]?></div>
				<div class="logo"></div>
			</div>
		</div>
	<?endforeach?>
</div>
