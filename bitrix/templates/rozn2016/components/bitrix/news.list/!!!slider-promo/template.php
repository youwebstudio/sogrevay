<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick.css");
$APPLICATION->setAdditionalCSS(SITE_TEMPLATE_PATH."/assets/slick/slick-theme.css");
$APPLICATION->addHeadScript(SITE_TEMPLATE_PATH."/assets/slick/slick.min.js");
?>


<div class="welcome-slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"></div>
	<?endforeach?>
</div>
<div class="VAYlogo" style="background-image: url(<?=SITE_TEMPLATE_PATH."/images/VAYlogo.png"?>);"></div>
