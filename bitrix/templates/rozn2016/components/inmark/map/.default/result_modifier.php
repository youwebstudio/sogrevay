<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$num = 0;



CModule::IncludeModule('inmarketing.grastin');
$gr = new CGrastin(); 
$gr->getGrastinData();
/*Формирование строки - всплывающих подсказок объектов на карте*/
foreach ($gr->GRASTIN_DATA[arBoxberrySelfPickup] as $ResultItem) {
	pra(trim($ResultItem[latitude])." | ".trim($ResultItem[longitude]));
    $str1 .= '{"type": "Feature", "id": '.$num.', "geometry": {
    	"type": "Point", 
    	"coordinates": ['.trim($ResultItem[latitude]).','.trim($ResultItem[longitude]).']},
    	"properties":{
    		"balloonContent":"<address><strong>Адрес: </strong>'.addslashes($ResultItem[Name]).'</address><div class=\"desc\">'.str_replace("Проезд:","<strong>Проезд: </strong>",str_replace("\n", "<br>",addslashes($ResultItem[drivingdescription]))).'</div><div class=\"schedule\"><strong>График работы: </strong>'.addslashes($ResultItem[schedule]).'</div>",';
    $str1 .='"clusterCaption": "'.addslashes($ResultItem[Name]).'", "hintContent": "'.addslashes($ResultItem[Name]).'"}},';
    $num++;
}
$str1 = substr($str1, 0, strLen($str1)-1);

foreach ($gr->GRASTIN_DATA[arGrastinSelfPickup] as $ResultItem) {
    $str2 .= '{"type": "Feature", "id": '.$num.', "geometry": {
    	"type": "Point", 
    	"coordinates": ['.trim($ResultItem[latitude]).','.trim($ResultItem[longitude]).']},
    	"properties":{
    		"balloonContent":"<address><strong>Адрес: </strong>'.addslashes($ResultItem[Name]).'</address><div class=\"desc\"><strong>Проезд: </strong>'.str_replace("\n", "<br>",addslashes($ResultItem[drivingdescription])).'</div><div class=\"schedule\"><strong>График работы: </strong>'.addslashes($ResultItem[timetable]).'</div>",';
    $str2 .='"clusterCaption": "'.addslashes($ResultItem[Name]).'", "hintContent": "'.addslashes($ResultItem[Name]).'"}},';
    $num++;
}
$str2 = substr($str2, 0, strLen($str2)-1);
$arResult["POINTS"] = [];
array_push($arResult["POINTS"], $str1, $str2);
?>