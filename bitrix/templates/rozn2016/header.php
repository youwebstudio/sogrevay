<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
//$wizTemplateId = COption::GetOptionString("main", "wizard_template_id", "eshop_adapt_horizontal", SITE_ID);
CJSCore::Init(array("fx", "jquery"));
 
if(!is_array($_SESSION["GEO_DATA"])) 
{
	$data = get_geo_data($_SESSION["SESS_IP"]);
	$data["CITY"] = iconv("windows-1251", "utf-8", $data["CITY"]);
	$data["REGION"] = iconv("windows-1251", "utf-8", $data["REGION"]);
	$data["DISTRICT"] = iconv("windows-1251", "utf-8", $data["DISTRICT"]);
	$_SESSION["GEO_DATA"] = $data;
} 
$curPage = $APPLICATION->GetCurPage(true);

if($_SERVER["HTTPS"] == "on") $pro = "https"; else $pro = "http";
$url = COption::GetOptionString("main", "server_name", "www.sogrevay.ru");
$APPLICATION->SetPageProperty("og:image", $pro."://".$url."/upload/logo_red.png");
$APPLICATION->SetPageProperty("og:description", "Интернет-магазин SOGREVAY.RU предлагает большой выбор одежды из трикотажа: женский, мужской и детский трикотаж. Цены. Доставка по Москве и России. Тел. 8 (800) 770-05-33");

global $USER;
// Определям индексная страница ли каталога?
if(($_SERVER["REQUEST_URI"] == "/catalog/") && ($_SERVER["SCRIPT_NAME"] == "/catalog/index.php")) $catalog_index = true; else $catalog_index = false;

?>
<!DOCTYPE html>   
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>"> 
<head>


	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
	<?$APPLICATION->ShowHead();?>
	<?
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");  
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
	
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/vay_male.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/fancybox2/source/jquery.fancybox.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/tooltip/tooltipster.bundle.min.css");
	//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/vay_kids.css");
	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/assets/fancybox2/source/jquery.fancybox.pack.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/assets/jquery.scrollbar.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/assets/tooltip/tooltipster.bundle.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/script.js");
	?>

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#cc3615">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<title><?$APPLICATION->ShowTitle()?></title>
	
	<!-- Google.Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-60609893-10', 'auto');
	  ga('send', 'pageview');
	setTimeout(function(){
	ga('send', 'event', 'New Visitor', location.pathname);
	}, 15000);
	</script>
	<!-- /Google.Analytics -->
	
</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	
	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"top-row", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "12",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "1",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "BACKGROUND_COLOR",
			1 => "COLOR",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "top-row"
	),
	false
);?>
	
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => "/include/top_banner.php",
			"EDIT_TEMPLATE" => ""
		),
	false
	);?>
	
	<div class="bx-wrapper<?if($VAY_MALE):?> vay-male<?endif?><?if($VAY_KIDS):?> vay-kids<?endif?>">
		
		

		<header class="bx-header">
			<div class="container">
				<div class="row">
				
					<div class="col-lg-5 col-md-6 hidden-sm hidden-xs">
						<div class="logo">
							<a title="SOGREVAY - интернет-магазин российского трикотажа от производителя" href="/"></a>
						</div>
						<div class="samovyvoz">
							<a href="/magazin/samovyvoz/">Самовывоз</a>
						</div>
						<div class="russia">
							<a href="/magazin/dostavka/">Доставка по всей России</a>
						</div>
						<div style="clear:both;"></div>
					</div>

					<div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="padding:0;">
						<div class="top-block-2">
						
						<div class="phones-block">
							<a rel="nofollow" href="tel:88007700533" class="phone">8 (800) 770-05-33</a>
							<a rel="nofollow" href="tel:+74955029099" class="phone">8 (495) 502-90-99</a>
							<a rel="nofollow" href="tel:+79055442414" class="phone">8 (905) 544-24-14</a>
							<a rel="nofollow" class="callme fancybox-iframe-callme" href="/callme/">Заказать звонок</a>
							<?/*?>
							<?if(($_SESSION["GEO_DATA"]["CITY"] == "Москва")|| ($_SESSION["GEO_DATA"]["REGION"] == "Московская область")):?>
								<a rel="nofollow" href="tel:+74955029099" class="phone">8 (495) 502 90 99</a>
							<?else:?>
								<a rel="nofollow" href="tel:88007700533" class="phone">8 (800) 770 05 33</a>
							<?endif?>
								<a rel="nofollow" href="tel:+79055442414" class="phone2">8 (905) 544 24 14</a>
							<?*/?>
						</div>
							
							
							
							
						</div>
					</div>
					
					<div class="col-lg-4 col-md-3 hidden-sm hidden-xs" style="padding:0;">
						<div class="top-block-1">
							<div class="cabinet-block">
								
								<?if($USER->IsAuthorized()):
									$name = trim($USER->GetFullName());
									if (! $name)
										$name = trim($USER->GetLogin());
									if (strlen($name) > 18)
										$name = substr($name, 0, 15).'...';
									?>
									<a class="lk" href="/personal/"><?=$name?></a>
									<a class="zakazy" href="/personal/order/">Мои заказы</a> | <a class="logout" href="?logout=yes">Выйти</a>
								<?else:?>
									<a class="login" href="/login/" onclick="yaCounter38114470.reachGoal('VHOD'); return true;">Вход</a> | <a class="regin" href="/reg/" onclick="yaCounter38114470.reachGoal('REGISTR'); return true;">Регистрация</a><br/>
									<?$APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "top");?>
								<?endif?>
							</div>
							
							<?$APPLICATION->IncludeComponent(
								"bitrix:sale.basket.basket.line", 
								"rozn", 
								array(
									"PATH_TO_BASKET" => SITE_DIR."personal/korzina/",
									"PATH_TO_PERSONAL" => SITE_DIR."personal/",
									"SHOW_PERSONAL_LINK" => "N",
									"SHOW_NUM_PRODUCTS" => "Y",
									"SHOW_TOTAL_PRICE" => "Y",
									"SHOW_PRODUCTS" => "N",
									"POSITION_FIXED" => "N",
									"SHOW_AUTHOR" => "N",
									"PATH_TO_REGISTER" => SITE_DIR."reg/",
									"PATH_TO_PROFILE" => SITE_DIR."personal/",
									"COMPONENT_TEMPLATE" => "rozn",
									"SHOW_EMPTY_VALUES" => "Y",
									"SHOW_DELAY" => "Y",
									"SHOW_NOTAVAIL" => "N",
									"SHOW_SUBSCRIBE" => "N",
									"SHOW_IMAGE" => "N",
									"SHOW_PRICE" => "Y",
									"SHOW_SUMMARY" => "Y",
									"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
									"POSITION_HORIZONTAL" => "right",
									"POSITION_VERTICAL" => "top"
								),
								false
								);
							?>
							
							
							<div style="clear:both;"></div>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="top-menu-block" style="">
				<div class="container">
					<div class="row">
						<div class="col-lg-10 col-md-10 hidden-sm hidden-xs">
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu", 
								"main_menu", 
								array(
									"ROOT_MENU_TYPE" => "left",
									"MENU_CACHE_TYPE" => "Y",
									"MENU_CACHE_TIME" => "300",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_THEME" => "black",
									"CACHE_SELECTED_ITEMS" => "N",
									"MENU_CACHE_GET_VARS" => array(
									),
									"MAX_LEVEL" => "4",
									"CHILD_MENU_TYPE" => "left",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "main_menu"
								),
								false
							);?>
						</div>
						<?/*?>
						<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
							<a rel="nofollow" class="callButton fancybox-iframe-callme" href="/callme/">Заказать звонок</a>
							<a rel="nofollow" target="_blank" class="optButton" href="//newvay.ru">Опт</a>
						</div>
						<?*/?>
						<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
							<?$APPLICATION->IncludeComponent(
								"bitrix:search.title", 
								"top", 
								array(
									"CATEGORY_0" => array(
										0 => "iblock_1c_catalog",
									),
									"CATEGORY_0_TITLE" => "",
									"CATEGORY_0_iblock_1c_catalog" => array(
										0 => "4",
									),
									"CHECK_DATES" => "Y",
									"COMPONENT_TEMPLATE" => "top",
									"CONTAINER_ID" => "title-search",
									"INPUT_ID" => "title-search-input",
									"NUM_CATEGORIES" => "all",
									"ORDER" => "date",
									"PAGE" => "#SITE_DIR#search/",
									"SHOW_INPUT" => "Y",
									"SHOW_OTHERS" => "N",
									"TOP_COUNT" => "10",
									"USE_LANGUAGE_GUESS" => "N"
								),
								false
							);?>
						</div>
					</div>
					
				</div>
			</div>
			
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => "/include/under_main_menu_block.php",
					"EDIT_TEMPLATE" => ""
				),
			false
			);?>
			
			<div class="hidden-lg hidden-md mlogo" style="">
				<div class="logo"></div>
			</div>
		
			<div class="hidden-lg hidden-md msearch" style="">
				<?$APPLICATION->IncludeComponent(
					"bitrix:search.form",
					"top-mobile",
					Array(
						"COMPONENT_TEMPLATE" => ".default",
						"PAGE" => "#SITE_DIR#search/",
						"USE_SUGGEST" => "N"
					)
				);?>
			</div>
		</header>

		
		
		
		
		
		
		<?if(!$catalog_index):?>
			<div class="breadcrumbs-block" style="width:100%; background:#EEEEEE;">
				<div class="container">
					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "rozn", array(
							"START_FROM" => "0",
							"PATH" => "",
							"SITE_ID" => "-"
						),
						false,
						Array('HIDE_ICONS' => 'Y')
					);?>
				</div>
			</div>
		<?endif?>
	
		<div class="workarea<?if(!$catalog_index):?> container<?endif?>">
			<?if(!$catalog_index):?><h1 class="bx-title"><?=$APPLICATION->ShowTitle(false);?></h1><?endif?> 

			
		

			


