<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>


<?
$pic = "";
if(strLen(strVal($arParams["DATA_IMAGE"])) > 0) 
	$pic = ' data-image="http://'.$_SERVER["SERVER_NAME"].strVal($arParams["DATA_IMAGE"]).'"';

$url = "";
if(strLen(strVal($arParams["DATA_URL"])) > 0) 
	$url = ' data-url="http://'.$_SERVER["SERVER_NAME"].strVal($arParams["DATA_URL"]).'"';
else
	$url = ' data-url="http://'.$_SERVER["SERVER_NAME"].substr($_SERVER["REQUEST_URI"], 0, strpos($_SERVER["REQUEST_URI"], "?")).'"';

$services ="";
$services = implode(",", $arParams["DATA_SERVICES"]);

$type = "";
if(strVal($arParams["DATA_TYPE"]) == "icons") 	$type = ''; 
if(strVal($arParams["DATA_TYPE"]) == "counters") $type = ' data-counter=""';
if(strVal($arParams["DATA_TYPE"]) == "menu")		$type = ' data-limit="3"';
if(strVal($arParams["DATA_TYPE"]) == "small") 	$type = ' data-size="s"';
?>


<div class="podelitsa">
	<?if(strLen(strVal($arParams["INTRO_TEXT"])) > 0):?>
		<span><?=strVal($arParams["INTRO_TEXT"])?></span>
	<?endif?>
	<div class="ya-share2" data-services="<?=$services?>" <?=$type?><?=$pic?><?=$url?>></div>
</div>