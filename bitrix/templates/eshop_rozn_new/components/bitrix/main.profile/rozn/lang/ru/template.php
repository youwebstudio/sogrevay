<?
$MESS ['PROFILE_DATA_SAVED'] = "Изменения сохранены";
$MESS ['NAME'] = "Имя";
$MESS ['LAST_NAME'] = "Фамилия";
$MESS ['SECOND_NAME'] = "Отчество";
$MESS ['NEW_PASSWORD_CONFIRM'] = "Подтверждение пароля";
$MESS ['NEW_PASSWORD_REQ'] = "Новый пароль";
$MESS ['MAIN_SAVE'] = "Сохранить данные";
$MESS ['MAIN_PSWD'] = "Пароль";
$MESS ['LEGEND_PROFILE'] = "Личные данные";
$MESS ['EMAIL'] = "E-Mail";
$MESS ['PERSONAL_PHONE'] = "Телефон";
$MESS ['PERSONAL_BIRTHDAY'] = "Дата рождения";
$MESS ['DATE_FORMAT'] = "DD.MM.YYYY";
?>