<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<script>
$(document).ready( function() {
	$('.auth-toggle').on('click', function() {
		if($('.bx-authform.in-order').is(':visible'))
			$('.bx-authform.in-order').slideUp(400);
		else
			$('.bx-authform.in-order').slideDown(400);
		return false;
		});
});
</script>



<h4>Авторизуйтесь</h4>

<div style="padding-left:15px;">

	<p>Если Вы были ранее зарегистрированы на сайте и желаете, чтобы новый заказ был добавлен к Вашему существующему аккаунту, Вам необходимо авторизоваться
	<a class="auth-toggle" style="padding-left:15px;" href="">Открыть форму авторизации <i class="fa fa-angle-down"></i></a></p>

	<div class="bx-authform in-order">

	<?if(!empty($arParams["~AUTH_RESULT"])):?>
		<div class="alert alert-danger"><?=htmlspecialcharsbx($arParams["~AUTH_RESULT"]["MESSAGE"])?></div>
	<?endif?>

	<?if($arResult['ERROR_MESSAGE'] <> ''):?>
		<div class="alert alert-danger"><?=htmlspecialcharsbx($arResult['ERROR_MESSAGE'])?></div>
	<?endif?>
		

	<?//prn($arResult);?>	
		
	<?if($arResult["AUTH_SERVICES"]):?>
	<?
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form",
		"flat",
		array(
			"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
			"AUTH_URL" => $arResult["AUTH_URL"],
			"POST" => $arResult["POST"],
		),
		$component,
		array("HIDE_ICONS"=>"Y")
	);
	?>

	<?endif?>

		<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<?if (strlen($arResult["BACKURL"]) > 0):?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>

			<?foreach ($arResult["POST"] as $key => $value):?>
					<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>

			<div class="bx-authform-formgroup-container">
				<div class="bx-authform-label-container" style="display:inline-block; width:100px;"><?=GetMessage("AUTH_LOGIN")?></div>
				<div class="bx-authform-input-container" style="display:inline-block;">
					<input type="text" name="USER_LOGIN" maxlength="255" style="background: none;" value="<?=$arResult["LAST_LOGIN"]?>" />
				</div>
			</div>

			<div class="bx-authform-formgroup-container">
				<div class="bx-authform-label-container" style="display:inline-block; width:100px;"><?=GetMessage("AUTH_PASSWORD")?></div>
				<div class="bx-authform-input-container" style="display:inline-block;">
	<?if($arResult["SECURE_AUTH"]):?>
					<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
						<div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

	<script type="text/javascript">
	document.getElementById('bx_auth_secure').style.display = '';
	</script>
	<?endif?>
					<input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" style="display:inline-block;" />
				</div>
			</div>

	<?if($arResult["CAPTCHA_CODE"]):?>
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />

			<div class="bx-authform-formgroup-container dbg_captha">
				<div class="bx-authform-label-container">
					<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>
				</div>
				<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
				<div class="bx-authform-input-container">
					<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />
				</div>
			</div>
	<?endif;?>

	<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
			<div class="bx-authform-formgroup-container">
				<div class="checkbox">
					<label class="bx-filter-param-label">
						<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />
						<span class="bx-filter-param-text"><?=GetMessage("AUTH_REMEMBER_ME")?></span>
					</label>
				</div>
			</div>
	<?endif?>
			<div class="bx-authform-formgroup-container">
				<input type="submit" class="btn btn-primary" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
			</div>
		</form>

	<?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
		<noindex>
			<div class="bx-authform-link-container" style="display: inline-block;">
				<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></b></a>
			</div>
		</noindex>
	<?endif?>

	<?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
		<noindex>
			<div class="bx-authform-link-container" style="display: inline-block; margin-left:15px;">
				<a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_REGISTER")?></b></a>
			</div>
		</noindex>
	<?endif?>

	</div>
</div>
	

<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>

