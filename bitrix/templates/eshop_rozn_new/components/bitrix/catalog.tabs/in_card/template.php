<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (isset($arParams["DATA"]) && !empty($arParams["DATA"]) && is_array($arParams["DATA"]))
{
	$content = "";
	$activeTabId = "";
	$jsObjName = "catalogTabs_".$arResult["ID"];
	$tabIDList = array();
?>

<?
$container_width = "";
if(isset($arResult["WIDTH"]))
{
	if($arParams["WIDTH_UNITS"] == "%") $container_width = ' style="width: '.$arParams["WIDTH"].'%;"';	
	else $container_width = ' style="width: '.$arResult["WIDTH"].'px;"';	
}


$count = 0;
foreach($arParams["DATA"] as $dat) if(strLen(trim($dat["CONTENT"])) > 0) $count++;
$tab_width = floor(100/$count);
$last_tab_width = 100 - $tab_width*($count-1);
?>

<div id="<? echo $arResult["ID"]; ?>" class="bx-catalog-tab-section-container"<?=$container_width?>>
	<ul class="bx-catalog-tab-list" style="left: 0px;">
		<?
		$num = 0;
		foreach ($arParams["DATA"] as $tabId => $arTab)
		{
			if (isset($arTab["NAME"]) && isset($arTab["CONTENT"]))
			{
				$num++;
				$id = $arResult["ID"].$tabId;
				$tabActive = (isset($arTab["ACTIVE"]) && $arTab["ACTIVE"] == "Y");
				?><li style="width:<?=($num==$count)?$last_tab_width:$tab_width?>%" id="<?=$id?>"><span><?=$arTab["NAME"]?></span></li><?
				if($tabActive || $activeTabId == "")
					$activeTabId = $tabId;

				$content .= '<div id="'.$id.'_cont" class="tab-off">'.$arTab["CONTENT"].'</div>';
				$tabIDList[] = $tabId;
				
			}
		}
		?>
	</ul>
	<div class="bx-catalog-tab-body-container">
		<div class="bx-catalog-tab-container">
			<?=$content?>
		</div>
	</div>
</div>
<?
$arJSParams = array(
	'activeTabId' =>  $activeTabId,
	'tabsContId' => $arResult["ID"],
	'tabList' => $tabIDList
);
?>
<script type="text/javascript">
var <?=$jsObjName?> = new JCCatalogTabs(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>
<?
}
?>