$(document).ready(function(){
	
	// FANCYBOX
	$('.fancybox-iframe').fancybox({
		'autoSize'			: true,
		'minHeight'			: 250,
		'type'				: 'iframe'
		});
		
	$('.fancybox').fancybox({
		'loop'				: false,
		'padding'			: 10,
		'margin'			: 20
		});

});