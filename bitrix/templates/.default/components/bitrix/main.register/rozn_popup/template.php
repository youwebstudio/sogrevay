<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();


$APPLICATION->RestartBuffer();

?>
<style>
body {margin:0 !important;} 
</style>

<?

unset($arResult["ERRORS"]["LOGIN"]); 


// подкорректируем ошибки
if(count($arResult["ERRORS"]) > 0)
{
	$ar = explode("<br>", $arResult["ERRORS"][0]);
	$newAr = Array();
	foreach($ar as $key => $str)
	{
		//prn($key);
		if(strLen($str) < 1) continue;
		if(strpos(" ".$str, "Пользователь с логином ") > 0) continue;
		$newAr[] = $str;
	}
	$arResult["ERRORS"][0] = implode("<br>", $newAr);
}



$newFields = Array("LOGIN", "NAME", "EMAIL", "PERSONAL_PHONE", "PASSWORD", "CONFIRM_PASSWORD");
foreach($arResult["SHOW_FIELDS"] as $field)
{
	if(in_array($field, $newFields)) continue;
	$newFields[] = $field;
}
$arResult["SHOW_FIELDS"] = $newFields;

$path = str_replace($_SERVER["DOCUMENT_ROOT"], "", __DIR__)."/";


?>
<!--
<link rel="stylesheet" type="text/css" href="/bitrix/templates/rozn2016/template_styles.css" />
<link rel="stylesheet" type="text/css" href="/bitrix/templates/rozn2016/styles.css" />
-->
<link rel="stylesheet" type="text/css" href="<?=$path?>style.css?<?=rand(1,10000)?>" />
<link rel="stylesheet" type="text/css" href="/bitrix/css/main/font-awesome.css" />
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/bitrix/templates/rozn2016/assets/jquery.maskedinput.min.js"></script>

<script>
$(document).ready( function() {
		$("input.reg_PERSONAL_PHONE").mask('8 (999) 999-99-99');
	});
</script>

<div class="bx-authform mainreg">

	<div class="form-title">
		<span class="ico"></span>
		<span class="text">Заполните форму и получите<br>скидку 10%<br>на первый заказ</span>
	</div>



<?if($USER->IsAuthorized()):?>

	<p class="is-auth"><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
	<p style="padding: 0 30px;"><a href="" onclick="javascript: window.parent.$.fancybox.close(); window.parent.document.location.href = window.parent.document.location.href;">Перейти на сайт</a></p>
	
<?else:?>
	
	<?
	if(count($arResult["ERRORS"]) > 0):
		foreach ($arResult["ERRORS"] as $key => $error)
			if (intval($key) == 0 && $key !== 0) $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
		?>
		<div class="alert alert-danger"><?=implode("<br>", $arResult["ERRORS"])?></div>
		<?
	elseif(($arResult["USE_EMAIL_CONFIRMATION"] === "Y") && ($arResult["VALUES"]["USER_ID"] > 0)):
	?>
		<div class="alert alert-success"><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></div>
	<?endif?>



	
	
	

	<?if(!$arResult["VALUES"]["USER_ID"]):?>

		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">

			<?
			if($arResult["BACKURL"] <> ''):
			?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?
			endif;
			?>

			<input size="30" type="hidden" name="REGISTER[LOGIN]" value="USER_<?=time()?>_<?=rand(1,10000)?>" />











			<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
				<?if($FIELD == "LOGIN") continue;?>
				<?//if($FIELD == "CONFIRM_PASSWORD") continue;?>
				
				<?
				switch ($FIELD)
				{
					case "PASSWORD":
						?>
						<div class="bx-authform-formgroup-container" style="margin-top:25px;">
							<div class="bx-authform-input-container">
								<?if($arResult["SECURE_AUTH"]):?>
												<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

								<script type="text/javascript">
									document.getElementById('bx_auth_secure').style.display = '';
								</script>
								<?endif?>
								<i class="fa fa-key"></i><input placeholder="Пароль" type="password" name="REGISTER[<?=$FIELD?>]" maxlength="255" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" /><span class="bx-authform-starrequired">*</span>
							</div>
						</div>
						<?
						break;
						
					case "CONFIRM_PASSWORD":
						?>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-input-container">
								<?if($arResult["SECURE_AUTH"]):?>
												<div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><?echo GetMessage("AUTH_SECURE_NOTE")?></div></div>
								<script type="text/javascript">
								document.getElementById('bx_auth_secure_conf').style.display = '';
								</script>
								<?endif?>
								<i class="fa fa-key"></i><input placeholder="Подтвердите пароль" type="password" name="REGISTER[<?=$FIELD?>]" class="reg_<?=$FIELD?>" maxlength="255" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" /><span class="bx-authform-starrequired">*</span>
							</div>
						</div>
						
						
						
						
						<?
						break;
				
					default:
					
						?>
						<div class="bx-authform-formgroup-container">
							<div class="bx-authform-input-container">
								<?
								$icon = "user";
								if($FIELD == "PERSONAL_PHONE") $icon = "phone";
								if($FIELD == "EMAIL") $icon = "envelope-o";
								?>
								<i class="fa fa-<?=$icon?>"></i><input placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>" type="text" name="REGISTER[<?=$FIELD?>]" class="reg_<?=$FIELD?>" maxlength="255" value="<?=$arResult["VALUES"][$FIELD]?>" /><?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="bx-authform-starrequired">*</span><?endif?>
							</div>
						</div>
						<?
				}
						
				
			endforeach?>






			<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
				<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>

					<div class="bx-authform-formgroup-container">
						<div class="bx-authform-label-container"><?if ($arUserField["MANDATORY"]=="Y"):?><span class="bx-authform-starrequired">*</span><?endif?><?=$arUserField["EDIT_FORM_LABEL"]?></div>
						<div class="bx-authform-input-container">
							<?
							$APPLICATION->IncludeComponent(
								"bitrix:system.field.edit",
								$arUserField["USER_TYPE"]["USER_TYPE_ID"],
								array(
									"bVarsFromForm" => $arResult["bVarsFromForm"],
									"arUserField" => $arUserField,
									"form_name" => "bform"
								),
								null,
								array("HIDE_ICONS"=>"Y")
							);
							?>
						</div>
					</div>

				<?endforeach;?>
			<?endif;?>



			<?if ($arResult["USE_CAPTCHA"] == "Y"):?>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

					<div class="bx-authform-formgroup-container">
						<div class="bx-authform-label-container">
							<span class="bx-authform-starrequired">*</span> <?=GetMessage("REGISTER_CAPTCHA_TITLE")?>
						</div>
						<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
						<div class="bx-authform-input-container">
							<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" placeholder="Введите текст с картинки" />
						</div>
					</div>

			<?endif?>



			<div class="bx-authform-formgroup-container">
				<input type="submit" class="btn btn-primary" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" onclick="yaCounter38114470.reachGoal('REGISTR1'); return true;"/>
			</div>

			<?/*?><hr class="bxe-light"><?*/?>

			<div class="bx-authform-description-container">
				<p class="form-note">
					Придумайте пароль не менее 6 символов.<br>Звездочкой отмечены поля, обязательные для заполнения.
				</p>
			</div>

			<?/*?>
			<div class="bx-authform-description-container">
				<small><span class="bx-authform-starrequired">*</span><?=GetMessage("AUTH_REQ")?></small>
			</div>

			<div class="bx-authform-link-container">
				<small><a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a></small>
			</div>
			<?*/?>





		</form>
		
	<?endif?>
	
	
	
	
	
	
	
	
	
	

	
<?endif?>
</div>

<div style="clear:both;"></div>

<?die();?>