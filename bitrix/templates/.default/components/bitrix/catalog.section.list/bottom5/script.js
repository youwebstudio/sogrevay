$(document).ready(function(){
	
	var limit = 752;
	var w = $('.bx-wrapper').width();
	
	if(w < limit) 
		$('.bottom-section-list').hide()
			.parent().find('h3').css('cursor', 'pointer')
			.find('.fa').show();
	else 
		$('.bottom-section-list').show()
			.parent().find('h3').css('cursor', 'default')
			.find('.fa').hide();
	
	$('.footer-menu-block h3').on("click", function() {
		if(w < limit){
			if($(this).next().is(':visible')) $(this).find("i").removeClass("fa-angle-up").addClass("fa-angle-down");
			else $(this).find("i").removeClass("fa-angle-down").addClass("fa-angle-up");
			$(this).next().toggle(200);
			}
		else $(this).css('cursor', 'default');
		});
	
	$(window).resize( function() {
		w = $('.bx-wrapper').width();
		if(w < limit) 
			$('.bottom-section-list').hide()
				.parent().find('h3').css('cursor', 'pointer')
				.find('.fa').show();
		else 
			$('.bottom-section-list').show()
				.parent().find('h3').css('cursor', 'default')
				.find('.fa').hide();
		});
	
});