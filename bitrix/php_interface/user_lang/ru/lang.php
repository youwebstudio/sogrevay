<?
/* /bitrix/modules/main/lang/ru/classes/general/user.php" */
$MESS["/bitrix/modules/main/lang/ru/classes/general/user.php"]["DATA_NOT_FOUND"] = "Указанный E-Mail не найден.";
$MESS["/bitrix/modules/main/lang/ru/classes/general/user.php"]["WRONG_LOGIN"] = "Неверный E-Mail или пароль.";
$MESS["/bitrix/modules/main/lang/ru/classes/general/user.php"]["PASSWORD_CHANGE_OK"] = "Пароль успешно сменен.<br>На ваш E-Mail высланы новые регистрационные данные.";
$MESS["/bitrix/modules/main/lang/ru/classes/general/user.php"]["CHECKWORD_INCORRECT"] = "Неверное контрольное слово для пользователя с E-Mail \"#LOGIN#\".";

/* /bitrix/components/bitrix/sale.order.ajax/lang/ru/component.php" */
$MESS["/bitrix/components/bitrix/sale.order.ajax/lang/ru/component.php"]["STOF_ERROR_REG_FLAG"] = "Ошибка регистрации нового пользователя: Введите желаемый E-Mail";
$MESS["/bitrix/components/bitrix/sale.order.ajax/lang/ru/component.php"]["STOF_ERROR_REG_FLAG1"] = "Ошибка регистрации нового пользователя: введите желаемый пароль";
?>