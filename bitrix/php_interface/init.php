<?
// Настройки для розничного сайта
define("PRODUCTS_IBLOCK_ID", 4);		// ID инфоблока товаров
define("OFFERS_IBLOCK_ID", 5);			// ID инфоблока торговыйх предложений

define("PROPERTY_CML2_LINK", 65);
define("COLOR_PROP_ID", 75);
define("SIZE_PROP_ID", 76);

define("PROPERTY_CML2_LINK_ID", 65);	// ID свойства привязки предложения к товару
define("PROPERTY_BRAND_ID", 81);		// ID свойства BRAND типа список (откуда будем переносить, см.далее)
define("PROPERTY_BRAND_REF_ID", 83);	// ID свойства BRAND_REF типа справочник (куда будем переносить, см.далее)
define("PROPERTY_FEATURES_ID", 68);		// ID свойства FEATURES (характеристики, пришедшие из 1с)
define("PROPERTY_COLOR_ID", 75);		// ID свойства COLOR
define("PROPERTY_SIZE_ID", 76);			// ID свойства SIZE
define("PROPERTY_TONE_ID", 80);			// ID свойства TONE (ОТТЕНКИ)
define("PROPERTY_NOVIZNA_ID", 63);		// ID свойства NOVIZNA 
 
define("VESNUSHKI_VALUE", 169);			// ID значения "VESNUSHKI" свойства BRAND_REF
define("VAYKIDS_VALUE", 170);			// ID значения "VAYKIDS" свойства BRAND_REF
define("VAY_VALUE", 171);				// ID значения "VAY" свойства BRAND_REF
define("MAIN_CATALOG_VALUE", 21);		// ID значения "Основной каталог" свойства NOVIZNA 

define("SUBSCRIBER_USER_GROUP_ID", 8);	// ID группы подписчиков

define("NOFOTO_FILE_ID", 14073);   		// ID файла в медиабиблиотеке для показа вместо отсутствующего фото

//die("Updating...");

$GLOBALS["arrFilterTrends"] = Array("IBLOCK_ID" => PRODUCTS_IBLOCK_ID, ">PROPERTY_SHOW_IN_TRENDS" => 0, "ACTIVE" => "Y");



/*
function custom_mail($to,$subject,$body,$headers) {
	$f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
	fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
	fclose($f);
	return mail($to,$subject,$body,$headers);
}
*/



if($_GET["utm_term"] == "sale2017")
{
	$f = fopen($_SERVER["DOCUMENT_ROOT"]."/substat.csv", "a+");
	fwrite($f, date("d.m.Y H:i:s").";".$_SERVER['REMOTE_ADDR'].";https://www.sogrevay.ru".$_SERVER["REQUEST_URI"].";".$_SERVER["HTTP_REFERER"]."\n");
	fclose($f); 
}



/*
$f = fopen($_SERVER["DOCUMENT_ROOT"]."/log.csv", "a+");
fwrite($f, date("d.m.Y H:i:s").";".$_SERVER['REMOTE_ADDR'].";https://www.sogrevay.ru".$_SERVER["REQUEST_URI"].";".$_SERVER["HTTP_REFERER"]."\n");
fclose($f); 
*/



/*
AddEventHandler("sale", "OnBeforeBasketAdd", "OnBeforeBasketAddHandler");
function OnBeforeBasketAddHandler(&$arFields)
{
	die();
	global $APPLICATION;
    $APPLICATION->throwException("Временно недоступно");
    return false;
}
*/



// ФУНКЦИЯ установки товара дня, один в женском, один в детском, старые товары дня становятся обычными

function dayThingSet()
{
	$PRODUCT_IBLOCK_ID = 4;	
	$VAL_NOVINKA = 20;				// смотреть в свойствах инфоблока товаров
	$VAL_DAY_THING_YES = 3516; 		// смотреть в свойствах инфоблока товаров
	$ZHEN_RAZDEL = 46;
	$DET_RAZDEL = 52;	
	$PRICE_LIMIT_ZHEN = 0;       // выбирать товары не дешевле PRICE_LIMIT_ZHEN женские
	$PRICE_LIMIT_DET = 1500;       	// выбирать товары не дешевле PRICE_LIMIT_DET детские
	
	CModule::IncludeModule('iblock');

	// выберем ID товара дня из новинок в женском каталоге
	$arSet = Array();
	$dbRes = CIBlockElement::GetList(Array("RAND" => "ASC"), Array("IBLOCK_ID" => $PRODUCT_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_NOVIZNA" => $VAL_NOVINKA, "SECTION_ID" => $ZHEN_RAZDEL, "INCLUDE_SUBSECTIONS" => "Y", ">PROPERTY_MIN_PRICE" => $PRICE_LIMIT_ZHEN, "!PROPERTY_AKCIYA_ON_SITE" => "Y"), false,  Array ("nTopCount" => 1), Array("ID", "NAME", "PROPERTY_NOVIZNA", "PROPERTY_MIN_PRICE", "PROPERTY_AKCIYA_ON_SITE"));
	while($arRes = $dbRes->GetNext())
	{
		$arSet[$arRes["ID"]] = $arRes["NAME"];
	}

	// выберем ID товара дня из новинок в детском каталоге
	$dbRes = CIBlockElement::GetList(Array("RAND" => "ASC"), Array("IBLOCK_ID" => $PRODUCT_IBLOCK_ID, "ACTIVE" => "Y", /*"PROPERTY_NOVIZNA" => $VAL_NOVINKA, */"SECTION_ID" => $DET_RAZDEL, "INCLUDE_SUBSECTIONS" => "Y", ">PROPERTY_MIN_PRICE" => $PRICE_LIMIT_DET, "!PROPERTY_AKCIYA_ON_SITE" => "Y"), false,  Array ("nTopCount" => 1), Array("ID", "NAME", "PROPERTY_NOVIZNA", "PROPERTY_MIN_PRICE", "PROPERTY_AKCIYA_ON_SITE"));
	while($arRes = $dbRes->GetNext())
	{
		$arSet[$arRes["ID"]] = $arRes["NAME"];
	}

	// выберем ID старых товаров дня для изменения свойства
	$arDel = Array();
	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PRODUCT_IBLOCK_ID, "ACTIVE" => "Y", "PROPERTY_DAY_THING_VALUE" => "Да"), false,  false, Array("ID", "NAME", "PROPERTY_DAY_THING", "ACTIVE"));
	while($arRes = $dbRes->GetNext())
	{
		$arDel[$arRes["ID"]] = $arRes["ACTIVE"];
	}
	
	//prn($arSet);
	//prn($arDel);
	
	// стираем свойство DAY_THING у старых товаров дня
	foreach($arDel as $id => $act)
	{
		CIBlockElement::SetPropertyValuesEx($id, false, array("DAY_THING" => ""));
	}

	// устанавливаем новые товары дня
	foreach($arSet as $id=>$name)
	{
		CIBlockElement::SetPropertyValuesEx($id, false, array("DAY_THING" => $VAL_DAY_THING_YES));
	}
	
	// ОЧЕНЬ ВАЖНО! Указываем что кеш для инфоблока товаров нужно будет обновить
	CIBlock::clearIblockTagCache($PRODUCT_IBLOCK_ID);
	
	// мессадж об изменении
	mail("info@sogrevay.ru", "Обновлены товары дня на SOGREVAY.RU", "Установлены новые товары дня: ".implode(", ", $arSet), "Content-type: text/html; charset=utf-8");
	
	return "dayThingSet();";
	
}




// Функция делающая переасчет smartSort, сохраняет в товарах

function smartSortSet()
{
	$PRODUCT_IBLOCK_ID = 4;	
	
	CModule::IncludeModule('iblock');
	$dbRes = CIBlockElement::GetList(Array("PROPERTY_CML2_ARTICLE" => "DESC"), Array("IBLOCK_ID" => $PRODUCT_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("ID", "ACTIVE", "NAME", "SORT", "PROPERTY_NOVIZNA", "PROPERTY_SEZON", "PROPERTY_AKCIYA_ON_SITE", "PROPERTY_CML2_ARTICLE", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO"));
	while($arRes = $dbRes->GetNext())
	{
		$name = $arRes["NAME"]." / ".$arRes["PROPERTY_SEZON_VALUE"]." / ".$arRes["PROPERTY_NOVIZNA_VALUE"]." / ".$arRes["PROPERTY_CML2_ARTICLE_VALUE"]." / ".$arRes["IBLOCK_SECTION_ID"];
		$sortInt = 0;
		
		//if($arRes["PROPERTY_SEZON_VALUE"] == "Весна-Лето") $sortInt = 200000000; else $sortInt = 100000000; 
		if($arRes["PROPERTY_NOVIZNA_VALUE"] == "Новинка") $sortInt +=  200000000;
		//if($arRes["PROPERTY_AKCIYA_ON_SITE_VALUE"] == "Y") $sortInt += 100000000;
		
		if($arRes["IBLOCK_SECTION_ID"] == 77) $sortInt += 10000000;
		if($arRes["IBLOCK_SECTION_ID"] == 50) $sortInt += 1000000;
		if($arRes["IBLOCK_SECTION_ID"] == 78) $sortInt += 100000;
		if($arRes["IBLOCK_SECTION_ID"] == 49) $sortInt += 10000;
		if($arRes["IBLOCK_SECTION_ID"] == 47) $sortInt += 1000;
		if($arRes["IBLOCK_SECTION_ID"] == 48) $sortInt += 100;
		if($arRes["PROPERTY_NOVIZNA_VALUE"] == "Распродажа") $sortInt += 10;
		if($arRes["PROPERTY_NOVIZNA_VALUE"] == "Товары со скидкой") $sortInt += 10;
		$sortInt = $sortInt + intVal($arRes["PROPERTY_CML2_ARTICLE_VALUE"]);
		
		// если нет картинок то sortInt = артикул
		if((count($arRes["PROPERTY_MORE_PHOTO_VALUE"]) < 1) && (strLen($arRes["PREVIEW_PICTURE"]) < 1) && (strLen($arRes["DETAIL_PICTURE"]) < 1))
		{
			$sortInt = intVal($arRes["PROPERTY_CML2_ARTICLE_VALUE"]);
		}
		
		// если обычная сортировка выше 1000 значит это ручной вынос товара вверх, прибавим 20000000 чтобы быть выше всех
		if($arRes["SORT"] > 1000) $sortInt = 200000000 + $arRes["SORT"];
		
		$arr[$sortInt]= $name;
	
		$ELEMENT_ID = $arRes["ID"];  // код элемента
		$PROPERTY_CODE = "SMART_SORT";  // код свойства
		$PROPERTY_VALUE = $sortInt;  // значение свойства
		
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
	}
	CIBlock::clearIblockTagCache($PRODUCT_IBLOCK_ID);
	
	//krsort($arr);
	//prn($arr);
	
	//mail("turtell@yandex.ru", "SmartSortSet на Sogrevay.ru", "Откорректирована smartSort");
	
}





// Функция подчистки "дыр" в каталоге (деактивация товаров без торговых предложений)
// функция-агент, работает раз в минуту

function cleanGoods() 
{
	$PROUDCTS_IBLOCK_ID = 4;
	$OFFERS_IBLOCK_ID = 5;
	
	$SECTION_ID = 154;
	
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");

	$arActiveGoods = Array();
	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $OFFERS_IBLOCK_ID), false, false, Array("ID", "PROPERTY_CML2_LINK", "PROPERTY_CML2_LINK.ACTIVE", "ACTIVE", "CATALOG_QUANTITY"));
	while($arRes = $dbRes->Fetch())
	{
		if(strLen($arRes["PROPERTY_CML2_LINK_VALUE"]) < 1) continue;
		if($arRes["CATALOG_QUANTITY"] < 1) continue;
		// если торговое предложение >0 и связь с товаром есть, то товар по любому должен быть активен
		$arActiveGoods[$arRes["PROPERTY_CML2_LINK_VALUE"]] = "Y";
	}
	//$arActiveGoods[99777] = "N";   // артикул 4323 прибиваем по любому до конца разбирателсьтв что с ним

	$arTotalGoods = Array();
	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PROUDCTS_IBLOCK_ID), false, false, Array("ID", "ACTIVE", "IBLOCK_SECTION_ID"));
	while($arRes = $dbRes->Fetch())
	{
		if(($arRes["ACTIVE"] == "Y") && ($arActiveGoods[$arRes["ID"]] != "Y")) $arTotal[$arRes["ID"]] = "N";
		if(($arRes["ACTIVE"] == "N") && ($arActiveGoods[$arRes["ID"]] == "Y")) $arTotal[$arRes["ID"]] = "Y";
		if(($arRes["IBLOCK_SECTION_ID"] == $SECTION_ID)&&($arRes["ACTIVE"] == "Y")) $arTotal[$arRes["ID"]] = "N";   // деактивируем если заданный раздел (НОВЫЕ КОЛЛЕКЦИИ)
		if(($arRes["IBLOCK_SECTION_ID"] == $SECTION_ID)&&($arRes["ACTIVE"] == "N")) unset($arTotal[$arRes["ID"]]);   // деактивируем если заданный раздел (НОВЫЕ КОЛЛЕКЦИИ)
	}

	$str = "";
	if(count($arTotal) > 0)
	{
		foreach($arTotal as $id => $act)
		{
			$el = new CIBlockElement; 
			$res = $el->Update($id, Array("ACTIVE" => $act));
			if($res) $str .= date('d.m.Y H:i:s')." - ".$id." change active to ".$act."\n";//date('d.m.Y H:i:s')." - успешно перевели ".$id." в состояние ".$act."\n");
			else $str .= date('d.m.Y H:i:s')." - ERROR ACTIVATE ".$id."\n";//date('d.m.Y H:i:s')." - ОШИБКА: ".$id." ---> ".$act."\n");
		}
	}
	if(strLen($str) > 0) mail("turtell@yandex.ru", "CleanGoods on Sogrevay", $str);
	return "cleanGoods();";
}




// получение гео-данных от сервисе Geo-IP
function get_geo_data($ip) 
{
	$arParams["IP"] = $ip;
	//$arParams['IP'] = "178.217.24.5"; // подольск
	$strQueryText = QueryGetData(
		"194.85.91.253",
		8090,
		"/geo/geo.html",
		"address=<ipquery><fields><all/></fields><ip-list><ip>".$arParams['IP']."</ip></ip-list></ipquery>",
		$error_number,
		$error_text,
		"POST"
	   );
	$objXML  = new CDataXML();
	$objXML->LoadString($strQueryText);
	$arData = $objXML->GetArray();
	$arOneData = $arData['ip-answer']['#']['ip'][0];
	if(is_array($arOneData)) 
	{
		$arResult['FULL_INFO'] = Array(
			"IP"=>$arOneData['@']['value'],
			"CITY"=>$arOneData['#']['city']['0']['#'],
			"INET_STATUS"=>$arOneData['#']['inet-status']['0']['#'],
			"INET_DESCR"=>$arOneData['#']['inet-descr']['0']['#'],
			"INETNUM"=>$arOneData['#']['inetnum']['0']['#'],
			"REGION"=>$arOneData['#']['region']['0']['#'],
			"DISTRICT"=>$arOneData['#']['district']['0']['#'],
			"LAT"=>$arOneData['#']['lat']['0']['#'],
			"LNG"=>$arOneData['#']['lng']['0']['#']
			);
	}
	return $arResult['FULL_INFO'];
}




 



// функции распечатки

function prn($str){
	echo '<pre style="text-align:left; border:1px solid black; background-color: #eee; color:black; z-index:10000000;">';
	print_r($str);
	echo '</pre>';
	}

function prd($str){
	echo '<pre style="text-align:left; border:1px solid black; background-color: #eee; color:black;">'; 
	print_r($str);
	echo '</pre>';
	die();
	}
	
function pra($str) {
	global $USER;
	if($USER->isAdmin()) {
		prn($str);
		}
	}
	
function prn1($str) {
	global $USER;
	if($USER->GetID() == 1) {
		prn($str);
		}
	}
	
function prd1($str) {
	global $USER;
	if($USER->GetID() == 1) {
		prn($str);
		}
	die();
	}






// =======================================================================================================================
// Отправка сообщения о добавлении нового комментария
// =======================================================================================================================	
AddEventHandler("blog", "OnCommentAdd", "OnCommentAddHandler");
function OnCommentAddHandler($ID, $arFields)
{
	global $USER;
	
	//mail("turtell@yandex.ru", "Добавлен комментарий ".$ID, implode("<br/>", $arFields));
	$t = $arFields["PATH"];
	$t = explode("?", $t);
	$url = str_replace(":80", "", $t[0]);
	$url = str_replace(":443", "", $url);
	
	$arBlogPost = CBlogPost::GetByID($arFields["POST_ID"]);
	$arEventFields = array(
		"ID"   => $ID,
		"EMAIL_TO" => "info@sogrevay.ru",
		"COMMENT_PRODUCT" => $arBlogPost["TITLE"],
		"COMMENT_TEXT" => $arFields["POST_TEXT"],
		"COMMENT_PATH" => $url
		);
	CEvent::Send("NEW_BLOG_COMMENT", "s1", $arEventFields);
	
	if($USER->isAuthorized())
	{
		$arFields["AUTHOR_NAME"] = $USER->GetFullname();
	}
	
	// добавляем запись о комментарии в инфоблок
	$el = new CIBlockElement;
	$PROP = array(
		"COMMENT_ID" => $ID,
		"POST_ID" => $arFields["POST_ID"],
		"AUTHOR" => $arFields["AUTHOR_NAME"],
		"PRODUCT" => $arEventFields["COMMENT_PRODUCT"],
		"URL" => $arEventFields["COMMENT_PATH"],
		"SHOW_ON_MAINPAGE" => ''//4815 // значение YES для свойства
	);
	$arLoadProductArray = Array(
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"      => 13,   // инфоблок для копий отзывов и указания какие выводить на главной
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => "Отзыв №".$ID,
		"ACTIVE"         => "Y",  
		"DETAIL_TEXT"    => $arFields["POST_TEXT"]
	  );
	$el->Add($arLoadProductArray);
	
	
	//prd($arFields);
	//[ID] => 371
    //[EMAIL_TO] => info@sogrevay.ru
    //[COMMENT_PRODUCT] => Жилет для дев. арт.543
    //[COMMENT_TEXT] => Соглашусь!
    //[COMMENT_PATH] => https://www.sogrevay.ru/catalog/detskiy-trikotazh/dlya-devochek/vay-kids/zhilety/zhilet-dlya-devochek-543/
	
	//[POST_ID] => 110
    //[BLOG_ID] => 1
    //[POST_TEXT] => Отличный рисунок!
    //[DATE_CREATE] => 07.06.2017 19:47:34
    //[AUTHOR_IP] => 194.28.29.223
    //[AUTHOR_IP1] => 194.28.29.223
    //[URL] => catalog_comments
    //[AUTHOR_ID] => 1
    //[PARENT_ID] => 
    //[PATH] => https://www.sogrevay.ru:443/catalog/detskiy-trikotazh/dlya-devochek/vay-kids/zhilety/zhilet-dlya-devochek-543/?commentId=#comment_id###comment_id#
	

	
	/*
	[POST_ID] => 1415
    [BLOG_ID] => 1
    [POST_TEXT] => опять я
    [DATE_CREATE] => 02.11.2016 18:15:42
    [AUTHOR_IP] => 194.28.29.223
    [AUTHOR_IP1] => 194.28.29.223
    [URL] => catalog_comments
    [AUTHOR_ID] => 1
    [PARENT_ID] => 
    [PATH] => http://www.sogrevay.ru:80/catalog/zhenskiy-trikotazh/dzhempery/dzhemper-zhen-4323/?commentId=#comment_id###comment_id#
	*/
}

	
	
	
	
	

	
// =======================================================================================================================
// Функция отправки СМС через сеовис smsc.ru, логин newvay-shop пароль newvay0530
// =======================================================================================================================
function sendSMS($number, $txt)
{
	if( $curl = curl_init() ) 
	{
		$txt = iconv("utf-8", "windows-1251", $txt);
		curl_setopt($curl, CURLOPT_URL, 'https://smsc.ru/sys/send.php');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, "login=newvay-shop&psw=".md5("newvay0530")."&phones=".$number."&mes=".urlencode($txt));
		$out = curl_exec($curl);
		//echo $out;
		curl_close($curl);
		if(substr($out, 0, 2) == "OK") return true;
		else return false;
	}
}

	
	

// =======================================================================================================================
// автоматически сгенерируем e-mail если пользователь не указал его при оформлении заказа
// =======================================================================================================================
	
AddEventHandler("sale", "OnSaleComponentOrderOneStepProcess", "OnSaleComponentOrderOneStepProcessHandler");
function OnSaleComponentOrderOneStepProcessHandler(&$arResult, &$arUserResult, &$arParams)
{
	
	if(strLen($arUserResult["ORDER_PROP"][2])>0)
	{
		$arUserResult["USER_EMAIL"] = $arUserResult["ORDER_PROP"][2];
		$arResult["ORDER_DATA"]["USER_EMAIL"] = $arUserResult["USER_EMAIL"];
	}
	else
	{
		$arUserResult["USER_EMAIL"] = "user".time()."@sogrevay.ru";
		$arResult["ORDER_DATA"]["USER_EMAIL"] = $arUserResult["USER_EMAIL"];
	}
	//prn($arResult);
	//prd($arUserResult);
}



AddEventHandler("sale", "OnSaleComponentOrderProperties", "OnSaleComponentOrderPropertiesHandler");
function OnSaleComponentOrderPropertiesHandler(&$arUserResult, $request, &$arParams, &$arResult)
{
	global $USER;
	if( (strLen($arUserResult["ORDER_PROP"][2]) < 1) && (!$USER->isAuthorized()) )
	{
		if(trim($arUserResult["ORDER_PROP"][2]) == "") $arUserResult["ORDER_PROP"][2] = "user".time()."@sogrevay.ru";
		//$arUserResult["ORDER_PROP"][2] = "user".time()."@sogrevay.ru";
		//$arUserResult["USER_EMAIL"] = $arUserResult["ORDER_PROP"][2];
		//if(strLen(trim($arUserResult["ORDER_PROP"][2])) < 1)
		//{
			
		//}
	}
	//prd($arUserResult);
	//prd($request);
}




// =======================================================================================================================
// Внесение кода службы доставки в свойство заказа "DELIVERY_CODE" при добавлении/изменении заказа
// =======================================================================================================================
AddEventHandler("sale", "OnOrderSave", "OnOrderSaveHandler");
function OnOrderSaveHandler($orderID, $arFields, $arOrder)
{
	setDeliveryCode($orderID);
}




// =======================================================================================================================
// Внесение кода службы доставки в свойство заказа "DELIVERY_CODE"
// (используем в событии OnOrderSave - стало неорбходимым после конвертации магазина)
// =======================================================================================================================
function setDeliveryCode($ORDER_ID)
{
	CModule::includeModule("sale");
	$PROPERTY_CODE = "DELIVERY_CODE";
	
	$arOrder = CSaleOrder::GetByID($ORDER_ID);
	$DELIVERY_CODE = $arOrder["DELIVERY_ID"];

	$dbRes = CSaleOrderPropsValue::GetList(array("SORT" => "ASC"), array("ORDER_ID" => $ORDER_ID, "CODE" => $PROPERTY_CODE));
	if($arRes = $dbRes->Fetch())
	{
		//if($res = CSaleOrderPropsValue::Update($arRes["ID"], Array("VALUE" => $DELIVERY_CODE))) prn("Изменили на $DELIVERY_CODE");
		$res = CSaleOrderPropsValue::Update($arRes["ID"], Array("VALUE" => $DELIVERY_CODE));
	}
	else
	{
		$dbRes = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("CODE" => $PROPERTY_CODE));
		while($arRes = $dbRes->Fetch()) $PROPERTY_ID = $arRes["ID"];
		$arFields = array(
			"ORDER_ID" => $ORDER_ID,
			"ORDER_PROPS_ID" => $PROPERTY_ID,
			"NAME" => "Код службы доставки",
			"CODE" => "DELIVERY_CODE",
			"VALUE" => $DELIVERY_CODE
		);
		//if($res = CSaleOrderPropsValue::Add($arFields)) prn("Добавили $DELIVERY_CODE");
		$res = CSaleOrderPropsValue::Add($arFields);
	}
}







// =======================================================================================================================
// перенос e-mail в поле LOGIN при регистрации нового пользователя
// а также пароля в UF_PWD для будущей отсылки в открытом виде (СМС)
// =======================================================================================================================
	
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserAdd", "OnBeforeUserUpdateHandler");
function OnBeforeUserUpdateHandler(&$arFields)
{
	// если регистрация при оформлении заказа
	if((strLen($_REQUEST["ORDER_PROP_3"])>0)&&($_REQUEST["confirmorder"]=="Y"))
	{
		$arFields["PERSONAL_PHONE"] = trim($_REQUEST["ORDER_PROP_3"]);
	}
	
	if($arFields["EXTERNAL_AUTH_ID"] != "socservices")/*&&(strpos(" ".$arFields["LOGIN"], "@") === false)) */
	{
		$arFields["LOGIN"] = $arFields["EMAIL"];
	}

	if($arFields["PASSWORD"]) $arFields["UF_PWD"] = $arFields["PASSWORD"];
	
	if(substr($arFields["PERSONAL_PHONE"], 0, 2) == "+7") $arFields["PERSONAL_PHONE"] = "8 ".substr($arFields["PERSONAL_PHONE"], 2);
	$arFields["PERSONAL_PHONE"] = str_replace("  ", " ", $arFields["PERSONAL_PHONE"]);
	
	return $arFields;
}







// =======================================================================================================================
// функция подмены при необходимости логина при авторизации с user76868686 на user76868686@newvay-shop.ru 
//        (т.е. можно авторизоваться по части логина до @, если ящик с @newvay-shop.ru)
// =======================================================================================================================
	
AddEventHandler("main", "OnBeforeUserLogin", "OnBeforeUserLoginHandler");
function OnBeforeUserLoginHandler(&$arFields)
{
	$rs = CUser::GetByLogin($arFields["LOGIN"]);
	$ar = $rs->Fetch();
	if(!$ar["ID"]) 
	{
		$rs1 = CUser::GetByLogin($arFields["LOGIN"]."@sogrevay.ru");
		$ar1 = $rs1->Fetch();
		if($ar1["ID"]) 
		{
			$arFields["LOGIN"] = $arFields["LOGIN"]."@sogrevay.ru";
		}
	}
	return $arFields;
}







// =======================================================================================================================
// добавление пользователя в группу подписчиков в зависимости от значения UF_SUBSCRIBE (пользовательское поля для USER)
// =======================================================================================================================

AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserUpdateHandler");
AddEventHandler("main", "OnAfterUserUpdate", "OnAfterUserUpdateHandler");
function OnAfterUserUpdateHandler(&$arFields)
{
	if(isset($arFields["UF_SUBSCRIBE"]))
	{
		$USER_ID = intVal($arFields["ID"]);
		$arGroups = CUser::GetUserGroup($USER_ID);
		
		if($arFields["UF_SUBSCRIBE"] > 0) 
		{
			if(!in_array(SUBSCRIBER_USER_GROUP_ID, $arGroups))
			{
				$arGroups[] = SUBSCRIBER_USER_GROUP_ID;
				CUser::SetUserGroup($USER_ID, $arGroups);
			}
		}
		else
		{
			if(in_array(SUBSCRIBER_USER_GROUP_ID, $arGroups))
			{
				$arNewGroups = Array();
				for($i=0; $i<count($arGroups); $i++) if($arGroups[$i] != SUBSCRIBER_USER_GROUP_ID) $arNewGroups[] = $arGroups[$i];
				CUser::SetUserGroup($USER_ID, $arNewGroups);
			}

		}
	}
}



// =======================================================================================================================
//  Отправка СМС, при рагистрации или просто добавлении нового пользователя с логином и паролем
// =======================================================================================================================

AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserAddSendSMS");
function OnAfterUserAddSendSMS($arFields) 
{
	if($arFields["ID"] > 0)
	{
	
		if($_SERVER["SCRIPT_URL"] == "/reg_popup/")
		{
			// Генерация Купона
			$DISCOUNT_ID = 9;  // скидка при всплывайке
			CModule::IncludeModule("catalog");
			$arDiscount = CCatalogDiscount::GetByID($DISCOUNT_ID);
			if($arDiscount["ACTIVE"] == "Y")
			{
				$COUPON = CatalogGenerateCoupon()."-".$arFields["ID"];
				$arCouponFields = array(
					"DISCOUNT_ID" => $DISCOUNT_ID,
					"ACTIVE" => "Y",
					"ONE_TIME" => "O",       // на один заказ
					"COUPON" => $COUPON
				);
				$CID = new CCatalogDiscountCoupon();
				if(!$CID->Add($arCouponFields)) $COUPON = "";
			}
			//prn($COUPON);
			//die("coupon");
			//$COUPON = "";
		}

		//prn($_SERVER);
		//die("here");
		
		
		if($arFields["PERSONAL_PHONE"])
		{
			$login = $arFields["LOGIN"];
			if((substr($login, 0, 4) == "user")&&(strpos(" ".$login, "@sogrevay.ru")>0))
			{
				$login = substr($login, 0, strpos($login, "@"));
			}
			
			//$txt = "Newvay-shop.ru\nLogin: ".$login."\nPassword: ".$arFields["UF_PWD"];
			$txt = "Sogrevay.ru\nLogin: ".$login."\nPassword: ".$arFields["UF_PWD"];
			if(!sendSMS($arFields["PERSONAL_PHONE"], $txt))
			{
				mail("turtell@yandex.ru", "Ошибка отправки смс с сайта [to ".$arFields["PERSONAL_PHONE"]."]", $txt);
			}
			else 
				mail("turtell@yandex.ru", "Отправка смс с сайта [to ".$arFields["PERSONAL_PHONE"]."]", $txt);
			
			/*
			if(strLen($COUPON)>0) 
			{
				$txt = "Ваш промокод на скидку -20%: ".$COUPON;
				if(!sendSMS($arFields["PERSONAL_PHONE"], $txt))
				{
					mail("turtell@yandex.ru", "Ошибка отправки смс с сайта [to ".$arFields["PERSONAL_PHONE"]."]", $txt);
				}
				else 
					mail("turtell@yandex.ru", "Отправка смс с сайта [to ".$arFields["PERSONAL_PHONE"]."]", $txt);
			}
			*/
		}
	
	}
	
	//prn($arFields);
	//die("here");
}








/*
AddEventHandler("main", "OnSendUserInfo", "MyOnSendUserInfoHandler"); 
function MyOnSendUserInfoHandler(&$arParams) 
{ 
	//$arParams["USER_ID"]
	$USER_ID = $arParams["USER_ID"];
	$DISCOUNT_ID = 9;
	CModule::IncludeModule("catalog");
	$dbDiscount = CCatalogDiscount::GetList(Array(), Array("DISCOUNT_ID" => $DISCOUNT_ID, "COUPON_ACTIVE" => "Y"));
	//$arCoupons = Array();
	while($arDiscount = $dbDiscount->Fetch())
	{
		$t = explode("-", $arDiscount["COUPON"]);
		$user_id = $t[count($t)-1];
		if($user_id != $USER_ID) continue;
		$COUPON = $arDiscount["COUPON"];
	}
	
	if(strLen(trim($COUPON)) > 0)
	{
		$arParams["MESSAGE"] = "Ваша купон на скидку 20% - ".$COUPON;
	};

} 

*/









// =======================================================================================================================
//  Изменения в почтовых шаблонах
// =======================================================================================================================
AddEventHandler("main", "OnBeforeEventSend", "BeforeNoticeSend");
function BeforeNoticeSend(&$arFields, $arTemplate) 
{
	global $USER;
	if(($arTemplate["ID"] == 1)||($arTemplate["ID"] == 2))
	{
		$USER_ID = $arFields["USER_ID"];
		$DISCOUNT_ID = 9;
		CModule::IncludeModule("catalog");
		$dbDiscount = CCatalogDiscount::GetList(Array(), Array("DISCOUNT_ID" => $DISCOUNT_ID, "COUPON_ACTIVE" => "Y"));
		//$arCoupons = Array();
		while($arDiscount = $dbDiscount->Fetch())
		{
			$t = explode("-", $arDiscount["COUPON"]);
			$user_id = $t[count($t)-1];
			if($user_id != $USER_ID) continue;
			$COUPON = $arDiscount["COUPON"];
		}
		
		if(strLen(trim($COUPON)) > 0)
			$arFields["MSG_COUPON"] = "<hr><p><b><span style='color: #ee1d24; font-size:20px;'>Ваш промокод на скидку 10%: ".$COUPON."</span></b><br>* - <i>Промокод действует на все товары интернет-магазина, кроме товаров по акции</i></p><hr>";
		else
			$arFields["MSG_COUPON"] = "";
		
		//prn($USER->GetID());
		$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$USER->GetID()),array("SELECT"=>array("UF_*")));
		$arUser = $rsUser->Fetch();
		$arFields["PASS"] = $arUser["UF_PWD"];
		//prn($arUser["UF_PWD"]);
		
		
	}
}







AddEventHandler("sale", "OnOrderNewSendEmail", "OnOrderNewSendEmailHandler");
function OnOrderNewSendEmailHandler($ID, $eventName, &$arFields) 
{
	$arOrderMail = getOrderMail($arFields["ORDER_ID"]);
	$arFields["ORDER_LIST"] = $arOrderMail["ORDER_LIST"];
	$arFields["ORDER_PROPS"] = $arOrderMail["ORDER_PROPS"];
	$arFields["USER_COMMENTS"] = $arOrderMail["USER_COMMENTS"];
	$arFields["PRICE_DELIVERY"] = $arOrderMail["PRICE_DELIVERY"];
	$arFields["DELIVERY_NAME"] = $arOrderMail["DELIVERY_NAME"];
	$arFields["PAYSYSTEM_NAME"] = $arOrderMail["PAYSYSTEM_NAME"];
}






	

// =======================================================================================================================
// Перекладывание свойтств в товарах перед сохранением/добавлением элемента инфоблока
// =======================================================================================================================

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "onBeforeElementHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "onBeforeElementHandler");
function onBeforeElementHandler(&$arFields)
{
	global $USER;
	
	// принудительно изменяем название
	if($arFields["ID"] == 124192) $arFields["NAME"] = "Календарь 2017/1";
	if($arFields["ID"] == 124194) $arFields["NAME"] = "Календарь 2017/2";
	
	//prd($arFields);
	//if($arFields["ACTIVE"]=="N") die();
	

	// Обработаем свойства товаров
	
	if($arFields["IBLOCK_ID"] == PRODUCTS_IBLOCK_ID)
	{
	
		// Перенесем значение из свойства Бренд (типа список) (PROPERTY_BRAND_ID) в свойство Бренды (типа справочник на HL-блоках) (PROPERTY_BRAND_REF_ID)
		
		/*
		if($arFields["PROPERTY_VALUES"][PROPERTY_BRAND_ID][0]["VALUE"] > 0) $br = $arFields["PROPERTY_VALUES"][PROPERTY_BRAND_ID][0]["VALUE"];
		elseif($arFields["PROPERTY_VALUES"][PROPERTY_BRAND_ID]["n0"]["VALUE"] > 0) $br = $arFields["PROPERTY_VALUES"][PROPERTY_BRAND_ID]["n0"]["VALUE"];
		else $br = "";
		
		$val = "";
		if($br == VESNUSHKI_VALUE) 	$val = "ВЕСНУШКИ";
		if($br == VAYKIDS_VALUE) 	$val = "VAY KIDS";
		if($br == VAY_VALUE) 		$val = "VAY";
		
		$arFields["PROPERTY_VALUES"][PROPERTY_BRAND_REF_ID] = Array(0 => Array("VALUE" => $val));
		*/
		
			
		
		
		
		// Уберем если установлено значение "Основной каталог"(MAIN_CATATLOG_VALUE) в свойстве "Новизна"(PROPERTY_NOVIZNA_ID)	
		if(isset($arFields["PROPERTY_VALUES"][PROPERTY_NOVIZNA_ID]))
		{
			$arNewNovizna = Array();
			foreach($arFields["PROPERTY_VALUES"][PROPERTY_NOVIZNA_ID] as $val)
			{
				if($val["VALUE"] == MAIN_CATALOG_VALUE) continue;
				$arNewNovizna[] = $val["VALUE"];
			}
			$arFields["PROPERTY_VALUES"][PROPERTY_NOVIZNA_ID] = $arNewNovizna;
		}
		
		
		// вычислим значение для умной сортировки (по умолчанию когда не выбрано никакой)
		$PROP_ARTICLE = 46;
		$PROP_NOVIZNA = 63;
		$VALUE_NOVINKA = 20;
		$VALUE_RASPRODAZHA = 22;
		$VALUE_SO_SKIDKOY = 246;
		$PROP_SEZON = 64;
		$VALUE_VESNA_LETO = 23;
		$VALUE_OSEN_ZIMA = 24;
		$VALUE_VISKOZA_SECTION = 50;
		$PROP_SMART_SORT = 141;
		$PROP_MORE_PHOTO = 51;
		
		foreach($arFields["PROPERTY_VALUES"] as $kod => $arVal)
		{
			foreach($arVal as $k => $v)
			{
				
				$value = $v["VALUE"];
				//pra($value);
				if($kod == $PROP_ARTICLE) $ARTICLE = $value;
				if($kod == $PROP_SEZON) $SEZON = $value;
				//if($kod == $PROP_NOVIZNA) $NOVIZNA = $value;
				if($kod == $PROP_NOVIZNA) $NOVIZNA = $v;
			}
		}
		//prd($arFields);
		//die();
		
		/*
		$sortInt = 0;
		if($SEZON == $VALUE_VESNA_LETO) $sortInt = 200000000; else $sortInt = 100000000; 
		if($NOVIZNA == $VALUE_NOVINKA) $sortInt += 10000000;
		if($arFields["IBLOCK_SECTION"][0] == 50) $sortInt += 1000000;
		if($NOVIZNA == $VALUE_RASPRODAZHA) $sortInt += 100000;
		if($NOVIZNA == $VALUE_SO_SKIDKOY) $sortInt += 100000;
		$sortInt = $sortInt + intVal($ARTICLE);
		$ar = $arFields["PROPERTY_VALUES"][$PROP_SMART_SORT];
		foreach($ar as $k => $v)
		{
			$arFields["PROPERTY_VALUES"][$PROP_SMART_SORT][$k]["VALUE"] = $sortInt;
		}
		*/
		
		$sortInt = 0;
		//if($SEZON == $VALUE_VESNA_LETO) $sortInt = 200000000; else $sortInt = 100000000; 
		if($NOVIZNA == $VALUE_NOVINKA) $sortInt 		  += 100000000;
		if($arFields["IBLOCK_SECTION"][0] == 77) $sortInt += 10000000;
		if($arFields["IBLOCK_SECTION"][0] == 50) $sortInt += 1000000;
		if($arFields["IBLOCK_SECTION"][0] == 78) $sortInt += 100000;
		if($arFields["IBLOCK_SECTION"][0] == 49) $sortInt += 10000;
		if($arFields["IBLOCK_SECTION"][0] == 47) $sortInt += 1000;
		if($arFields["IBLOCK_SECTION"][0] == 48) $sortInt += 100;
		if($NOVIZNA == $VALUE_RASPRODAZHA) $sortInt += 10;
		if($NOVIZNA == $VALUE_SO_SKIDKOY) $sortInt += 10;
		$sortInt = $sortInt + intVal($ARTICLE);
		
		// отодвинем товары без фото назад
		if( (!is_array($arFields["PREVIEW_PICTURE"])) && (!is_array($arFields["PROPERTY_VALUES"][$PROP_MORE_PHOTO])) ) $sortInt = intVal($ARTICLE);
		
		$ar = $arFields["PROPERTY_VALUES"][$PROP_SMART_SORT];
		foreach($ar as $k => $v)
		{
			$arFields["PROPERTY_VALUES"][$PROP_SMART_SORT][$k]["VALUE"] = $sortInt;
		}
		
	}
	
	
	
	
	// Обработаем свойства торговых предложений
		
	if($arFields["IBLOCK_ID"] == OFFERS_IBLOCK_ID)
	{
		
		// Установим свойства РАЗМЕР, ЦВЕТ и ОТТЕНОК в торговых предложениях
		
		foreach($arFields["PROPERTY_VALUES"][PROPERTY_FEATURES_ID] as $val_id => $arValue)
		{
			if($arValue["DESCRIPTION"] == "размер") 	$size = $arValue["VALUE"];   
			if($arValue["DESCRIPTION"] == "КодЦвета") 	$color = $arValue["VALUE"];  
			if($arValue["DESCRIPTION"] == "Оттенок") 	$tone[] = $arValue["VALUE"];  
		}
		
		$dbRes = CIBlockPropertyEnum::GetList(Array(), Array("CODE" => Array("SIZE", "COLOR"), "VALUE" => Array($size, $color)));
		while($arRes = $dbRes->GetNext())
		{
			if(($arRes["PROPERTY_CODE"] == "COLOR")&&($arRes["VALUE"] == $color)) 	$color_val_id = $arRes["ID"];
			if(($arRes["PROPERTY_CODE"] == "SIZE")&&($arRes["VALUE"] == $size)) 	$size_val_id = $arRes["ID"];
		}
		if(!isset($color_val_id))
		{
			$ibpenum = new CIBlockPropertyEnum;
			$color_val_id = $ibpenum->Add(Array("PROPERTY_ID" => PROPERTY_COLOR_ID, "VALUE" => $color));
		}
		if(!isset($size_val_id))
		{
			$ibpenum = new CIBlockPropertyEnum;
			$size_val_id = $ibpenum->Add(Array("PROPERTY_ID" => PROPERTY_SIZE_ID, "VALUE" => $size));
		}
		
		$arFields["PROPERTY_VALUES"][PROPERTY_COLOR_ID] = Array(Array("VALUE" => $color_val_id));
		$arFields["PROPERTY_VALUES"][PROPERTY_SIZE_ID] = Array(Array("VALUE" => $size_val_id));
		$arFields["PROPERTY_VALUES"][PROPERTY_TONE_ID] = $tone;
	}
	
	
	// принудительно изменяем название (два товара и два offers))
	if(in_array($arFields["ID"], Array(124192,124193,124194,124195,124196,124197))) 
	{
		$arFields["NAME"] = str_replace("Шарф (сувенирный)", "Календарь", $arFields["NAME"]);
		$params = array("replace_space" => "-", "replace_other" => "-", "change_case" => "L");
		$code = Cutil::translit($arFields["NAME"], "ru", $params);
		if(strLen($code) > 0) $arFields["CODE"] = $code;		
		//prd($arFields);
	}

	
	//prd1($arFields);		

}



// =======================================================================================================================
// Если задано $ALLOW_NEWVAY_FOTO_SYNC то дублируем фото с этого сайта на newvay.ru
// Задействуются файлы http://newvay.ru/tools/img_sync.php и http://sogrevay.ru/tools/get_photo.php
// =======================================================================================================================
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "onAfterElementHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "onAfterElementHandler");
function onAfterElementHandler($arFields)
{
	$ALLOW_NEWVAY_FOTO_SYNC = false;	// РЕЗРЕШАТЬ ДУБЛИРОВАТЬ ФОТО НА NEWVAY.RU? 
	if($arFields["RESULT"] == 1) // если успешно сохранилось
	{
		// исключим дублирование фото для выгрузки из 1с, только при ручном изменении из админки сайта!!!
		$update_foto = true;
		if($_REQUEST['filename'] == 'offers.xml') $update_foto = false;
		if($_REQUEST['filename'] == 'import.xml') $update_foto = false;
		
		if($update_foto && $ALLOW_NEWVAY_FOTO_SYNC)
		{
			$res = QueryGetData("www.newvay.ru", 80, "/tools/img_sync.php", "xml=".$arFields["XML_ID"], $errno, $errstr);
		}
	}
}




AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", "OnBeforeIBlockSectionUpdateHandler");
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", "OnBeforeIBlockSectionUpdateHandler");
function OnBeforeIBlockSectionUpdateHandler(&$arFields)
{
	global $USER;
	if($USER->isAdmin())
	{
		// принудительно прячем некоторые разделы (Новые коллекции, VAY KIDS и т.д.)
		if($arFields["ID"] == 53) $arFields["ACTIVE"] = "N";
		if($arFields["ID"] == 57) $arFields["ACTIVE"] = "N";
		if($arFields["ID"] == 154) $arFields["ACTIVE"] = "N";
	}
}





AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "onBeforeElementUpdateSection");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "onBeforeElementUpdateSection");
function onBeforeElementUpdateSection(&$arFields)
{
	global $USER;
	
	// надо сделать проверку на инфоблок чтобы срабаотывало только дла товаров не для торговых предложений
	if($arFields["IBLOCK_ID"] == PRODUCTS_IBLOCK_ID)
	{
		$PROPERTY_POL_ID = 139;					// ИД свойства ПОЛ
		$PROPERTY_POL_VAL_BOY = 2836;			// ИД значение свойства ПОЛ = МАЛЬЧИК
		$PROPERTY_POL_VAL_GIRL = 2971;			// ИД значение свойства ПОЛ = ДЕВОЧКА
		$PROPERTY_POL_VAL_DET = 4480; 			// ИД значение свойства ПОЛ = ДЕТСКИЙ
		
		$PROPERTY_RISUNOK_ID = 253;				// ИД свойства РИСУНОК
		$PROPERTY_RISUNOK_VAL_OLENI = 4471;		// ИД значения С ОЛЕНЯМИ
		
		$PROPERTY_SEZON_ID = 64;				// ИД свойства СЕЗОН
		$PROPERTY_SEZON_VAL_ZIMA = 24;			// ИД значения ОСЕНЬ-ЗИМА
		$PROPERTY_SEZON_VAL_LETO = 23;			// ИД значения ВЕСНА-ЛЕТО
		
		$PROPERTY_TIP_ID = 254;					// ИД свойства ТИП ИЗДЕЛИЯ
		$PROPERTY_TIP_VAL_PALTO = 4532;			// ИД значения ПАЛЬТО
		$PROPERTY_TIP_VAL_SHARF = 4533;			// ИД значения ШАРФ
		$PROPERTY_TIP_VAL_SNUD = 4534;			// ИД значения СНУД
		$PROPERTY_TIP_VAL_KARDIGAN = 4535;		// ИД значения КАРДИГАН
		$PROPERTY_TIP_VAL_SARAFAN = 4536;		// ИД значения САРАФАН
		
		
		
		// текущее значение свойства Пол
		if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_POL_ID]["n0"])) $PROP_POL_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_POL_ID]["n0"]["VALUE"]);
		else $PROP_POL_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_POL_ID][0]["VALUE"]);   

		// текущее значение свойства Тип изделия
		if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_TIP_ID]["n0"])) $PROP_TIP_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_TIP_ID]["n0"]["VALUE"]);
		else $PROP_TIP_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_TIP_ID][0]["VALUE"]);   

		// текущее значение свойства Сезон
		if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_SEZON_ID]["n0"])) $PROP_SEZON_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_SEZON_ID]["n0"]["VALUE"]);
		else $PROP_SEZON_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_SEZON_ID][0]["VALUE"]);   
		
		// текущее значение свойства Рисунок
		if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_RISUNOK_ID]["n0"])) $PROP_RISUNOK_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_RISUNOK_ID]["n0"]["VALUE"]);
		else $PROP_RISUNOK_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_RISUNOK_ID][0]["VALUE"]);   

		
		// перекладываем основные разделы детского трикотажа
		
		$arReplace = Array(
			54 => Array("BOY" => 160, "GIRL" => 167), // VAY KIDS / Жилеты детские
			56 => Array("BOY" => 161, "GIRL" => 168), // VAY KIDS / Рейтузы детские
			81 => Array("BOY" => 162, "GIRL" => 169), // VAY KIDS / Жакеты детские
			82 => Array("BOY" => 163, "GIRL" => 170), // VAY KIDS / Платья
			83 => Array("BOY" => 178, "GIRL" => 171), // VAY KIDS / Джемперы детские
			84 => Array("BOY" => 179, "GIRL" => 172), // VAY KIDS / Вискоза детская
			59 => Array("BOY" => 180, "GIRL" => 173), // Веснушки / Платья детские
			60 => Array("BOY" => 181, "GIRL" => 174), // Веснушки / Рейтузы
			86 => Array("BOY" => 182, "GIRL" => 175), // Веснушки / Жакеты детские
			87 => Array("BOY" => 183, "GIRL" => 176), // Веснушки / Жилеты детские
			88 => Array("BOY" => 184, "GIRL" => 177),  // Веснушки / Джемперы детские
			
			186 => Array("BOY" => 202, "GIRL" => 195),  // Школьный трикотаж / Жилеты
			187 => Array("BOY" => 203, "GIRL" => 196),  // Школьный трикотаж / Жакеты, кардиганы
			188 => Array("BOY" => 204, "GIRL" => 197),  // Школьный трикотаж / Джемперы
			189 => Array("BOY" => '', "GIRL" => 198),  // Школьный трикотаж / Платья, сарафаны
			190 => Array("BOY" => 205, "GIRL" => 199),  // Школьный трикотаж / Водолзаки, блузы
			191 => Array("BOY" => '', "GIRL" => 200)  // Школьный трикотаж / Юбки
			//192 => Array("BOY" => , "GIRL" => )  // Школьный трикотаж / Брюки
			//89 => Array("BOY" => 0, "GIRL" => 0)    // Веснушки / Вискоза детская
		);
		
		$arNewSect = Array();
		foreach($arFields["IBLOCK_SECTION"] as $sectID) 
		{
			if(isset($arReplace[$sectID]))
			{
				//prn($sectID);
				if($PROP_POL_VALUE == $PROPERTY_POL_VAL_BOY) $arNewSect[] = $arReplace[$sectID]["BOY"];
				if($PROP_POL_VALUE == $PROPERTY_POL_VAL_GIRL) $arNewSect[] = $arReplace[$sectID]["GIRL"];
				if($PROP_POL_VALUE == $PROPERTY_POL_VAL_DET) $arNewSect = array_values($arReplace[$sectID]);
			}
		}

		if(count($arNewSect) > 0)
		{
			$arFields["IBLOCK_SECTION"] = $arNewSect;
		}
		
		
		// обработка свойства с Оленями
		
		if($PROP_RISUNOK_VALUE == $PROPERTY_RISUNOK_VAL_OLENI)
		{
			$arReplaceOleni = Array(
				159 => Array(180, 181, 182, 183, 184, 160, 161, 162, 163, 178, 179),  		// для мальчиков
				166 => Array(167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177),		// для девочек
				228 => Array(49, 227),														// с оленями женские
				233 => Array(150, 152, 231, 232)											// с оленями мужские
				);
			$arNewSect = Array();
			foreach($arFields["IBLOCK_SECTION"] as $sectID)
			{
				foreach($arReplaceOleni as $newSectID => $ids)
				{
					if(in_array($sectID, $ids)) $arNewSect[] = $newSectID;
				}
			}
			if(count($arNewSect) > 0)
			{
				$arFields["IBLOCK_SECTION"] = $arNewSect;
			}
		}
		
		
		
		// обработка некоторых свойств

		$arNewSect = Array();
		foreach($arFields["IBLOCK_SECTION"] as $sectID)
		{
			// Жакеты женские распределяем на теплые и легкие
			if(in_array($sectID, Array(77, 222))) 
			{
				if($PROP_SEZON_VALUE == $PROPERTY_SEZON_VAL_LETO) $arNewSect[] = 224;
				if($PROP_SEZON_VALUE == $PROPERTY_SEZON_VAL_ZIMA) $arNewSect[] = 223;
				if($PROP_TIP_VALUE == $PROPERTY_TIP_VAL_KARDIGAN) $arNewSect = Array(225);		// кардиганы
				if($PROP_TIP_VALUE == $PROPERTY_TIP_VAL_PALTO) $arNewSect = Array(226);			// пальто
			}
			
			if($sectID == 216) $arNewSect = Array(216, 230);	// дублируем платья из швейных изделий в летние платья
			if($sectID == 217) $arNewSect = Array(217, 230);	// дублируем сарафаны из швейных изделий в летние платья
			
		}
		if(count($arNewSect) > 0)
		{
			$arFields["IBLOCK_SECTION"] = $arNewSect;
		}
		
		
		
		
		

		/*
		if($PROP_SEZON_VALUE == $PROPERTY_SEZON_VAL_LETO)
		{
			$arReplaceOleni = Array(
				159 => Array(180, 181, 182, 183, 184, 160, 161, 162, 163, 178, 179),  		// для мальчиков
				166 => Array(167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177),		// для девочек
				228 => Array(49, 227),														// с оленями женские
				233 => Array(150, 152, 231, 232)											// с оленями мужские
				);
			$arNewSect = Array();
			foreach($arFields["IBLOCK_SECTION"] as $sectID)
			{
				foreach($arReplaceOleni as $newSectID => $ids)
				{
					if(in_array($sectID, $ids)) $arNewSect[] = $newSectID;
				}
			}
			if(count($arNewSect) > 0)
			{
				$arFields["IBLOCK_SECTION"] = $arNewSect;
			}
		}
		*/
		
		
		
		

	}
	

}
	
	
	
	
// =======================================================================================================================
// Фикс для того чтобы обновлялось количество в ноль в предложении при импорте из 1с, если в нем не приходит <Количество>
// =======================================================================================================================

AddEventHandler("catalog", "OnBeforeProductUpdate", "unsetquantity");
function unsetquantity($ID,&$Fields)
{
	if (@$_REQUEST['mode']=='import')//импорт  из 1с? 
    {
        if(!$Fields['QUANTITY']) 
		{
			$Fields['QUANTITY'] = 0;
		}
    }	
}	

// =======================================================================================================================
// Фикс для того чтобы деактивировалось предложение в котором наличие равно нулю (сразу realtime)
// =======================================================================================================================

AddEventHandler("catalog", "OnProductUpdate", "deactivateOffer");
function deactivateOffer($ID, $Fields)
{
	if (@$_REQUEST['mode']=='import')//импорт  из 1с? 
    {
        if(($Fields['QUANTITY'] == 0)&&($_REQUEST["filename"] == "offers.xml"))
		{
			$el = new CIBlockElement;
			$res = $el->Update($ID, Array("ACTIVE" => "N"));	
		}
    }
}	







	

	
	
	
// =======================================================================================================================
//   Функция подстановки картинок из свойства MORE_PHOTO и DETAIL_PICTURE товара в соответствующие его предложения
//   (сопоставление по подписям картинок и названиям торговых предложений)
//    принимает ArItem и отдаёт измененный его же
//    учитывает некоторые различия при вызове из раздела (section = true) и из карточки товара (secion = false)
// =======================================================================================================================

function setOfferPictures($arItem, $section = true) 
{
	if($section) 
	{
		$WIDTH = 200;
		$HEIGHT = 300;
	}
	else
	{
		$WIDTH = 1000;
		$HEIGHT = 1500;
	}
	
	$arPictures = Array();
	
	// Добавим в массив фото детальное фото товара, ибо оно не попадает в свойство MORE_PHOTO
	if($arItem["DETAIL_PICTURE"])
	{
		//pra(" ".strtoupper($arItem["DETAIL_PICTURE"]["DESCRIPTION"]));
		//pra(strpos(" ".strtoupper($arItem["DETAIL_PICTURE"]["DESCRIPTION"]), "ЦВЕТ"));
		if(strpos("  ".strtoupper($arItem["DETAIL_PICTURE"]["DESCRIPTION"]), " ЦВЕТ ") > 0)
			$t = explode(" ЦВЕТ ", strtoupper($arItem["DETAIL_PICTURE"]["DESCRIPTION"]));
		else
			$t[1] = strtoupper($arItem["DETAIL_PICTURE"]["DESCRIPTION"]);
		//if(strpos(" ".$t[1], "(") > 0) $t[1] = trim(substr($t[1], 0, strpos($t[1], "(")));		// берем часть строки до скобки (чтобы не брать (1)(2) и т.д.)
		
		$file = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width' => $WIDTH, 'height' => $HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$f = Array(
				"ID" => $arItem["DETAIL_PICTURE"]["ID"],
				"SRC" => $file["src"],
				"WIDTH" => $file["width"],
				"HEIGHT" => $file["height"],
				"DESCRIPTION" => $arItem["DETAIL_PICTURE"]["DESCRIPTION"]
				);
		$arPictures[trim($t[1])][] = $f;
	}
	
	
	// Добавим все картинки из свойства MORE_PHOTO (если там одна картинка, чуть-чуть обработка отличается)
	if(count($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["VALUE"]) == 1)
	{
		if(strpos("  ".strtoupper($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["DESCRIPTION"]), " ЦВЕТ ") > 0)
			$t = explode(" ЦВЕТ ", strtoupper($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["DESCRIPTION"]));
		else 
			$t[1] = strtoupper($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["DESCRIPTION"]);
		//if(strpos(" ".$t[1], "(") > 0) $t[1] = trim(substr($t[1], 0, strpos($t[1], "(")));		// берем часть строки до скобки (чтобы не брать (1)(2) и т.д.)
		$file = CFile::ResizeImageGet($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["ID"], array('width' => $WIDTH, 'height' => $HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$f = Array(
				"ID" => $arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["ID"],
				"SRC" => $file["src"],
				"WIDTH" => $file["width"],
				"HEIGHT" => $file["height"],
				"DESCRIPTION" => $arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["DESCRIPTION"]
				);
		$arPictures[trim($t[1])][] = $f;
	}
	else foreach($arItem["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"] as $arPicture)
	{
		if(strpos("  ".strtoupper($arPicture["DESCRIPTION"]), " ЦВЕТ ") > 0)
			$t = explode(" ЦВЕТ ", strtoupper($arPicture["DESCRIPTION"]));
		else
			$t[1] = strtoupper($arPicture["DESCRIPTION"]);
		//if(strpos(" ".$t[1], "(") > 0) $t[1] = trim(substr($t[1], 0, strpos($t[1], "(")));		// берем часть строки до скобки (чтобы не брать (1)(2) и т.д.)
		$file = CFile::ResizeImageGet($arPicture["ID"], array('width' => $WIDTH, 'height' => $HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$f = Array(
				"ID" => $arPicture["ID"],
				"SRC" => $file["src"],
				"WIDTH" => $file["width"],
				"HEIGHT" => $file["height"],
				"DESCRIPTION" => $arPicture["DESCRIPTION"]
				);
		$arPictures[trim($t[1])][] = $f;
	}
	
	//pra($arPictures); 
	
	// если запуск из раздела
	if($section)
	{
		// Сравниваем по свойству цвет торгового предложения (раньше было по названию!)
		foreach($arItem["OFFERS"] as $keyOffer => $arOffer)
		{
			
			// находим совпадения
			foreach($arPictures as $color => $arP)
			{
				if(trim($color) == trim(strtoupper($arOffer["PROPERTIES"]["COLOR"]["VALUE"])))
				//if(strpos(" ".strtoupper($arOffer["NAME"]), $color) > 0) 
				{
					$arItem["OFFERS"][$keyOffer]["DETAIL_PICTURE"] = $arP[0];
					$arItem["OFFERS"][$keyOffer]["PREVIEW_PICTURE"] = Array(
						"ID" => $arP[0]["ID"],
						"SRC" => $arP[0]["SRC"],
						"WIDTH" => $arP[0]["WIDTH"],
						"HEIGHT" => $arP[0]["HEIGHT"]
						);
					break;
				}
			}
			
			// если нет совпадения, ставим заглушку
			if(!is_array($arItem["OFFERS"][$keyOffer]["DETAIL_PICTURE"]))
			{
				$arItem["OFFERS"][$keyOffer]["PREVIEW_PICTURE"] = Array(
					"ID" => "",
					"SRC" => "/upload/no_female_big.gif",
					"WIDTH" => $WIDTH,
					"HEIGHT" => $HEIGHT
					);
				$arItem["OFFERS"][$keyOffer]["DETAIL_PICTURE"] = $arItem["OFFERS"][$keyOffer]["PREVIEW_PICTURE"];
			}
		}
	}
	// иначе из карточки товара
	else
	{
		// Сравниваем по свойству цвет торгового предложения (раньше было по названию!)
		foreach($arItem["OFFERS"] as $keyOffer => $arOffer)
		{
			// находим совпадения
			foreach($arPictures as $color => $arP)
			{
				//pra(trim($color)." = ".trim(strtoupper($arOffer["PROPERTIES"]["COLOR"]["VALUE"])));
				if(trim($color) == trim(strtoupper($arOffer["PROPERTIES"]["COLOR"]["VALUE"])))
				//if(strpos(" ".strtoupper($arOffer["NAME"]), $color) > 0) 
				{
					$arItem["OFFERS"][$keyOffer]["MORE_PHOTO"] = $arP;
					$arItem["OFFERS"][$keyOffer]["MORE_PHOTO_COUNT"] = count($arP);
					break;
				}
			}
		}
	}
	
	return $arItem;
	
}




// =======================================================================================================================
//   Функция сортировки торговых предложений чтобы сначала шли те, картинка которых задана как основная картинка товара
//    учитываем возможный фильтр по цвету или оттенку цвета
// =======================================================================================================================

function sortOffersInItem($arItem, $offerID = false)
{
	global $USER;
	
	$PROPERTY_COLOR_CODE = "=PROPERTY_75";  // код свойства цвет торговых предлжожений 
	$PROPERTY_TONE_CODE = "=PROPERTY_80";   // код свойства оттенок торговых предложений
	
	//prn($GLOBALS["arrFilter"]);
	
	if(isset($GLOBALS["arrFilter"]["OFFERS"][$PROPERTY_COLOR_CODE]))
	{
		$arColors = $GLOBALS["arrFilter"]["OFFERS"][$PROPERTY_COLOR_CODE];
		$arRes = CIBlockPropertyEnum::GetByID($arColors[0]);
		$FIRST_COLOR = strtoupper($arRes["VALUE"]);
		
		$arFirstOffers = Array();
		$arLastOffers = Array();
		$arNewOffers = Array();
		foreach($arItem["OFFERS"] as $arOffer)
		{
			if(stripos(" ".$arOffer["NAME"], $FIRST_COLOR) > 0) $arFirstOffers[] = $arOffer;
			else $arLastOffers[] = $arOffer;			
		}
		$arNewOffers = array_merge($arFirstOffers, $arLastOffers);
		$arItem["OFFERS"] = $arNewOffers;
	}
	elseif(isset($GLOBALS["arrFilter"]["OFFERS"][$PROPERTY_TONE_CODE]))
	{
		$arFirstOffers = Array();
		$arLastOffers = Array();
		$arNewOffers = Array();
		
		foreach($arItem["OFFERS"] as $arOffer)
		{
			foreach($GLOBALS["arrFilter"]["OFFERS"][$PROPERTY_TONE_CODE] as $color)
			{
				if(in_array($color, $arOffer["DISPLAY_PROPERTIES"]["TONE"]["VALUE"])) $arFirstOffers[] = $arOffer;
				else $arLastOffers[] = $arOffer;
			}
		}
		$arNewOffers = array_merge($arFirstOffers, $arLastOffers);
		$arItem["OFFERS"] = $arNewOffers;
		//foreach($arItem["OFFERS"] as $arOffer) prn($arOffer["NAME"]);
	}
	else
	{
		// 1) сначала в качестве главного offer выберем offer с фото
		foreach($arItem["OFFERS"] as $arOffer)
		{
			if(is_array($arOffer["MORE_PHOTO"]) || ( is_array($arOffer["DETAIL_PICTURE"]) && ($arOffer["DETAIL_PICTURE"]["SRC"] != "/upload/no_female_big.gif"))) 
			{
				$FIRST_COLOR = strtoupper(trim($arOffer["PROPERTIES"]["COLOR"]["VALUE"]));
				break;
			};
		};
		//pra($FIRST_COLOR);
		
		
		// 2) далее, если в главном фото товара указан offer с фото, то назначим главным тогда его, иначе оставляем выбранный на первом этапе
		$t = explode(" ЦВЕТ ", strtoupper($arItem["DETAIL_PICTURE"]["DESCRIPTION"]));
		$var = "";   // возможный вариант цвета
		if(isset($t[1]))
			$var = strtoupper(trim($t[1]));
		else 
			$var = strtoupper(trim($t[0]));
		foreach($arItem["OFFERS"] as $arOffer)
		{
			if( (stripos(" ".$arOffer["NAME"], $var) > 0)/*  && is_array($arOffer["MORE_PHOTO"])*/) 
			{
				$FIRST_COLOR = strtoupper(trim($arOffer["PROPERTIES"]["COLOR"]["VALUE"]));
				break;
			};
		};
		//pra($FIRST_COLOR);
	
		
		
		$arFirstOffers = Array();
		$arLastOffers = Array();
		$arNewOffers = Array();
		
		foreach($arItem["OFFERS"] as $arOffer)
		{
			if(stripos(" ".$arOffer["NAME"], $FIRST_COLOR) > 0) $arFirstOffers[] = $arOffer;
			else $arLastOffers[] = $arOffer;			
		}
		$arNewOffers = array_merge($arFirstOffers, $arLastOffers);
		$arItem["OFFERS"] = $arNewOffers;
	}
	
	// если задан конкретный ID предложения, то выдвигаем его вперед (в основном для YML)
	if(intVal($offerID) > 0)
	{
		$offer_id = intVal($offerID);
		unset($first_offer);
		$arNewOffers = Array();
		foreach($arItem["OFFERS"] as $arOffer) 
		{
			if($arOffer["ID"] == $offer_id)
			{
				$first_offer = $arOffer;
				continue;
			}	
			$arNewOffers[] = $arOffer;
		}
		if($first_offer) 
		{
			array_unshift($arNewOffers, $first_offer);	
			$arItem["OFFERS"] = $arNewOffers;
		}
	}
	
	return $arItem;
}




// =======================================================================================================================
//   Функция подстановки нужной превью картинки по коду торгового предложения
//    возвращает массив картинки
//    если заданы width и height, то преобразует к заданному размеру
// =======================================================================================================================

function getPreview($offerID, $width=false, $height=false)
{
	$productID = CCatalogSku::GetProductInfo($offerID);
	//pra($productID);
		
	// если offer неактивный, то у него пропадает привязка к товару(!), на этот случай ищем товар по части XML_ID
	if(!is_array($productID))
	{
		$dbOffer = CIBlockElement::GetByID($offerID);
		if($arOffer = $dbOffer->Fetch())
		{
			$product_xml_id = $arOffer["XML_ID"];
		}
		if(strLen(trim($product_xml_id))>0)
		{
			$t = explode("#", $product_xml_id);
			$dbPr = CIBlockElement::GetList(Array(), Array("XML_ID" => $t[0]), false, false, Array("ID", "IBLOCK_ID"));
			if($arPr = $dbPr->Fetch()) $productID = Array("ID" => $arPr["ID"], "IBLOCK_ID" => $arPr["IBLOCK_ID"]);
		}
	}
		
	if(is_array($productID))
	{
		$dbRes = CIBlockElement::getList(
			Array(),
			Array(
				"IBLOCK_ID" => $productID["IBLOCK_ID"],
				"ID" => $productID["ID"]
				),
			false,
			false,
			Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_ID", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO")
			);
		$arPictures = Array();
		
		unset($detail_picture);
		while($arRes = $dbRes->GetNext())
		{
			if(($arRes["DETAIL_PICTURE"] > 0)&&(!isset($detail_picture)))
			{
				$detail_picture = $arRes["DETAIL_PICTURE"];	
				$t1 = CFile::GetFileArray($detail_picture);
				$e1 = explode(" ЦВЕТ ", strtoupper($t1["DESCRIPTION"]));
				if(count($e1)>1) $arPictures[trim($e1[1])][] = $t1;
				else $arPictures[trim($e1[0])][] = $t1;
			}
			
			if(is_array($arRes["PROPERTY_MORE_PHOTO_VALUE"]))
			{
				foreach($arRes["PROPERTY_MORE_PHOTO_VALUE"] as $arP)
				{
					$t = CFile::GetFileArray($arP);
					$e = explode(" ЦВЕТ ", strtoupper($t["DESCRIPTION"]));
					//$arPictures[trim($e[1])][] = $t;
					if(count($e)>1) $arPictures[trim($e[1])][] = $t;
					else $arPictures[trim($e[0])][] = $t;
				}
			}
			else
			{
				$t = CFile::GetFileArray($arRes["PROPERTY_MORE_PHOTO_VALUE"]);
				$e = explode(" ЦВЕТ ", strtoupper($t["DESCRIPTION"]));
				//$arPictures[trim($e[1])][] = $t;
				if(count($e)>1) $arPictures[trim($e[1])][] = $t;
				else $arPictures[trim($e[0])][] = $t;
			}
		}
		
		$dbRes = CIBlockElement::GetByID($offerID);
		if($arElement = $dbRes->Fetch())
		{
			foreach($arPictures as $color => $arPicture)
			{
				if(strpos(" ".strtoupper($arElement["NAME"]), $color) > 0)
				{
					$res = $arPicture[0];
					break;
				}
			}
		}
	}
	
	if(is_array($res))
	{
		if($width && $height)
		{
			$f = CFile::ResizeImageGet($res["ID"], array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$res["WIDTH"] = $f["width"];
			$res["HEIGHT"] = $f["height"];
			$res["SRC"] = $f["src"];
		}
		return $res;
	}
	else
	{
		$nofoto = CFile::GetFileArray(NOFOTO_FILE_ID);
		if($width && $height)
		{
			$f = CFile::ResizeImageGet(NOFOTO_FILE_ID, array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$nofoto["WIDTH"] = $f["width"];
			$nofoto["HEIGHT"] = $f["height"];
			$nofoto["SRC"] = $f["src"];
		}
		return $nofoto;
	}

}



// =======================================================================================================================
//   Функция выбора картинок торгового предложения для YML
//   если заданы width и height, то преобразует к заданному размеру
// =======================================================================================================================

function getOfferPictures($offerID, $width=false, $height=false)
{
	$productID = CCatalogSku::GetProductInfo($offerID);
	//pra($productID);
		
	// если offer неактивный, то у него пропадает привязка к товару(!), на этот случай ищем товар по части XML_ID
	if(!is_array($productID))
	{
		$dbOffer = CIBlockElement::GetByID($offerID);
		if($arOffer = $dbOffer->Fetch())
		{
			$product_xml_id = $arOffer["XML_ID"];
		}
		if(strLen(trim($product_xml_id))>0)
		{
			$t = explode("#", $product_xml_id);
			$dbPr = CIBlockElement::GetList(Array(), Array("XML_ID" => $t[0]), false, false, Array("ID", "IBLOCK_ID"));
			if($arPr = $dbPr->Fetch()) $productID = Array("ID" => $arPr["ID"], "IBLOCK_ID" => $arPr["IBLOCK_ID"]);
		}
	}
		
	if(is_array($productID))
	{
		$dbRes = CIBlockElement::getList(
			Array(),
			Array(
				"IBLOCK_ID" => $productID["IBLOCK_ID"],
				"ID" => $productID["ID"]
				),
			false,
			false,
			Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_ID", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO")
			);
		$arPictures = Array();
		
		unset($detail_picture);
		while($arRes = $dbRes->GetNext())
		{
			if(($arRes["DETAIL_PICTURE"] > 0)&&(!isset($detail_picture)))
			{
				$detail_picture = $arRes["DETAIL_PICTURE"];	
				$t1 = CFile::GetFileArray($detail_picture);
				$e1 = explode(" ЦВЕТ ", strtoupper($t1["DESCRIPTION"]));
				if(count($e1)>1) $arPictures[trim($e1[1])][] = $t1;
				else $arPictures[trim($e1[0])][] = $t1;
			}
			
			if(is_array($arRes["PROPERTY_MORE_PHOTO_VALUE"]))
			{
				foreach($arRes["PROPERTY_MORE_PHOTO_VALUE"] as $arP)
				{
					$t = CFile::GetFileArray($arP);
					$e = explode(" ЦВЕТ ", strtoupper($t["DESCRIPTION"]));
					//$arPictures[trim($e[1])][] = $t;
					if(count($e)>1) $arPictures[trim($e[1])][] = $t;
					else $arPictures[trim($e[0])][] = $t;
				}
			}
			else
			{
				$t = CFile::GetFileArray($arRes["PROPERTY_MORE_PHOTO_VALUE"]);
				$e = explode(" ЦВЕТ ", strtoupper($t["DESCRIPTION"]));
				//$arPictures[trim($e[1])][] = $t;
				if(count($e)>1) $arPictures[trim($e[1])][] = $t;
				else $arPictures[trim($e[0])][] = $t;
			}
		}
		//prn($arPictures);
		
		
		$dbRes = CIBlockElement::GetByID($offerID);
		if($arElement = $dbRes->Fetch())
		{
			foreach($arPictures as $color => $arPicture)
			{
				if(strpos(" ".strtoupper($arElement["NAME"]), $color) > 0)
				{
					$res = $arPicture;
					break;
				}
			}
		}
	}
	
	if(is_array($res))
	{
		if($width && $height)
		{
			$f = CFile::ResizeImageGet($res["ID"], array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$res["WIDTH"] = $f["width"];
			$res["HEIGHT"] = $f["height"];
			$res["SRC"] = $f["src"];
		}
		return $res;
	}
	else
	{
		$nofoto = CFile::GetFileArray(NOFOTO_FILE_ID);
		if($width && $height)
		{
			$f = CFile::ResizeImageGet(NOFOTO_FILE_ID, array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$nofoto["WIDTH"] = $f["width"];
			$nofoto["HEIGHT"] = $f["height"];
			$nofoto["SRC"] = $f["src"];
		}
		return Array($nofoto);
	}

}




// =======================================================================================================================
//    Доочистка каталога при импорте из 1с
// =======================================================================================================================

AddEventHandler("catalog", "OnSuccessCatalogImport1C", "OnSuccessCatalogImport1CHandler");
function OnSuccessCatalogImport1CHandler()
{
	
	if (@$_REQUEST['filename']=='offers.xml')
	{
		
		// Деактивируем торговые предложения, в которых наличие равно нулю
		// после апгрейда см.выше в обработчике OnProductUpdate нет необходимости, но на всякий случай проверяем 
		$dbRes = CIBlockElement::GetList(
			Array(),
			Array("IBLOCK_ID" => OFFERS_IBLOCK_ID, "ACTIVE" => "Y", "<CATALOG_QUANTITY" => 1), // инфоблок предложений
			false,
			false,
			Array("ID", "NAME")
			);
		$arDel = Array();
		while($arRes = $dbRes->GetNext()) 
		{	
			//prn($arRes["NAME"]." - ".$arRes["CATALOG_QUANTITY"]);
			$arDel[$arRes["ID"]] = $arRes["NAME"];
		}
		$countEmptyOffers = count($arDel);

		$str = date("d.m.Y H:i:s")."\n";
		if(count($arDel) <1 ) $str.="Not exists empty offers";
		foreach($arDel as $k=>$v) 
		{
			$str .= $v." - ";
			$el = new CIBlockElement;
			$arElementFields = Array("ACTIVE" => "N");  
			$res = $el->Update($k, $arElementFields);	
			if($res)
				$str .= "deactivated";
			else 
				$str .= "DEACTIVATION ERROR!";	
			$str .= "\n";
		}
		
		
		
		// проходим по всем торговым предложениям каждого товара
		// если в рамках одного товара есть предложения, время обновления которых значительно 
		// отличается от времени обновления остальных предложений в этом товаре, то оно деактивируется
		// (это означает что предложение пропало из выгрузки, что означает что нет свободного остатка)
		
		// ПО ИДЕЕ ЭТОТ БЛОК НЕ НУЖЕН, ИБО ЕСЛИ ПРЕДЛОЖЕНИЯ НЕТ В СВОБОДНОМ ОСТАТКЕ, ТО ОНО ВЫГРУЖАЕТСЯ БЕЗ ПОЛЯ КОЛИЧЕСТВО
		// И ОБРАБАТЫВАЕТСЯ ПО ОБЩИМ ПРАВИЛАМ, СНАЧАЛА СТАВИТСЯ НОЛЬ, ПОТОМ ДЕАКТИВИРУЕТСЯ
		
		/*
		$razbros = 120; // интервал времени в сек. после которого предложение считается необновленным
	
		$dbRes = CIBlockElement::GetList(
			Array(),
			Array("IBLOCK_ID" => OFFERS_IBLOCK_ID, "ACTIVE" => "Y"), // инфоблок предложений
			false,
			false,
			Array("ID", "NAME", "TIMESTAMP_X_UNIX", "PROPERTY_CML2_LINK", "CATALOG_GROUP_1")
			);
			
		$arr = Array();
		$arrForReport = Array();
		$arNewMinPrices = Array();  		// вычислим параллельно минимальные цены для каждого товара
		while($arRes = $dbRes->GetNext()) 
		{	
			$arr[$arRes["PROPERTY_CML2_LINK_VALUE"]][$arRes["ID"]] = $arRes["TIMESTAMP_X_UNIX"];
			$arrForReport[$arRes["ID"]] = $arRes["NAME"];
			
			if( ($arRes["CATALOG_PRICE_1"] < $arNewMinPrices[$arRes["PROPERTY_CML2_LINK_VALUE"]]) || ($arNewMinPrices[$arRes["PROPERTY_CML2_LINK_VALUE"]] == 0) )
				$arNewMinPrices[$arRes["PROPERTY_CML2_LINK_VALUE"]] = $arRes["CATALOG_PRICE_1"];
		}

		$del = Array();
		$delReport = Array();
		foreach($arr as $product_id => $times) 
		{
			$etalon = max($times);
			foreach($times as $k => $v) 
			{
				if(($etalon - $v) >= $razbros) 
				{
					$del[] = $k;
					$delReport[] = $arrForReport[$k];
				}

			}

		}

		$str .= "\n";
		if(count($del) <1 ) $str .= "Нет устаревших элементов";
		$countOldOffers = count($del);
		for($i=0; $i<count($del); $i++) 
		{
			
			//$str .= $v." - ";
			$el = new CIBlockElement;
			$arElementFields = Array("ACTIVE" => "N");  
			$res = $el->Update($del[$i], $arElementFields);	
			if($res)
				$str .= "deactivated: ".$arrForReport[$del[$i]]."\n";
			else 
				$str .= "DEACTIVATION ERROR! - ".$arrForReport[$del[$i]]."\n";	
			$str .= "\n";
			
		}
		*/
		
		
		
		
		
		// Деактивируем товары, не содержащие торговых предложений
		
		$arNewMinPrices = Array();  		// вычислим параллельно минимальные цены для каждого товара
		$dbRes = CIBlockElement::GetList(
			Array(),
			Array("IBLOCK_ID" => OFFERS_IBLOCK_ID, "ACTIVE" => "Y"), // инфоблок предложений
			false,
			false,
			Array("ID", "PROPERTY_CML2_LINK", "PROPERTY_CML2_LINK", "CATALOG_GROUP_1")
			);
		$arOffers = Array();
		while($arRes = $dbRes->GetNext()) 
		{	
			$arOffers[$arRes["PROPERTY_CML2_LINK_VALUE"]][] = $arRes["ID"];
			if( ($arRes["CATALOG_PRICE_1"] < $arNewMinPrices[$arRes["PROPERTY_CML2_LINK_VALUE"]]) || ($arNewMinPrices[$arRes["PROPERTY_CML2_LINK_VALUE"]] == 0) )
				$arNewMinPrices[$arRes["PROPERTY_CML2_LINK_VALUE"]] = $arRes["CATALOG_PRICE_1"];
		}

		$dbRes = CIBlockElement::GetList(
			Array(),
			Array("IBLOCK_ID" => PRODUCTS_IBLOCK_ID, "ACTIVE" => "Y"), // инфоблок товаров
			false,
			false,
			Array("ID", "NAME")
			);
		$arDel = Array();	
		while($arRes = $dbRes->GetNext()) 
		{	
			if(!isset($arOffers[$arRes["ID"]])) $arDel[$arRes["ID"]] = $arRes["NAME"];
		}
		$countEmptyProducts = count($arDel);

		$str .= "\n";
		if(count($arDel) <1 ) $str.="Not exists goods without SKU";
		foreach($arDel as $k=>$v) 
		{
			$str .= $v." - ";
			//$str .= "\n";
			$el = new CIBlockElement;
			$res = $el->Update($k, Array("ACTIVE" => "N")); 
			if($res) $str .= "deactivated"; else $str .= "DEACTIVATION ERROR!";
			
			$str .= "\n";
		}
		
		
		
		
		// расставим минимальные цены к товарам
		
		if(count($arNewMinPrices) > 0)
		{
			foreach($arNewMinPrices as $pid => $price)
				CIBlockElement::SetPropertyValuesEx($pid, PRODUCTS_IBLOCK_ID, Array("MIN_PRICE" => $price));
		}

		
		
		
		
		
		// Отсылаем результат
		
		$title =  date("d.m.Y H:i:s")." - ".$countEmptyProducts." products deactivated, ".$countEmptyOffers." offers deleted, ".$countOldOffers." old offers deleted";
		mail("turtell@yandex.ru", $title, $str);
	}
	
	
	if (@$_REQUEST['filename']=='import.xml')
	{
		smartSortSet();
	}
	
	
	
}







/*

// https://dev.1c-bitrix.ru/community/webdev/user/24164/blog/7651/

AddEventHandler('catalog', 'OnSuccessCatalogImport1C', 'customCatalogImportStep');

function customCatalogImportStep() 
{
    $stepInterval = (int) COption::GetOptionString("catalog", "1C_INTERVAL", "-");
    $startTime = time();
    // Флаг импорта файла торговых предложений
    $isOffers = strpos($_REQUEST['filename'], 'offers') !== false;
    $NS = &$_SESSION["BX_CML2_IMPORT"]["NS"];

    if (!isset($NS['custom']['lastId'])) {
        // Последний отработанный элемент для пошаговости.
        $NS['custom']['lastId'] = 0;
        $NS['custom']['counter'] = 0;
    }

    // Условия выборки элементов для обработки
    $arFilter = array(
        'IBLOCK_ID' => 1024,
        'ACTIVE' => 'Y', 
    );

    $res = CIBlockElement::GetList(array('ID' => 'ASC'), array_merge($arFilter, array('>ID' => $NS['custom']['lastId'])));
    $errorMessage = null;

    while ($arItem = $res->Fetch()) {
        
        // Что-нибудь делаем
        //if (updateElement($arItem['ID']) === false) {
        //   $error = true;
        //}
        

        if ($error === true) {
            $errorMessage = 'Что-то случилось.';
            break;
        }

        $NS['custom']['lastId'] = $arItem['ID'];
        $NS['custom']['counter']++;

        // Прерывание по времени шага
        if ($stepInterval > 0 && (time() - $startTime) > $stepInterval) {
            break;
        }
    }

    if ($arItem != false) {
        if ($errorMessage === null) {
            print "progress\n";
            print "Обработано " . $NS['custom']['counter'] . ' элементов, осталось ' . $res->SelectedRowsCount();
        } else {
            print "failure\n" . $errorMessage;
        }

        $contents = ob_get_contents();
        ob_end_clean();

        if (toUpper(LANG_CHARSET) != "WINDOWS-1251") {
            $contents = $GLOBALS['APPLICATION']->ConvertCharset($contents, LANG_CHARSET, "windows-1251");
        }

        header("Content-Type: text/html; charset=windows-1251");
        print $contents;
        exit;
    }

}
*/



// изменение разрешения доставки
// В НОВЫХ РЕАЛИЯХ НЕ НАДО

AddEventHandler("sale", "OnSaleDeliveryOrder", "OnSaleDeliveryOrderHandler");
function OnSaleDeliveryOrderHandler($ID, $val)
{
	/*
	if($val == "Y")
	{
		CModule::includeModule("sale");
		CModule::includeModule("inmarketing.grastin");
		$c = new CDeliveryGrastin();
		$test = "Y";
		$res = $c::ticketRequest($ID, $test);
		if($res) 
		{
			if($test=="Y") mail("info@newvay-shop.ru", $res." успешно сформирована тестовая заявка в личный кабинет Грастин", $res);
			else mail("info@newvay-shop.ru", $res." успешно сформирована заявка в личном кабинете Грастин", $res);
		}
		else
		{
			mail("info@newvay-shop.ru", $res." ОШИБКА формирования заявки в личном кабинете Грастин", $res."\r\nНеобходимо сообщить разработчику");
		}
	}
	*/
	//mail("turtell@yandex.ru", "delivery change", $ID." => ".$val);
}



// изменение оплаты

AddEventHandler("sale", "OnSalePayOrder", "OnSalePayOrderHandler");
function OnSalePayOrderHandler($ID, $val)
{

}



AddEventHandler("sale", "OnOrderUpdate", "OnOrderUpdateHandler");
function OnOrderUpdateHandler($ID, $arFields)
{
	/*
	if($arFields["UPDATED_1C"] == "Y")
	{
		ordersN2C();
	}
	*/
	
	/*
	$str = "Order ".$ID."\r\n";
	foreach($arFields as $k=>$v) $str .= $k." = ".$v."\r\n";
	if(!isset($arFields["LOCKED_BY"]))
	{
		mail("turtell@yandex.ru", "Order ".$ID." updated", $str);
		$arOrder = CSaleOrder::GetByID($ID);
		if(($arOrder["STATUS_ID"] == "N")&&($arFields["UPDATED_1C"] == "Y")&&($arFields["CANCELED"] != "Y"))
		{
			CSaleOrder::StatusOrder($ID, "C");
			
			// разрешаем доставку если оплата наличными
			if($arOrder["PAY_SYSTEM_ID"] == 1) 
				CSaleOrder::DeliverOrder($ID, "Y");
		}
	}
	*/
}





// =======================================================================================================================
//    Функция-агент для перевода заказов из статуса N в статус С (работает раз в минуту, смотреть в агентах)
//    переводятся заказы в статусе N, которые вернулись из 1с (проведены)
// =======================================================================================================================

/*
function ordersN2C()
{
	global $USER;
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("sale");
	$arIDs = Array();
	$dbOrders = CSaleOrder::GetList(Array("ID" => "asc"), Array("STATUS_ID" => "N"));
	while($arOrder = $dbOrders->Fetch())
	{
		$arIDs[] = $arOrder["ID"];
		//$arOrdersInfo[$arOrder["ID"]]["DATE_INSERT_FORMAT"] = $arOrder["DATE_INSERT_FORMAT"];
	}
	
	$arToUpdate = Array();
	$dbChanges = CSaleOrderChange::GetList(Array("ORDER_ID" => "ASC"), Array("ORDER_ID" => $arIDs));
	while ($arChange = $dbChanges->Fetch())
	{
		if($arChange["TYPE"] == "ORDER_1C_IMPORT") $arToUpdate[$arChange["ORDER_ID"]] = 1;
	}
	
	foreach($arToUpdate as $ID => $val)
	{
		// меняем статус
		CSaleOrder::StatusOrder($ID, "C");
		
		// разрешаем доставку если оплата наличными
		$arOrder = CSaleOrder::GetByID($ID);
		if($arOrder["PAY_SYSTEM_ID"] == 1) 
			CSaleOrder::DeliverOrder($ID, "Y");
	}
	
}
*/



/*
function ordersN2C()
{
	global $USER;
	
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("sale");

	// выясним в каких заказах нужно изменять статус и отправлять уведомление ($arToUpdate)
	// те которые не отменены и в истории изменений есть ORDER_1C_IMPORT и нынешний статус N
	$arOrdersInfo = Array();
	$arIDs = Array();
	$dbOrders = CSaleOrder::GetList(Array("ID" => "asc"), Array("STATUS_ID" => "N"));
	while($arOrder = $dbOrders->Fetch())
	{
		$arIDs[] = $arOrder["ID"];
		$arOrdersInfo[$arOrder["ID"]]["DATE_INSERT_FORMAT"] = $arOrder["DATE_INSERT_FORMAT"];
	}


	if(count($arIDs) > 0)
	{
		$arToUpdate = Array();
		$dbChanges = CSaleOrderChange::GetList(Array("ORDER_ID" => "ASC"), Array("ORDER_ID" => $arIDs));
		while ($arChange = $dbChanges->Fetch())
		{
			if($arChange["TYPE"] == "ORDER_1C_IMPORT") $arToUpdate[$arChange["ORDER_ID"]] = 1;
		}

		if(count($arToUpdate) > 0)
		{
			// Найдем общие данные для почтового шаблона
			$sale_email = COption::GetOptionString("sale", "order_email", "");
			$server_name = COption::GetOptionString("main", "server_name", "");
			$site_name = COption::GetOptionString("main", "site_name", "");
			$st = CSaleStatus::GetByID("C");
			$order_status = $st["NAME"];
			$order_desc = $st["DESCRIPTION"];

			// Находим еще недостающие данные для шаблона (EMAIL) из свойств заказа, меняем статус и отправляем уведомление
			foreach($arToUpdate as $ID => $val)
			{
				$arEventFields = Array();
				
				$db_props = CSaleOrderPropsValue::GetOrderProps($ID);
				while ($arProps = $db_props->Fetch())
				{
					if($arProps["CODE"] == "EMAIL") 
					{
						$email = $arProps["VALUE"];	
						break;
					}
				}
				
				$arEventFields = array( 
					"SALE_EMAIL" => $sale_email,
					"EMAIL" => $email,
					"SERVER_NAME" => $server_name,
					"ORDER_ID" => $ID,
					"SITE_NAME" => $site_name,
					"ORDER_DATE" => $arOrdersInfo[$ID]["DATE_INSERT_FORMAT"],
					"ORDER_STATUS" => $order_status,
					"ORDER_DESCRIPTION" => $order_desc,
					"TEXT" => ""
				);
				
				$arFields = Array("STATUS_ID" => "C");
				
				$status_res = CSaleOrder::Update($ID, $arFields);
				$send_res = CEvent::Send("SALE_STATUS_CHANGED_C", "s1", $arEventFields);



				// разрешаем доставку если оплата наличными
				$arOrder = CSaleOrder::GetByID($ID);
				if($arOrder["PAY_SYSTEM_ID"] == 1) 
					CSaleOrder::DeliverOrder($ID, "Y");


			}
		}

	}
	
	//mail("turtell@yandex.ru", "ordersN2C ran", date());
	return "ordersN2C();";
	
}
*/





// ================================================================================================================================
// Функция формирования данных arFields для шаблонов письма о новом заказе только по номеру заказа
// ================================================================================================================================

function getOrderMail($ORDER_ID) 
{
	if(intVal($ORDER_ID) > 0)
	{
	
		$arFields = Array();
	
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("catalog");
		CModule::IncludeModule("sale");
		
		
		
		
		
		
		// ВЫБЕРЕМ ДАННЫЕ ПО ЗАКАЗУ
		$arOrder = CSaleOrder::GetByID($ORDER_ID);
		$arPaySys = CSalePaySystem::GetByID($arOrder["PAY_SYSTEM_ID"], $arOrder["PERSON_TYPE_ID"]);

		if(strpos($arOrder["DELIVERY_ID"], ":")>0)
		{
			$t = explode(":", $arOrder["DELIVERY_ID"]);
			$delivery_id = $t[0];
			$profile_id = $t[1];
			//prn($id);
			//$arDelivery = CSaleDelivery::GetByID($id);	
			//prn($arDelivery);
			$dbResult = CSaleDeliveryHandler::GetBySID($delivery_id);
			if($arResult = $dbResult->Fetch())
			{
				foreach($arResult["PROFILES"] as $p_code => $profile)
				{
					if($p_code == $profile_id)
					{
						//prn($p_code);
						//prn($profile);
						$arDelivery["NAME"] = $profile["TITLE"];
					}
				}
				//prn($arResult);
			}
			
		}
		else
		{
			$arDelivery = CSaleDelivery::GetByID($arOrder["DELIVERY_ID"]);	
			if(!is_array($arDelivery)) $arDelivery["NAME"] = $arOrder["DELIVERY_ID"];	
		}
		//prn($arOrder["DELIVERY_ID"]);

		$arPersonType = CSalePersonType::GetByID($arOrder["PERSON_TYPE_ID"]);
		$arOrderProps = Array();
		$dbRes = CSaleOrderPropsValue::GetList(
			array("SORT" => "ASC"),
			array("ORDER_ID" => $ORDER_ID)
			);
		while($arRes = $dbRes->Fetch())
		{
			$arOrderProps[$arRes["CODE"]] = Array(
				"NAME" => $arRes["NAME"],
				"VALUE" => $arRes["VALUE"]
				);
		}
		//prn($arOrder);
		//prn($arPaySys);
		//prn($arDelivery);
		//prn($arOrderProps);
		
		
		
		

		// ВЫБЕРЕМ ДАННЫЕ ПО КОРЗИНЕ

		$arBasketList = array();
		$dbBasketItems = CSaleBasket::GetList(
			array("ID" => "ASC"),
			array("ORDER_ID" => $ORDER_ID),
			false,
			false,
			Array()
			);
		$arOfferID = Array();
		while ($arItem = $dbBasketItems->Fetch())
		{
			$arOfferID[] = $arItem["PRODUCT_ID"];
			if (CSaleBasketHelper::isSetItem($arItem))
				continue;
			$arBasketList[] = $arItem;
		}
		$arBasketList = getMeasures($arBasketList);
		
		
	
		$arExtData = Array();
		$arProductID = Array();
		$dbRes = CIBlockElement::GetList(
			Array(),
			Array("IBLOCK_ID" => OFFERS_IBLOCK_ID, "ID" => $arOfferID),
			false,
			false,
			Array("ID", "IBLOCK_ID", "PROPERTY_CML2_LINK", "PROPERTY_CML2_LINK.DETAIL_PAGE_URL", "PROPERTY_COLOR", "PROPERTY_SIZE")
			);
		while($arRes = $dbRes->GetNext())
		{
			$db = CIBlockElement::GetByID($arRes["PROPERTY_CML2_LINK_VALUE"]);
			$ar = $db->GetNext();
			$arExtData[$arRes["ID"]]["LINK"] = "http://www.sogrevay.ru".$ar["DETAIL_PAGE_URL"];
			$arExtData[$arRes["ID"]]["COLOR"] = $arRes["PROPERTY_COLOR_VALUE"];
			$arExtData[$arRes["ID"]]["SIZE"] = $arRes["PROPERTY_SIZE_VALUE"];
		}
		
		
		
		// Комментарий пользователя
		if(strLen($arOrder["USER_DESCRIPTION"])>0)
		{
			$arFields["USER_COMMENTS"] = "<p style='font-size:14px;'><b>Комментарий пользователя:</b><br/>".$arOrder["USER_DESCRIPTION"]."</p>";
		}
		
		
		
		// Стоимость доставки
		if($arOrder["PRICE_DELIVERY"] > 0)
		{
			$arFields["PRICE_DELIVERY"] = "<br/>(включая стоимость доставки: ".$arOrder["PRICE_DELIVERY"]." руб.)";
		}			
		
		
		
		// СОСТАВИМ strOrderProps - вывод свойтсв заказа
		$strOrderProps = "<p style='font-size:14px;'><b>Параметры заказа:</b></p>";
		$strOrderProps .= "<table style='border-top:1px solid #CCC'>";
		foreach($arOrderProps as $code => $arProp)
		{
			if($code == "DELIVERY_PARAM") continue;
			if($code == "DELIVERY_CODE") continue;
			if($code == "LOCATION")
			{
				$ar = CSaleLocation::GetByID($arProp["VALUE"], "ru"); 
				$strLoc = "";
				if(strLen($ar["COUNTRY_NAME"])>0) 	$strLoc .= $ar["COUNTRY_NAME"].", ";
				if(strLen($ar["REGION_NAME"])>0) 	$strLoc .= $ar["REGION_NAME"].", ";
				if(strLen($ar["CITY_NAME"])>0) 		$strLoc .= $ar["CITY_NAME"];
				$arProp["VALUE"] = $strLoc;
			}
			$strOrderProps .= "<tr style='border-bottom:1px solid #CCC;'><td style='width:200px; padding:2px 20px; font-size:14px; vertical-align: top;'>".$arProp["NAME"]."</td><td style='padding:2px 40px; font-size:14px; vertical-align: top;'>".$arProp["VALUE"]."</td></tr>";
		}
		$strOrderProps .= "</table>";
		$arFields["ORDER_PROPS"] = $strOrderProps;
		
		
		// СОСТАВИМ strOrderList - вывод корзины товаров

		$strOrderList = "<table border='0' style='width:760px; border-collapse:collapse;' cellpadding='5'>";
		$strOrderList .= "<thead>\n<tr style='border-bottom:1px solid #CCCCCC;'>";
		$strOrderList .= "<td style='padding:10px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>№</b></td>";
		$strOrderList .= "<td style='padding:10px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Фото</b></td>";
		$strOrderList .= "<td style='padding:10px; text-align:left; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Наименование</b></td>";
		$strOrderList .= "<td style='padding:10px; width:100px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Цена</b></td>";
		$strOrderList .= "<td style='padding:10px; width:80px; background-color: #D12A2D; color: #FFFFFF; white-space: nowrap; font-size:14px;'><b>Кол-во, шт.</b></td>";
		$strOrderList .= "<td style='padding:10px; width:100px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Сумма</b></td>";
		$strOrderList .= "</tr>\n</thead><tbody>\n"; 
		
		$num = 0;
		foreach ($arBasketList as $arItem)
		{
			$arImg = getPreview($arItem["PRODUCT_ID"],100,150);
			$num++;
			$measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOA_SHT");
			$strOrderList .= "<tr style='border-bottom:1px solid #CCCCCC;'>";
			$strOrderList .= "<td style='padding:10px; font-size:14px;'>".$num."</td>\n";
			$strOrderList .= "<td style='padding:10px; font-size:14px;'><img src='".$arImg["SRC"]."'></td>\n";
			$strOrderList .= "<td style='padding:10px; padding-right:30px; font-size:14px;' class='prod-name'>";
			$strOrderList .= "<b><a href='".$arExtData[$arItem["PRODUCT_ID"]]["LINK"]."'>".$arItem["NAME"]."</a></b>";
			if(strLen($arExtData[$arItem["PRODUCT_ID"]]["COLOR"]) > 0) $strOrderList .= "<br/><span class='prod' style='font-size:80%;'>Цвет: ".$arExtData[$arItem["PRODUCT_ID"]]["COLOR"]."</span>";
			if(strLen($arExtData[$arItem["PRODUCT_ID"]]["SIZE"]) > 0) $strOrderList .= "<br/><span class='prod' style='font-size:80%;'>Размер: ".$arExtData[$arItem["PRODUCT_ID"]]["SIZE"]."</span>";
			$strOrderList .= "</td>\n";
			$strOrderList .= "<td style='padding:10px; white-space: nowrap; font-size:14px;'>".$arItem["PRICE"]." руб.";
			if($arItem["DISCOUNT_VALUE"]>0) $strOrderList .= "<br/><span class='' style='color:red; font-size:90%;'>(с учетом скидки ".round($arItem["DISCOUNT_VALUE"])."%)</span>";
			$strOrderList .= "</td>\n";
			$strOrderList .= "<td style='padding:10px; text-align:center; font-size:14px;'>".round($arItem["QUANTITY"])."</td>\n";
			$strOrderList .= "<td style='padding:10px; white-space: nowrap; font-size:14px;'><b>".$arItem["PRICE"]*$arItem["QUANTITY"]." руб.</b></td>\n";
			$strOrderList .= "</tr>\n";
		}
		$strOrderList .= "</tbody></table>\n";

		$arFields["ORDER_LIST"] = $strOrderList;
		$arFields["PAYSYSTEM_NAME"] = $arPaySys["NAME"];
		$arFields["DELIVERY_NAME"] = $arDelivery["NAME"];

		return $arFields;

	}
	else
		return "Неверный номер заказа";
	
}







AddEventHandler("main", "OnEpilog", "CorrectPagen");
function CorrectPagen() {
	//global $USER;
	global $APPLICATION;
	if(isset($_REQUEST["PAGEN_1"])) 
	{
		$title = $APPLICATION->GetPageProperty("title");
		$t = explode("|", $title);
		if(count($t) > 1)
			$newtitle = trim($t[0])." - страница ".intVal($_REQUEST["PAGEN_1"])." | ".trim($t[1]);
		else 
			$newtitle = $title;
		$APPLICATION->SetPageProperty("title", $newtitle);
		$APPLICATION->SetPageProperty("keywords", "");
		$APPLICATION->SetPageProperty("description", "");
	} 
}






	
AddEventHandler("main", "OnEpilog", "Redirect404");
function Redirect404() {
	if (CHTTP::GetLastStatus()=='404 Not Found') 
	{
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		CHTTP::SetStatus("404 Not Found");
		include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header.php");
		include($_SERVER["DOCUMENT_ROOT"]."/404.php");
		include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer.php");
	}
}


?><? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/zyter.smtp/classes/general/cmodulezytersmtp.php");?>