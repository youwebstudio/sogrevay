<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

$Sum = CSalePaySystemAction::GetParamValue("SHOULD_PAY");
$ShopID = CSalePaySystemAction::GetParamValue("SHOP_ID");
$scid = CSalePaySystemAction::GetParamValue("SCID");
$customerNumber = CSalePaySystemAction::GetParamValue("ORDER_ID");
$orderDate = CSalePaySystemAction::GetParamValue("ORDER_DATE");
$orderNumber = CSalePaySystemAction::GetParamValue("ORDER_ID");
$paymentType = CSalePaySystemAction::GetParamValue("PAYMENT_VALUE");

$Sum = number_format($Sum, 2, ',', '');
?>
<div style="background: #FFFCF0;
    border: 3px solid #F6D85C;
    width: 100%;
    margin: 0;
    padding: 10px 20px;
	margin: 10px 0 20px 0;">
	<font class="tablebodytext">
	<p>Услугу предоставляет сервис онлайн-платежей <b>&laquo;Яндекс.Касса&raquo;</b></p>
	<div style="font-size:20px;">Сумма к оплате по счету: <b><?=$Sum?> руб.</b></div>
	</font>
	<?if(strlen(CSalePaySystemAction::GetParamValue("IS_TEST")) > 0):
		?>
		<form name="ShopForm" action="https://demomoney.yandex.ru/eshop.xml" method="post" target="_blank">
	<?else:
		?>
		<form name="ShopForm" action="https://money.yandex.ru/eshop.xml" method="post">
	<?endif;?>
	<font class="tablebodytext">
	<input name="ShopID" value="<?=$ShopID?>" type="hidden">
	<input name="scid" value="<?=$scid?>" type="hidden">
	<input name="customerNumber" value="<?=$customerNumber?>" type="hidden">
	<input name="orderNumber" value="<?=$orderNumber?>" type="hidden">
	<input name="Sum" value="<?=$Sum?>" type="hidden">
	<input name="paymentType" value="<?=$paymentType?>" type="hidden">
	<input name="cms_name" value="1C-Bitrix" type="hidden">

	<!-- <br /> -->
	<!-- Детали заказа:<br /> -->
	<!-- <input name="OrderDetails" value="заказ №<?=$orderNumber?> (<?=$orderDate?>)" type="hidden"> -->

	<input name="BuyButton" value="Оплатить" type="submit" style="background: green; font-family: 'Circe-Light', Arial, sans-serif; color: #fff; display: inline-block; border-radius: 0px; border: 1px solid green; text-align: center; text-decoration: none; font-size: 20px; cursor: pointer; text-shadow: none;    text-transform: uppercase; padding: 5px 20px; margin: 10px 0;">
	</form>
</div>