<?
// Листинг файла /bitrix/php_interface/include/sale_delivery/delivery_grastin.php
require_once($_SERVER['DOCUMENT_ROOT'] . '/temp/index5.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/xml.php'); 


CModule::IncludeModule("sale"); 

class CDeliveryVar
{
	function Init() 
	{
		return Array(
			
			/* Основное описание */
			"SID" 				=> "var", 
			"NAME" 				=> "Служба доставки Var",
			"DESCRIPTION" 		=> "", 
			"DESCRIPTION_INNER" => 
				"Обработчик службы курьерской доставки Grastin. Для функционирования необходимо "
				."наличие хотя бы одной группы местоположений. При настройке обработчика указывается "
				."фиксированная стоимость доставки для каждой группы местоположений. Для того, чтобы "
				."группа не участвовала в обработке, оставьте пустым поле стоимости для этой группы."
				."<br />"
				."<a href=\"/bitrix/admin/sale_location_group_admin.php?lang=ru\" target=\"_blank\">"
				."Редактировать группы местоположений"
				."</a>.",
			"BASE_CURRENCY" 	=> COption::GetOptionString("sale", "default_currency", "RUB"),
			"HANDLER" => __FILE__,
			
			/* Методы обработчика */
			"DBGETSETTINGS" 	=> array("CDeliveryVar", "GetSettings"),
			"DBSETSETTINGS" 	=> array("CDeliveryVar", "SetSettings"),
			"GETCONFIG" 		=> array("CDeliveryVar", "GetConfig"),

			"COMPABILITY" 		=> array("CDeliveryVar", "Compability"),      
			"CALCULATOR" 		=> array("CDeliveryVar", "Calculate"),      

			/* Список профилей доставки */
			"PROFILES" => Array(
				"boxberry" => Array(
					"TITLE" => "Boxberry",
					"DESCRIPTION" => "",
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
					),
				"boxberrySelfpickup" => Array(
					"TITLE" => "BoxberrySelfPickup",
					"DESCRIPTION" => "",
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
					),
				"grastin" => Array(
					"TITLE" => "Grastin",
					"DESCRIPTION" => "",
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
					),
				"post" => Array(
					"TITLE" => "Почта России",
					"DESCRIPTION" => "Срок доставки от 2 до 7 дней",
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
					)
					/*,
				"hermes" => Array(
					"TITLE" => "Hermes",
					"DESCRIPTION" => "Срок доставки от 2 до 7 дней",
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
					),
				"dpd" => Array(
					"TITLE" => "DPD",
					"DESCRIPTION" => "Срок доставки от 2 до 7 дней",
					"RESTRICTIONS_WEIGHT" => array(0), // без ограничений
					"RESTRICTIONS_SUM" => array(0), // без ограничений
					)
				*/
				)
			);
	}

  // настройки обработчика
	function GetConfig()
	{
		$arConfig = Array(
			"CONFIG_GROUPS" => Array(
				"group1" => "Группа 1",
				"group2" => "Группа 2",
				),
			"CONFIG" => Array(
				"param1_1" => Array(
					"TYPE" => "STRING",
					"DEFAULT" => "",
					"TITLE" => "Параметр 1.1",
					"GROUP" => "group1",
					), 
				"param1_2" => Array(
					"TYPE" => "STRING",
					"DEFAULT" => "",
					"TITLE" => "Параметр 1.2",
					"GROUP" => "group1",
					),
				"param2_1" => Array(
					"TYPE" => "STRING",
					"DEFAULT" => "",
					"TITLE" => "Параметр 2.1",
					"GROUP" => "group2",
					),
				"param2_2" => Array(
					"TYPE" => "STRING",
					"DEFAULT" => "",
					"TITLE" => "Параметр 2.2",
					"GROUP" => "group2",
					)
				)
			);
		return $arConfig; 
	}

  // подготовка настроек для занесения в базу данных
  function SetSettings($arSettings)
  {
    // Проверим список значений стоимости. Пустые значения удалим из списка.
    /*
	foreach ($arSettings as $key => $value) 
    {
      if (strlen($value) > 0)
        $arSettings[$key] = doubleval($value);
      else
        unset($arSettings[$key]);
    }
	*/
    // вернем значения в виде сериализованного массива.
    // в случае более простого списка настроек можно применить более простые методы сериализации.
    return serialize($arSettings);
  }

  // подготовка настроек, полученных из базы данных
  function GetSettings($strSettings)
  {
    // вернем десериализованный массив настроек
    return unserialize($strSettings);
  }
    
  // введем служебный метод, определяющий группу местоположения и возвращающий стоимость для этой группы.
  function __GetLocationPrice($LOCATION_ID, $arConfig)
  {
    // получим список групп для переданного местоположения
    $dbLocationGroups = CSaleLocationGroup::GetLocationList(array("LOCATION_ID" => $LOCATION_ID));
    
    while ($arLocationGroup = $dbLocationGroups->Fetch())
    {
      if (
        array_key_exists('price_'.$arLocationGroup["LOCATION_GROUP_ID"], $arConfig) 
        && 
        strlen($arConfig['price_'.$arLocationGroup["LOCATION_GROUP_ID"]]["VALUE"] > 0)
      )
      {
      // если есть непустая запись в массиве настроек для данной группы, вернем ее значение
        return $arConfig['price_'.$arLocationGroup["LOCATION_GROUP_ID"]]["VALUE"];
      }
    }
 
    // если не найдено подходящих записей, вернем false
    return false;
  }

  // метод проверки совместимости в данном случае практически аналогичен рассчету стоимости
  function Compability($arOrder, $arConfig)
  {
    // проверим наличие стоимости доставки
    /*$price = CDeliveryVar::__GetLocationPrice($arOrder["LOCATION_TO"], $arConfig);
    
    if ($price === false)
      return array(); // если стоимость не найдено, вернем пустой массив - не подходит ни один профиль
    else
      return array('simple'); // в противном случае вернем массив, содержащий идентфиикатор единственного профиля доставки
	*/
	
	$zipcode = 0;
	$arLocs = CSaleLocation::GetLocationZIP($arOrder["LOCATION_TO"]); 
	$arLocs = $arLocs->Fetch(); 
	$zipcode = $arLocs["ZIP"];
	
	
	//prn();
	//die();
	
	switch ($zipcode) 
	{
		case "143900": return Array("boxberry"); break;
		case "190000": return Array("grastin"); break;
		case "650000": return Array("boxberrySelfpickup"); break;
		case "690000": return Array("boxberrySelfpickup"); break;
		default: return Array("post");
	}
	
	//return Array("boxberry", "grastin");
  }
    
  // собственно, рассчет стоимости
  function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
  {
	  //prn($arOrder);
	  //die("99");
	  
	/*
	$grastinObj = new Grastin();
	$grastinObj->APIKEY = "c934ac77-3d80-4fb0-8732-856ae93d6d0b";
	$grastinObj->URL = "http://api.grastin.ru/api.php";  
	//prn($grastinObj->getDeliveryRegion());  
	
	$pr = 0;
	if($profile == "boxberry") 
	{
		//prn($arOrder["LOCATION_ZIP"]);
		
		$arPostcodes = Array();
		$re = $grastinObj->getBoxberryPostcode();
		foreach($re as $arCode) $arPostcodes[$arCode["Postcode"]] = $arCode["Id"];
		//prn($arPostcodes);
		if(isset($arPostcodes[$arOrder["LOCATION_ZIP"]])) $idpostcode = $arPostcodes[$arOrder["LOCATION_ZIP"]];
		else $idpostcode = 0;
		
		
		
		$arData = Array(
			"number" 			=> "zak",
			"idregion" 			=> "7d9e8b58-c94b-445d-97b8-c5cf57654f11",
			"selfPickup"		=> "false",
			"weight" 			=> $arOrder["WEIGHT"] == 0 ? "1000" : $arOrder["WEIGHT"],
			"assessedsumma" 	=> $arOrder["PRICE"],
			"summa" 			=> $arOrder["PRICE"],
			"bulky" 			=> "false",
			"volume"			=> "0",
			"width" 			=> "0",
			"height" 			=> "0",
			"length" 			=> "0",
			"transportcompany" 	=> "false",
			"idpostcode"		=> $idpostcode
			//"idpostcode"		=> "568ee109-117f-11e4-a9f5-00155d030401"
			//"paiddistance" 		=> "20",
			//"idpickup" 			=> "9cae6b7e-f583-11e4-87e3-00155d030401"
		);
		//prn($arData);
		//prn($grastinObj->getBoxberryPostcode());
		$res = $grastinObj->calcShipingCost($arData);
		//prn($res);
		$pr = $res["shippingcost"];
	}  
	  
	if($profile == "grastin") 
	{
		$pr = 100;  
	}  
	*/ 
	  
	  
	  
	  
    return Array(
      "RESULT" => "OK",
      "VALUE" => 1000,
	  "TRANSIT" => $arOrder["LOCATION_ZIP"]
    );
  }
}

// установим метод CDeliveryMySimple::Init в качестве обработчика события
AddEventHandler("sale", "onSaleDeliveryHandlersBuildList", array('CDeliveryVar', 'Init')); 
?>