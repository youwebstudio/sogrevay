<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
// файл прдназначен для синхронизации фото этого сайта с сайтом-образцом
// сначала выбираем в массив PREVIEW, DETAIL и MORE_PHOTO по своим файлам по заданному XML_ID с подсчетом md5 для файлов
// запрашиваем скрипт http://newvaysogrevay.ru/tools/get_photo.php и получаем аналогичную инфу с сайта-образца в текстовом формате
// сравниваем 
// обновляем в соответствии с сайтом-образцом при необходимости
// И ВСЕ ЭТО РЕКУРСИВНО ПО ВСЕМ ТОВАРАМ САЙТА - СКРИПТ ПЕРЕЗАПУСКАЕТСЯ САМ НА JAVASCRIPT
?>

<?

CModule::IncludeModule("iblock");

$interval = 30;
$arXML = Array();
$arList = Array();
$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 4), false, false, Array("ID", "XML_ID"));
while($arRes = $dbRes->GetNext())
{
	$arXML[] = $arRes["XML_ID"];
	$arList[$arRes["XML_ID"]] = $arRes["ID"];
}


$step = intVal($_GET["step"]);
//prn($step);
echo $step." - ".($step+$interval)." из ".count($arXML);
echo "<hr/>";


for($i=$step; $i<($step+$interval); $i++)
{
	if(!isset($arXML[$i])) break;
	//prn($i.". ".$arXML[$i]); 
	$xml = $arXML[$i];
	
	
	
	
	// соберем массив фото на этом сайте
	$dbRes = CIBlockElement::GetList(Array(), Array("XML_ID" => $xml), false, false, Array("ID", "XML_ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO"));
	$arPict = Array();
	while($arRes = $dbRes->GetNext())
	{
		if(!is_array($arPict["PREVIEW_PICTURE"])) 
			if($arRes["PREVIEW_PICTURE"]>0)
			{
				$t = Array();
				$t = CFile::GetFileArray($arRes["PREVIEW_PICTURE"]);
				$arPict["PREVIEW_PICTURE"] = Array(
					"SRC" => $t["SRC"],
					"DESCRIPTION" => $t["DESCRIPTION"],
					"SIZE" => $t["FILE_SIZE"],
					"MD5" => md5_file($_SERVER["DOCUMENT_ROOT"].$t["SRC"])
					);
			}
		if(!is_array($arPict["DETAIL_PICTURE"])) 
			if($arRes["DETAIL_PICTURE"]>0)
			{
				$t = Array();
				$t = CFile::GetFileArray($arRes["DETAIL_PICTURE"]);
				$arPict["DETAIL_PICTURE"] = Array(
					"SRC" => $t["SRC"],
					"DESCRIPTION" => $t["DESCRIPTION"],
					"SIZE" => $t["FILE_SIZE"],
					"MD5" => md5_file($_SERVER["DOCUMENT_ROOT"].$t["SRC"])
					);
			}
		if(is_array($arRes["PROPERTY_MORE_PHOTO_VALUE"]))
		{
			foreach($arRes["PROPERTY_MORE_PHOTO_VALUE"] as $imgID)
			{
				if($imgID>0) 
				{
					$t = array();
					$t = CFile::GetFileArray($imgID);
					$arPict["MORE_PHOTO"][] = Array(
						"SRC" => $t["SRC"],
						"DESCRIPTION" => $t["DESCRIPTION"],
						"SIZE" => $t["FILE_SIZE"],
						"MD5" => md5_file($_SERVER["DOCUMENT_ROOT"].$t["SRC"])
					);
				}
			}
		}
		else 
		{
			if($arRes["PROPERTY_MORE_PHOTO_VALUE"] > 0)
			{
				$t = array();
				$t = CFile::GetFileArray($arRes["PROPERTY_MORE_PHOTO_VALUE"]);
				$arPict["MORE_PHOTO"][] = Array(
					"SRC" => $t["SRC"],
					"DESCRIPTION" => $t["DESCRIPTION"],
					"SIZE" => $t["FILE_SIZE"],
					"MD5" => md5_file($_SERVER["DOCUMENT_ROOT"].$t["SRC"])
				);
				$arPict["MORE_PHOTO"][] = CFile::GetFileArray($arRes["PROPERTY_MORE_PHOTO_VALUE"]);
			}
		}
	}
	//prn($arPict);
	
	
	
	
	
	
	$res = QueryGetData("www.newvay.ru", 80, "/tools/get_photo.php", "xml=".$xml, $errno, $errstr);
	$t = explode("\n", $res);
	$arRemote = Array();
	foreach($t as $row)
	{
		if(trim($row) == "") continue;
		$tt = explode(";", $row);
		$one = Array(
				"ID" => $tt[1],
				"SRC" => $tt[2],
				"MD5" => $tt[3],
				"DESCRIPTION" => $tt[4]
			);
		if($tt[0] == "PREVIEW_PICTURE") $arRemote["PREVIEW_PICTURE"] = $one;
		if($tt[0] == "DETAIL_PICTURE") $arRemote["DETAIL_PICTURE"] = $one;
		if($tt[0] == "MORE_PHOTO")	$arRemote["MORE_PHOTO"][] = $one;
	}
	//prn($arRemote);
	
	
	
	
	
	
	
	
	// сравниваем
	$arActions = Array(
		"PREVIEW_PICTURE" => "",
		"DETAIL_PICTURE" => "",
		"MORE_PHOTO" => ""
	);
	$arAction = Array();

	// превью
	if(($arPict["PREVIEW_PICTURE"]["MD5"] == $arRemote["PREVIEW_PICTURE"]["MD5"]) && ($arPict["PREVIEW_PICTURE"]["DESCRIPTION"] == $arRemote["PREVIEW_PICTURE"]["DESCRIPTION"]))
		$arAction["PREVIEW_PICTURE"] = "SYNC";
	else
	{
		if(is_array($arPict["PREVIEW_PICTURE"]) && !is_array($arRemote["PREVIEW_PICTURE"])) $arAction["PREVIEW_PICTURE"] = "DEL";
		if(!is_array($arPict["PREVIEW_PICTURE"]) && is_array($arRemote["PREVIEW_PICTURE"])) $arAction["PREVIEW_PICTURE"] = "ADD";
		if(is_array($arPict["PREVIEW_PICTURE"]) && is_array($arRemote["PREVIEW_PICTURE"])) 	$arAction["PREVIEW_PICTURE"] = "UPD";
	}
	
	// детальное
	if(($arPict["DETAIL_PICTURE"]["MD5"] == $arRemote["DETAIL_PICTURE"]["MD5"]) && ($arPict["DETAIL_PICTURE"]["DESCRIPTION"] == $arRemote["DETAIL_PICTURE"]["DESCRIPTION"]))
		$arAction["DETAIL_PICTURE"] = "SYNC";
	else
	{
		if(is_array($arPict["DETAIL_PICTURE"]) && !is_array($arRemote["DETAIL_PICTURE"])) $arAction["DETAIL_PICTURE"] = "DEL";
		if(!is_array($arPict["DETAIL_PICTURE"]) && is_array($arRemote["DETAIL_PICTURE"])) $arAction["DETAIL_PICTURE"] = "ADD";
		if(is_array($arPict["DETAIL_PICTURE"]) && is_array($arRemote["DETAIL_PICTURE"]))  $arAction["DETAIL_PICTURE"] = "UPD";
	}
	
	// more_photo
	$arAction["MORE_PHOTO"] = "SYNC";
	if(!is_array($arPict["MORE_PHOTO"]) && is_array($arRemote["MORE_PHOTO"])) $arAction["MORE_PHOTO"] = "UPD";
	if(count($arPict["MORE_PHOTO"]) != count($arRemote["MORE_PHOTO"])) $arAction["MORE_PHOTO"] = "UPD";
	foreach($arPict["MORE_PHOTO"] as $k => $arPic)
	{
		if(($arPict["MORE_PHOTO"][$k]["MD5"] == $arRemote["MORE_PHOTO"][$k]["MD5"]) && ($arPict["MORE_PHOTO"][$k]["DESCRIPTION"] == $arRemote["MORE_PHOTO"][$k]["DESCRIPTION"]))
			continue;
		else
		{
			$arAction["MORE_PHOTO"] = "UPD";
			break;
		}
	}
	//prn($arAction);
	
	
	
	
	
	if(($arAction["PREVIEW_PICTURE"] == "SYNC")&&($arAction["DETAIL_PICTURE"] == "SYNC")&&($arAction["MORE_PHOTO"] == "SYNC"))
	{
		echo $xml." - SYNC<br/>";
		$sync = true;
	}
	else 
	{
		echo $xml." - NEED SYNCRHONIZE<br/>";
		$sync = false;
	}
	
	
	
	
	if(!$sync)
	{
		//prn($arPict);
		//prn($arRemote);
		//prn($arList[$xml]);
		$ID = $arList[$xml];
		
		// выполняем действия по PREVIEW
		if(($arAction["PREVIEW_PICTURE"] == "ADD")||($arAction["PREVIEW_PICTURE"] == "UPD"))
		{
			$arImg = CFile::MakeFileArray($arRemote["PREVIEW_PICTURE"]["SRC"]);
			$arImg["description"] = $arRemote["PREVIEW_PICTURE"]["DESCRIPTION"];
			$el = new CIBlockElement;
			$res = $el->Update($ID, Array("PREVIEW_PICTURE" => $arImg));
		}
		if($arAction["PREVIEW_PICTURE"] == "DEL")
		{
			$el = new CIBlockElement;
			$res = $el->Update($ID, Array("PREVIEW_PICTURE" => Array("del" => "Y")));
		}
		
		
		// выполняем действия по DETAIL
		if(($arAction["DETAIL_PICTURE"] == "ADD")||($arAction["DETAIL_PICTURE"] == "UPD"))
		{
			$arImg = CFile::MakeFileArray($arRemote["DETAIL_PICTURE"]["SRC"]);
			$arImg["description"] = $arRemote["DETAIL_PICTURE"]["DESCRIPTION"];
			$el = new CIBlockElement;
			$res = $el->Update($ID, Array("DETAIL_PICTURE" => $arImg));
		}
		if($arAction["DETAIL_PICTURE"] == "DEL")
		{
			$el = new CIBlockElement;
			$res = $el->Update($ID, Array("DETAIL_PICTURE" => Array("del" => "Y")));
		}
		
		// выполняем действия по MORE_PHOTO
		if($arAction["MORE_PHOTO"] == "UPD")
		{
			$arProps = Array();
			foreach($arRemote["MORE_PHOTO"] as $k => $arImg)
			{
				$arOne = CFile::MakeFileArray($arRemote["MORE_PHOTO"][$k]["SRC"]);
				$arOne["description"] = $arRemote["MORE_PHOTO"][$k]["DESCRIPTION"];
				$arProps["MORE_PHOTO"][] = $arOne;
			}
			if(count($arProps) < 1) 
			{
				foreach($arPict["MORE_PHOTO"] as $arImg) 
				{
					$arOne = CFile::MakeFileArray($arImg["ID"]);
					$arOne["del"] = "Y";
					$arProps["MORE_PHOTO"][] = $arOne;
				}
			}
			CIBlockElement::SetPropertyValuesEx($ID, false, $arProps);
		}
		CIBlock::clearIblockTagCache(4);
		echo "===========================================> PRODUCT WAS SYNCHRONIZED!!!!<br/>";
		mail("turtell@yandex.ru", "NEWVAY to SOGREVAY sync images for ".$xml, $xml);
	}

	
	
	
	
	
	//echo $i.". ".$arXML[$i]."<br/>";
}

if(($step+$interval)<=count($arXML))
{
	//echo '<hr/><a id="go" href="/tools/img_sync_all.php?step='.($step+$interval).'">Следующий шаг</a>';
	?>
	<script>
		setTimeout( function(){document.location.href='/tools/img_sync_all.php?step=<?=($step+$interval)?>';}, 5000);
	</script>
	<?
}
else echo "FINISHED";


?>
