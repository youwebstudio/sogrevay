<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?
// =======================================================================================================================
//   Функция подстановки нужной превью картинки по коду торгового предложения
//    возвращает массив картинки
//    если заданы width и height, то преобразует к заданному размеру
// =======================================================================================================================

function getOfferPictures_($offerID, $width=false, $height=false)
{
	$productID = CCatalogSku::GetProductInfo($offerID);
	//pra($productID);
		
	// если offer неактивный, то у него пропадает привязка к товару(!), на этот случай ищем товар по части XML_ID
	if(!is_array($productID))
	{
		$dbOffer = CIBlockElement::GetByID($offerID);
		if($arOffer = $dbOffer->Fetch())
		{
			$product_xml_id = $arOffer["XML_ID"];
		}
		if(strLen(trim($product_xml_id))>0)
		{
			$t = explode("#", $product_xml_id);
			$dbPr = CIBlockElement::GetList(Array(), Array("XML_ID" => $t[0]), false, false, Array("ID", "IBLOCK_ID"));
			if($arPr = $dbPr->Fetch()) $productID = Array("ID" => $arPr["ID"], "IBLOCK_ID" => $arPr["IBLOCK_ID"]);
		}
	}
		
	if(is_array($productID))
	{
		$dbRes = CIBlockElement::getList(
			Array(),
			Array(
				"IBLOCK_ID" => $productID["IBLOCK_ID"],
				"ID" => $productID["ID"]
				),
			false,
			false,
			Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_ID", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO")
			);
		$arPictures = Array();
		
		unset($detail_picture);
		while($arRes = $dbRes->GetNext())
		{
			if(($arRes["DETAIL_PICTURE"] > 0)&&(!isset($detail_picture)))
			{
				$detail_picture = $arRes["DETAIL_PICTURE"];	
				$t1 = CFile::GetFileArray($detail_picture);
				$e1 = explode(" ЦВЕТ ", strtoupper($t1["DESCRIPTION"]));
				if(count($e1)>1) $arPictures[trim($e1[1])][] = $t1;
				else $arPictures[trim($e1[0])][] = $t1;
			}
			
			if(is_array($arRes["PROPERTY_MORE_PHOTO_VALUE"]))
			{
				foreach($arRes["PROPERTY_MORE_PHOTO_VALUE"] as $arP)
				{
					$t = CFile::GetFileArray($arP);
					$e = explode(" ЦВЕТ ", strtoupper($t["DESCRIPTION"]));
					//$arPictures[trim($e[1])][] = $t;
					if(count($e)>1) $arPictures[trim($e[1])][] = $t;
					else $arPictures[trim($e[0])][] = $t;
				}
			}
			else
			{
				$t = CFile::GetFileArray($arRes["PROPERTY_MORE_PHOTO_VALUE"]);
				$e = explode(" ЦВЕТ ", strtoupper($t["DESCRIPTION"]));
				//$arPictures[trim($e[1])][] = $t;
				if(count($e)>1) $arPictures[trim($e[1])][] = $t;
				else $arPictures[trim($e[0])][] = $t;
			}
		}
		prn($arPictures);
		
		
		$dbRes = CIBlockElement::GetByID($offerID);
		if($arElement = $dbRes->Fetch())
		{
			foreach($arPictures as $color => $arPicture)
			{
				if(strpos(" ".strtoupper($arElement["NAME"]), $color) > 0)
				{
					$res = $arPicture;
					break;
				}
			}
		}
	}
	
	if(is_array($res))
	{
		if($width && $height)
		{
			$f = CFile::ResizeImageGet($res["ID"], array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$res["WIDTH"] = $f["width"];
			$res["HEIGHT"] = $f["height"];
			$res["SRC"] = $f["src"];
		}
		return $res;
	}
	else
	{
		$nofoto = CFile::GetFileArray(NOFOTO_FILE_ID);
		if($width && $height)
		{
			$f = CFile::ResizeImageGet(NOFOTO_FILE_ID, array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$nofoto["WIDTH"] = $f["width"];
			$nofoto["HEIGHT"] = $f["height"];
			$nofoto["SRC"] = $f["src"];
		}
		return Array($nofoto);
	}

}

$arImg = getOfferPictures_($_GET["OFFER_ID"]);

foreach($arImg as $ar)
{
	echo $ar["DESCRIPTION"]."<br>";
	echo "<img src='".$ar["SRC"]."'/><br>";
}

prn($arImg);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>