<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule("iblock");
prn(34);

$filter = array("=IBLOCK_ID"=>4);

$nav = new \Bitrix\Main\UI\PageNavigation("nav-more-news");
$nav->allowAllRecords(true)
   ->setPageSize(5)
   ->initFromUri();
//prn($nav);

$newsList = \Bitrix\Iblock\ElementTable::getList(
   array(
      "filter" => $filter,
	  "select" => Array("ID", "NAME"),
	  "order" => Array("ID" => "desc"),
      "count_total" => true,
      "offset" => $nav->getOffset(),
      "limit" => $nav->getLimit(),
   )
);
//prn($newsList);

$nav->setRecordCount($newsList->getCount());

while($news = $newsList->fetch())
{
	prn($news);
}
?>

<?
$APPLICATION->IncludeComponent(
   "bitrix:main.pagenavigation",
   "",
   array(
      "NAV_OBJECT" => $nav,
      "SEF_MODE" => "N",
   ),
   false
);
?>
