<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<?
// получение гео-данных от сервисе Geo-IP
function get_geo_data_($ip) 
{
	$arParams["IP"] = $ip;
	//$arParams['IP'] = "178.217.24.5"; // подольск
	$strQueryText = QueryGetData(
		"194.85.91.253",
		8090,
		"/geo/geo.html",
		"address=<ipquery><fields><all/></fields><ip-list><ip>".$arParams['IP']."</ip></ip-list></ipquery>",
		$error_number,
		$error_text,
		"POST"
	   );
	$objXML  = new CDataXML();
	$objXML->LoadString($strQueryText);
	$arData = $objXML->GetArray();
	$arOneData = $arData['ip-answer']['#']['ip'][0];
	if(is_array($arOneData)) 
	{
		$arResult['FULL_INFO'] = Array(
			"IP"=>$arOneData['@']['value'],
			"CITY"=>$arOneData['#']['city']['0']['#'],
			"INET_STATUS"=>$arOneData['#']['inet-status']['0']['#'],
			"INET_DESCR"=>$arOneData['#']['inet-descr']['0']['#'],
			"INETNUM"=>$arOneData['#']['inetnum']['0']['#'],
			"REGION"=>$arOneData['#']['region']['0']['#'],
			"DISTRICT"=>$arOneData['#']['district']['0']['#'],
			"LAT"=>$arOneData['#']['lat']['0']['#'],
			"LNG"=>$arOneData['#']['lng']['0']['#']
			);
	}
	return $arResult['FULL_INFO'];

}



?>

<?if(($_SESSION["GEO_DATA"]["CITY"] == "Москва")|| ($_SESSION["GEO_DATA"]["REGION"] == "Московская область")):?>
	<p>москва и область</p>
<?else:?>
	<p>другой регион</p>
<?endif?>
 

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>