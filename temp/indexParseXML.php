<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// ==================================================================================
// ВНИМАНИЕ!
// В данном импорте действуют как минимум следующие ограничения:
// - валюта РУБЛИ
// - единицы измерений ШТУКИ
// - вложенность разделов каталога не больше трех (жестко три цикла, можно увеличить копипастом)
// - каждый товар может быть привязан к одному разделу (просто не знаю как выглядит когда к нескольким)
// ================================================================================== 

$fileImport = $_SERVER["DOCUMENT_ROOT"]."/temp/import/import.xml";
$fileOffers = $_SERVER["DOCUMENT_ROOT"]."/temp/import/offers.xml";

if(file_exists($fileImport))
{
	$xml = simplexml_load_file($fileImport);

	$arItems = Array();
	$arGroups = Array();
	$arGroupsTree = Array();
	$arPriceTypes = Array();
	$arFeatureTypes = Array();
	
	foreach($xml->Классификатор->Группы->Группа as $arGroup1)
	{
		$arGroupsTree[trim($arGroup1->Ид)] = Array("NAME" => trim($arGroup1->Наименование));
		$arGroups[trim($arGroup1->Ид)] = Array(
			"NAME" => trim($arGroup1->Наименование),
			"PARENT" => ""
			);
		foreach($arGroup1->Группы->Группа as $arGroup2)
		{
			$arGroupsTree[trim($arGroup1->Ид)]["GROUPS"][trim($arGroup2->Ид)] = Array("NAME" => trim($arGroup2->Наименование));
			$arGroups[trim($arGroup2->Ид)] = Array(
				"NAME" => trim($arGroup2->Наименование),
				"PARENT" => trim($arGroup1->Ид)
				);
			foreach($arGroup2->Группы->Группа as $arGroup3)
			{
				$arGroupsTree[trim($arGroup1->Ид)]["GROUPS"][trim($arGroup2->Ид)]["GROUPS"][trim($arGroup3->Ид)] = Array("NAME" => trim($arGroup3->Наименование));
				$arGroups[trim($arGroup3->Ид)] = Array(
					"NAME" => trim($arGroup3->Наименование),
					"PARENT" => trim($arGroup2->Ид)
					);
			}
		}
	}
	
	foreach($xml->Классификатор->Свойства->Свойство as $arFeatureType)
	{
		$arFeatureTypes[trim($arFeatureType->Ид)]["NAME"] = trim($arFeatureType->Наименование);
		$arFeatureTypes[trim($arFeatureType->Ид)]["TYPE"] = trim($arFeatureType->ТипЗначений) == "" ? "Строка" : trim($arFeatureType->ТипЗначений);
		if(trim($arFeatureType->ТипЗначений) == "Справочник") 
		{
			foreach($arFeatureType->ВариантыЗначений->Справочник as $arValue)
			{
				$arFeatureTypes[trim($arFeatureType->Ид)]["VALUES"][trim($arValue->ИдЗначения)] = trim($arValue->Значение);
			}
		}
	}
	
	
	foreach($xml->Каталог->Товары->Товар as $arItem)
	{
		//prn($arItem);
		$id = trim($arItem->Ид);
		$arItems[$id]["NAME"] = trim($arItem->Наименование);
		$arItems[$id]["SECTION"] = trim($arItem->Группы->Ид);
		$arItems[$id]["SECTION_STR"] = $arGroups[trim($arItem->Группы->Ид)]["NAME"];
		$arItems[$id]["ARTIKUL"] = trim($arItem->Артикул);
		if($arItem->Картинка)
		{
			foreach($arItem->Картинка as $arPicture)
			{
				//echo $arPicture."<br/>";
				$arItems[$id]["IMAGES"][trim($arPicture)] = trim($arPicture);
			}
		}
		
		foreach($arItem->ЗначенияРеквизитов->ЗначениеРеквизита as $arRekv)
			{
				if(trim($arRekv->Наименование) == "ОписаниеФайла")
				{
					$t = explode("#", trim($arRekv->Значение));
					$arItems[$id]["IMAGES"][$t[0]] = $t[1];
				}
				if(trim($arRekv->Наименование) == "Вес")
				{
					$arItems[$id]["WEIGHT"] = trim($arRekv->Значение);
				}
			}
		
		foreach($arItem->ЗначенияСвойств->ЗначенияСвойства as $arFeatureValue)
		{
			//$arItems[$id]["FEATURES_CODES"][trim($arFeatureValue->Ид)] = trim($arFeatureValue->Значение);   // коды свойств
			if($arFeatureTypes[trim($arFeatureValue->Ид)]["TYPE"] == "Справочник")
			{
				$arItems[$id]["FEATURES"][$arFeatureTypes[trim($arFeatureValue->Ид)]["NAME"]] = $arFeatureTypes[trim($arFeatureValue->Ид)]["VALUES"][trim($arFeatureValue->Значение)];
			}
			else
			{
				$arItems[$id]["FEATURES"][$arFeatureTypes[trim($arFeatureValue->Ид)]["NAME"]] = trim($arFeatureValue->Значение);
			}
			
		}
		
	}

}





if(file_exists($fileOffers))
{
	$xml = simplexml_load_file($fileOffers);
	
	$arOffers = Array();
	$arPriceTypes = Array();
	

		
	// парсим типы цен
	foreach($xml->ПакетПредложений->ТипыЦен->ТипЦены as $arPriceType)
	{
		$arPriceTypes[trim($arPriceType->Ид)] = trim($arPriceType->Наименование);
	}

	
	
	// парсим торговые предложения
	$num = 0;
	$numZero = 0;
	foreach($xml->ПакетПредложений->Предложения->Предложение as $k=>$arItem)
	{
		$num++;
		$arPrices = Array();
		$arFeatures = Array();
		foreach($arItem->Цены->Цена as $arPrice)
		{
			$arPrices[trim($arPrice->ИдТипаЦены)] = trim($arPrice->ЦенаЗаЕдиницу);
		}
		
		foreach($arItem->ХарактеристикиТовара->ХарактеристикаТовара as $arFeature)
		{
			$arFeatures[trim($arFeature->Наименование)] = trim($arFeature->Значение);
		}

		$arOffers[trim($arItem->Ид)] = Array(
			"NAME" => trim($arItem->Наименование),
			"COUNT" => trim($arItem->Количество) == "" ? "0" : trim($arItem->Количество),
			"FEATURES" => $arFeatures,
			"PRICE" => $arPrices
			);
			
		$t = explode("#", trim($arItem->Ид));
		//prn($t[0]);
		$arItems[$t[0]]["OFFERS"][trim($arItem->Ид)] = Array(
			"NAME" => trim($arItem->Наименование),
			"COUNT" => trim($arItem->Количество) == "" ? "0" : trim($arItem->Количество),
			"FEATURES" => $arFeatures,
			"PRICE" => $arPrices
			);
			
		if(intVal($arItem->Количество) < 1) $numZero++;
		//prn($arItem);
		//if($num>10) break;
	}
	
	//prn($arOffers);
		

}

prn($arGroups);
prn($arGroupsTree);
prn($arPriceTypes);
prn($arFeatureTypes);
prn($arItems);


?>