<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


function getPreview_($offerID, $width=false, $height=false)
{
	$productID = CCatalogSku::GetProductInfo($offerID);
	//prn($productID);
	
	$dbRes = CIBlockElement::getList(
		Array(),
		Array(
			"IBLOCK_ID" => $productID["IBLOCK_ID"],
			"ID" => $productID["ID"]
			),
		false,
		false,
		Array("ID", "IBLOCK_ID", "NAME", "IBLOCK_ID", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO")
		);
	$arPictures = Array();
	while($arRes = $dbRes->GetNext())
	{
		if(is_array($arRes["PROPERTY_MORE_PHOTO_VALUE"]))
		{
			foreach($arRes["PROPERTY_MORE_PHOTO_VALUE"] as $arP)
			{
				$t = CFile::GetFileArray($arP);
				$e = explode("ЦВЕТ", strtoupper($t["DESCRIPTION"]));
				$arPictures[trim($e[1])][] = $t;
			}
		}
		else
		{
			$t = CFile::GetFileArray($arRes["PROPERTY_MORE_PHOTO_VALUE"]);
			$e = explode("ЦВЕТ", strtoupper($t["DESCRIPTION"]));
			$arPictures[trim($e[1])][] = $t;
		}
		$detail_picture = $arRes["DETAIL_PICTURE"];
	}
	$t1 = CFile::GetFileArray($detail_picture);
	$e1 = explode("ЦВЕТ", strtoupper($t1["DESCRIPTION"]));
	$arPictures[trim($e1[1])][] = $t1;
	
	$dbRes = CIBlockElement::GetByID($offerID);
	if($arElement = $dbRes->Fetch())
	{
		foreach($arPictures as $color => $arPicture)
		{
			if(strpos(" ".strtoupper($arElement["NAME"]), $color) > 0)
			{
				$res = $arPicture[0];
				break;
			}
		}
	}
	
	if(is_array($res))
	{
		if($width && $height)
		{
			$f = CFile::ResizeImageGet($res["ID"], array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$res["WIDTH"] = $f["width"];
			$res["HEIGHT"] = $f["height"];
			$res["SRC"] = $f["src"];
		}
		return $res;
	}
	else
	{
		$nofoto = CFile::GetFileArray(NOFOTO_FILE_ID);
		if($width && $height)
		{
			$f = CFile::ResizeImageGet(NOFOTO_FILE_ID, array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$nofoto["WIDTH"] = $f["width"];
			$nofoto["HEIGHT"] = $f["height"];
			$nofoto["SRC"] = $f["src"];
		}
		return $nofoto;
	}

}



function getOrderMail_($ORDER_ID) 
{
	if(intVal($ORDER_ID) > 0)
	{
	
		$arFields = Array();
	
		CModule::IncludeModule("iblock");
		CModule::IncludeModule("catalog");
		CModule::IncludeModule("sale");
		
		
		
		
		
		
		// ВЫБЕРЕМ ДАННЫЕ ПО ЗАКАЗУ
		$arOrder = CSaleOrder::GetByID($ORDER_ID);
		$arPaySys = CSalePaySystem::GetByID($arOrder["PAY_SYSTEM_ID"], $arOrder["PERSON_TYPE_ID"]);

		if(strpos($arOrder["DELIVERY_ID"], ":")>0)
		{
			$t = explode(":", $arOrder["DELIVERY_ID"]);
			$delivery_id = $t[0];
			$profile_id = $t[1];
			//prn($id);
			//$arDelivery = CSaleDelivery::GetByID($id);	
			//prn($arDelivery);
			$dbResult = CSaleDeliveryHandler::GetBySID($delivery_id);
			if($arResult = $dbResult->Fetch())
			{
				foreach($arResult["PROFILES"] as $p_code => $profile)
				{
					if($p_code == $profile_id)
					{
						//prn($p_code);
						//prn($profile);
						$arDelivery["NAME"] = $profile["TITLE"];
					}
				}
				//prn($arResult);
			}
			
		}
		else
		{
			$arDelivery = CSaleDelivery::GetByID($arOrder["DELIVERY_ID"]);	
			if(!is_array($arDelivery)) $arDelivery["NAME"] = $arOrder["DELIVERY_ID"];	
		}
		//prn($arOrder["DELIVERY_ID"]);

		$arPersonType = CSalePersonType::GetByID($arOrder["PERSON_TYPE_ID"]);
		$arOrderProps = Array();
		$dbRes = CSaleOrderPropsValue::GetList(
			array("SORT" => "ASC"),
			array("ORDER_ID" => $ORDER_ID)
			);
		while($arRes = $dbRes->Fetch())
		{
			$arOrderProps[$arRes["CODE"]] = Array(
				"NAME" => $arRes["NAME"],
				"VALUE" => $arRes["VALUE"]
				);
		}
		//prn($arOrder);
		//prn($arPaySys);
		//prn($arDelivery);
		//prn($arOrderProps);
		
		
		
		

		// ВЫБЕРЕМ ДАННЫЕ ПО КОРЗИНЕ

		$arBasketList = array();
		$dbBasketItems = CSaleBasket::GetList(
			array("ID" => "ASC"),
			array("ORDER_ID" => $ORDER_ID),
			false,
			false,
			Array()
			);
		$arOfferID = Array();
		while ($arItem = $dbBasketItems->Fetch())
		{
			$arOfferID[] = $arItem["PRODUCT_ID"];
			if (CSaleBasketHelper::isSetItem($arItem))
				continue;
			$arBasketList[] = $arItem;
		}
		$arBasketList = getMeasures($arBasketList);
		
		
	
		$arExtData = Array();
		$arProductID = Array();
		$dbRes = CIBlockElement::GetList(
			Array(),
			Array("IBLOCK_ID" => OFFERS_IBLOCK_ID, "ID" => $arOfferID),
			false,
			false,
			Array("ID", "IBLOCK_ID", "PROPERTY_CML2_LINK", "PROPERTY_CML2_LINK.DETAIL_PAGE_URL", "PROPERTY_COLOR", "PROPERTY_SIZE")
			);
		while($arRes = $dbRes->GetNext())
		{
			$db = CIBlockElement::GetByID($arRes["PROPERTY_CML2_LINK_VALUE"]);
			$ar = $db->GetNext();
			$arExtData[$arRes["ID"]]["LINK"] = "http://www.sogrevay.ru".$ar["DETAIL_PAGE_URL"];
			$arExtData[$arRes["ID"]]["COLOR"] = $arRes["PROPERTY_COLOR_VALUE"];
			$arExtData[$arRes["ID"]]["SIZE"] = $arRes["PROPERTY_SIZE_VALUE"];
		}
		prn($arExtData);
		
		
		
		// Комментарий пользователя
		if(strLen($arOrder["USER_DESCRIPTION"])>0)
		{
			$arFields["USER_COMMENTS"] = "<p style='font-size:14px;'><b>Комментарий пользователя:</b><br/>".$arOrder["USER_DESCRIPTION"]."</p>";
		}
		
		
		
		// Стоимость доставки
		if($arOrder["PRICE_DELIVERY"] > 0)
		{
			$arFields["PRICE_DELIVERY"] = "<br/>(включая стоимость доставки: ".$arOrder["PRICE_DELIVERY"]." руб.)";
		}			
		
		
		
		// СОСТАВИМ strOrderProps - вывод свойтсв заказа
		$strOrderProps = "<p style='font-size:14px;'><b>Параметры заказа:</b></p>";
		$strOrderProps .= "<table style='border-top:1px solid #CCC'>";
		foreach($arOrderProps as $code => $arProp)
		{
			if($code == "DELIVERY_PARAM") continue;
			if($code == "LOCATION")
			{
				$ar = CSaleLocation::GetByID($arProp["VALUE"], "ru"); 
				$strLoc = "";
				if(strLen($ar["COUNTRY_NAME"])>0) 	$strLoc .= $ar["COUNTRY_NAME"].", ";
				if(strLen($ar["REGION_NAME"])>0) 	$strLoc .= $ar["REGION_NAME"].", ";
				if(strLen($ar["CITY_NAME"])>0) 		$strLoc .= $ar["CITY_NAME"];
				$arProp["VALUE"] = $strLoc;
			}
			$strOrderProps .= "<tr style='border-bottom:1px solid #CCC;'><td style='width:200px; padding:2px 20px; font-size:14px; vertical-align: top;'>".$arProp["NAME"]."</td><td style='padding:2px 40px; font-size:14px; vertical-align: top;'>".$arProp["VALUE"]."</td></tr>";
		}
		$strOrderProps .= "</table>";
		$arFields["ORDER_PROPS"] = $strOrderProps;
		
		
		// СОСТАВИМ strOrderList - вывод корзины товаров

		$strOrderList = "<table border='0' style='width:760px; border-collapse:collapse;' cellpadding='5'>";
		$strOrderList .= "<thead>\n<tr style='border-bottom:1px solid #CCCCCC;'>";
		$strOrderList .= "<td style='padding:10px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>№</b></td>";
		$strOrderList .= "<td style='padding:10px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Фото</b></td>";
		$strOrderList .= "<td style='padding:10px; text-align:left; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Наименование</b></td>";
		$strOrderList .= "<td style='padding:10px; width:100px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Цена</b></td>";
		$strOrderList .= "<td style='padding:10px; width:80px; background-color: #D12A2D; color: #FFFFFF; white-space: nowrap; font-size:14px;'><b>Кол-во, шт.</b></td>";
		$strOrderList .= "<td style='padding:10px; width:100px; background-color: #D12A2D; color: #FFFFFF; font-size:14px;'><b>Сумма</b></td>";
		$strOrderList .= "</tr>\n</thead><tbody>\n"; 
		
		$num = 0;
		foreach ($arBasketList as $arItem)
		{
			$arImg = getPreview_($arItem["PRODUCT_ID"],100,150);
			$num++;
			$measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOA_SHT");
			$strOrderList .= "<tr style='border-bottom:1px solid #CCCCCC;'>";
			$strOrderList .= "<td style='padding:10px; font-size:14px;'>".$num."</td>\n";
			$strOrderList .= "<td style='padding:10px; font-size:14px;'><img src='".$arImg["SRC"]."'></td>\n";
			$strOrderList .= "<td style='padding:10px; padding-right:30px; font-size:14px;' class='prod-name'>";
			$strOrderList .= "<b><a href='".$arExtData[$arItem["PRODUCT_ID"]]["LINK"]."'>".$arItem["NAME"]."</a></b>";
			if(strLen($arExtData[$arItem["PRODUCT_ID"]]["COLOR"]) > 0) $strOrderList .= "<br/><span class='prod' style='font-size:80%;'>Цвет: ".$arExtData[$arItem["PRODUCT_ID"]]["COLOR"]."</span>";
			if(strLen($arExtData[$arItem["PRODUCT_ID"]]["SIZE"]) > 0) $strOrderList .= "<br/><span class='prod' style='font-size:80%;'>Размер: ".$arExtData[$arItem["PRODUCT_ID"]]["SIZE"]."</span>";
			$strOrderList .= "</td>\n";
			$strOrderList .= "<td style='padding:10px; white-space: nowrap; font-size:14px;'>".$arItem["PRICE"]." руб.";
			if($arItem["DISCOUNT_VALUE"]>0) $strOrderList .= "<br/><span class='' style='color:red; font-size:90%;'>(с учетом скидки ".$arItem["DISCOUNT_VALUE"].")</span>";
			$strOrderList .= "</td>\n";
			$strOrderList .= "<td style='padding:10px; text-align:center; font-size:14px;'>".round($arItem["QUANTITY"])."</td>\n";
			$strOrderList .= "<td style='padding:10px; white-space: nowrap; font-size:14px;'><b>".$arItem["PRICE"]*$arItem["QUANTITY"]." руб.</b></td>\n";
			$strOrderList .= "</tr>\n";
		}
		$strOrderList .= "</tbody></table>\n";

		$arFields["ORDER_LIST"] = $strOrderList;
		$arFields["PAYSYSTEM_NAME"] = $arPaySys["NAME"];
		$arFields["DELIVERY_NAME"] = $arDelivery["NAME"];

		return $arFields;

	}
	else
		return "Неверный номер заказа";
	
}


?>

<?
prn(getOrderMail(294));
?> 

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>