<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

function getFileArray($id)
{
	$img = CFile::GetFileArray($id);
	$res = Array(
		"ID" => $id,
		"SRC" => $_SERVER["DOCUMENT_ROOT"].$img["SRC"],
		"URL" => "http://".$_SERVER["SERVER_NAME"].$img["SRC"],
		"SIZE" => $img["FILE_SIZE"],
		"TIME" => filemtime($_SERVER["DOCUMENT_ROOT"].$img["SRC"]),
		"DESCRIPTION" => $img["DESCRIPTION"],
		"MD5" => md5_file($_SERVER["DOCUMENT_ROOT"].$img["SRC"])
		);
	return $res;
}


if(!file_exists($_SERVER["DOCUMENT_ROOT"]."/temp/data.txt"))
{
	$num = 0;
	$arPict = Array();
	$dbRes = CIBlockElement::GetList(Array("CML2_ARTICLE" => "asc"), Array("IBLOCK_ID" => 4), false, false, Array("ID", "XML_ID", "ACTIVE","NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_MORE_PHOTO"));
	while($arRes = $dbRes->GetNext())
	{
		$id = $arRes["ID"];
		$arPict[$arRes["XML_ID"]] = Array(
			"PREVIEW_PICTURE" => "", 
			"DETAIL_PICTURE" => "",
			"MORE_PHOTO" => ""
		);
		if($arRes["PREVIEW_PICTURE"])
		{
			if(!is_array($arPict[$id]["PREVIEW_PICTURE"]))
			{
				$arPict[$arRes["XML_ID"]]["PREVIEW_PICTURE"] = getFileArray($arRes["PREVIEW_PICTURE"]);
			}
		}
		if($arRes["DETAIL_PICTURE"])
		{
			if(!is_array($arPict[$id]["DETAIL_PICTURE"]))
			{
				$arPict[$arRes["XML_ID"]]["DETAIL_PICTURE"] = getFileArray($arRes["DETAIL_PICTURE"]);
			}
		}
		if(is_array($arRes["PROPERTY_MORE_PHOTO_VALUE"]))
		{
			foreach($arRes["PROPERTY_MORE_PHOTO_VALUE"] as $val)
			{
				$arPict[$arRes["XML_ID"]]["MORE_PHOTO"][] = getFileArray($val);
			}
		}
		$num++;
		if($num>2500) break;
	} 
	
	ksort($arPict);
	prn(count($arPict));
	//prn($arPict);
	
	$str = serialize($arPict);
	$f = fopen($_SERVER["DOCUMENT_ROOT"]."/temp/data.txt", "w+");
	fwrite($f, $str);
	fclose($f);
	prn("write ok");
	
}


//die();

$f = file($_SERVER["DOCUMENT_ROOT"]."/temp/data.txt");
$arPict = Array();
$arPict = unserialize($f[0]);
prn("read ok - ".count($arPict)." elements");



$num = 0;
foreach($arPict as $k => $v)
{
	prn($v);
	$num++;
	if($num > 10) break;
}

?>
