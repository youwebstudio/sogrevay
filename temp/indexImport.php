<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");

// ==================================================================================
// ВНИМАНИЕ!
// В данном импорте действуют как минимум следующие ограничения:
// - валюта РУБЛИ
// - единицы измерений ШТУКИ
// - вложенность разделов каталога не больше трех (жестко три цикла, можно увеличить копипастом)
// - каждый товар может быть привязан к одному разделу (просто не знаю как выглядит когда к нескольким)
// ================================================================================== 

//$fileImport = $_SERVER["DOCUMENT_ROOT"]."/temp/import1/import.xml"; 
$fileOffers = $_SERVER["DOCUMENT_ROOT"]."/temp/import1/offers.xml";

$arXML = Array();
$arBD = Array();

$arList = Array();	// массив справочный XML_ID -> ID


if(file_exists($fileOffers))
{
	$xml = simplexml_load_file($fileOffers);
	
	$arPriceTypes = Array();
	
	// парсим типы цен
	foreach($xml->ПакетПредложений->ТипыЦен->ТипЦены as $arPriceType)
	{
		$arPriceTypes[trim($arPriceType->Ид)] = trim($arPriceType->Наименование);
	}
	
	// парсим торговые предложения
	$num = 0;
	foreach($xml->ПакетПредложений->Предложения->Предложение as $k=>$arItem)
	{
		if(intVal(trim($arItem->Количество)) > 0) 
		{
			$num++;
			
			$arPrices = Array();
			
			foreach($arItem->Цены->Цена as $arPrice)
			{
				$arPrices[trim($arPrice->ИдТипаЦены)] = trim($arPrice->ЦенаЗаЕдиницу);
			}
			
			$offerID = trim($arItem->Ид);
			$t = explode("#", $offerID);
			$goodID = $t[0];
			
			
			$arXML[$goodID]["OFFERS"][$offerID] = Array(
				"NAME" => trim($arItem->Наименование),
				"COUNT" => round(trim($arItem->Количество) == "" ? "0" : trim($arItem->Количество)),
				"PRICE" => $arPrices,
				"ACTIVE" => "Y"
				);
			$arXML[$goodID]["ACTIVE"] = "Y";
		}
	}
}

//prn($arXML);




// Данные
$PRODUCTS_IBLOCK_ID = 4;
$OFFERS_IBLOCK_ID = 5;
$PRICETYPE = "1875e475-1516-11e3-88a1-002618a8d26d";   // внешний код типа цены интернет магазин из настроек магазина

$num = 0;
$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $OFFERS_IBLOCK_ID, "ACTIVE"=>"Y"), false, false, Array("XML_ID", "ID", "NAME", "CATALOG_GROUP_1", "CATALOG_QUANTITY", "ACTIVE"));
while($arRes = $dbRes->Fetch())
{
	$num++;
	$t = explode("#", $arRes["XML_ID"]);
	$prID = $t[0];
	$arList[$arRes["XML_ID"]] = Array(
		"ID" => $arRes["ID"],
		"NAME" => $arRes["NAME"]
	);
	//$arList[$t[0]] = $arRes["PROPERTY_CML2_LINK_VALUE"];
	//prn($arRes);
	//prn($arRes["XML_ID"]." ".$arRes["CATALOG_QUANTITY"]);
	$arBD[$prID]["OFFERS"][$arRes["XML_ID"]] = Array(
		"NAME" => $arRes["NAME"],
		"COUNT" => $arRes["CATALOG_QUANTITY"],
		"PRICE" => Array($PRICETYPE => $arRes["CATALOG_PRICE_1"]),
		"ACTIVE" => $arRes["ACTIVE"]
		);
	
	//$list_offers[$arRes["XML_ID"]] = $arRes["ID"];
	//if($num > 1000) break;
}



// Сортируем Offers внутри каждого товара
foreach($arXML as $key => $arX)
{
	$arSort = Array();
	$arSort = $arX["OFFERS"];
	ksort($arSort);
	$arXML[$key]["OFFERS"] = $arSort;
}
foreach($arBD as $k => $arB)
{
	$arSort = Array();
	$arSort = $arB["OFFERS"];
	ksort($arSort);
	$arBD[$k]["OFFERS"] = $arSort;
}




// довыберем в массив arBD все товары и выставим им статус
$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PRODUCTS_IBLOCK_ID), false, false, Array("XML_ID", "ID", "NAME", "ACTIVE"));
while($arRes = $dbRes->Fetch())
{
	//$arList[$arRes["XML_ID"]] = $arRes["ID"];
	$arList[$arRes["XML_ID"]] = Array(
		"ID" => $arRes["ID"],
		"NAME" => $arRes["NAME"]
	);
	$arBD[$arRes["XML_ID"]]["ACTIVE"] = $arRes["ACTIVE"];
}




//prn($arXML);
//prn($arBD);
//prn($arList);


//$GOOD_XML_ID = "10f25e49-5d8f-11df-b967-002197d362b4";

//prn($arXML[$GOOD_XML_ID]);
//prn($arBD[$GOOD_XML_ID]);
//if($arXML[$GOOD_XML_ID] == $arBD[$GOOD_XML_ID]) echo "SYNC";

//prn($arList[$GOOD_XML_ID]);




// Сравниваем
$arAction = Array();
$arSync = Array();
$num = 0;
$numRight = 0;
echo "<table celpadding='10' border='1'>";

$fileaction = $_SERVER["DOCUMENT_ROOT"]."/temp/act.txt";
$f = fopen($fileaction, "w");

foreach($arBD as $xmlID => $arItem)
{
	
	
	if($arBD[$xmlID] == $arXML[$xmlID]) 
	{
		$arBD[$xmlID]["STATUS"] = "SYNC";
		$numRight++;
		$arSync["SYNC"]++;
	}
	elseif(!isset($arXML[$xmlID])) 
	{
		if($arItem["ACTIVE"] == "Y")
		{
			$arBD[$xmlID]["STATUS"] = "DEACT";
			$arSync["DEACT"]++;
		}
		else
		{
			$arBD[$xmlID]["STATUS"] = "NONEED";
			$arSync["NONEED"]++;	
		}
	}
	elseif(isset($arXML[$xmlID]))
	{
		$arBD[$xmlID]["STATUS"] = "UPD";
		$arSync["UPD"]++;
	}


	if(($arBD[$xmlID]["STATUS"] == "DEACT") || ($arBD[$xmlID]["STATUS"] == "UPD"))
	{
		fwrite($f, $xmlID."||".serialize($arBD[$xmlID])."\r\n");
	}


	
	
	
	
	
	echo "<tr><td colspan='2'>".$arList[$xmlID]["NAME"]." (".$xmlID.")";
	if($arBD[$xmlID]["STATUS"]) echo" - ".$arBD[$xmlID]['STATUS'];
	echo "</td></tr>";
	if($xmlID == "e7cf17ca-e8b5-11e4-a37e-002618a8d26d")
	{
		echo "<tr><td valign='top'>";
		//if($arBD[$xmlID]["STATUS"] != "SYNC") prn($arBD[$xmlID]);
		prn($arBD[$xmlID]);
		echo "</td><td valign='top'>";
		//if($arBD[$xmlID]["STATUS"] != "SYNC") prn($arXML[$xmlID]);
		prn($arXML[$xmlID]);
		echo "</td></tr>";	
	}
	$num++;
}

fclose($f);

echo "</table>";



echo "Нужно добавить<br/><table border='1' cellpadding='1'>";
foreach($arXML as $xmlID => $arItem)
{
	if(!isset($arBD[$xmlID])) 
	{
		$arSync["ADD"]++;
		echo "<tr><td>".$xmlID."</td></tr>";
	}
}
echo "</table>";




prn($arSync);
prn("Всего - ".$num);
prn("В XML - ".count($arXML));



?>

