<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?
CModule::IncludeModule('inmarketing.grastin');
$gr = new CGrastin(); 
$gr->getGrastinData();
//prn($gr->GRASTIN_DATA[arGrastinSelfPickup]);




$num = 0;

/*Формирование строки - всплывающих подсказок объектов на карте*/

foreach ($gr->GRASTIN_DATA[arBoxberrySelfPickup] as $ResultItem) {
    $str1 .= '{"type": "Feature", "id": '.$num.', "geometry": {
    	"type": "Point", 
    	"coordinates": ['.trim($ResultItem[latitude]).','.trim($ResultItem[longitude]).']},
    	"properties":{
    		"balloonContent":"<address><strong>Адрес: </strong>'.addslashes($ResultItem[Name]).'</address><div class=\"desc\">'.str_replace("Проезд:","<strong>Проезд: </strong>",str_replace("\n", "<br>",addslashes($ResultItem[drivingdescription]))).'</div><div class=\"schedule\"><strong>График работы: </strong>'.addslashes($ResultItem[schedule]).'</div>",';
    $str1 .='"clusterCaption": "'.addslashes($ResultItem[Name]).'", "hintContent": "'.addslashes($ResultItem[Name]).'"}},';
    $num++;
}
$str1 = substr($str1, 0, strLen($str1)-1);

foreach ($gr->GRASTIN_DATA[arGrastinSelfPickup] as $ResultItem) {
    $str2 .= '{"type": "Feature", "id": '.$num.', "geometry": {
    	"type": "Point", 
    	"coordinates": ['.trim($ResultItem[latitude]).','.trim($ResultItem[longitude]).']},
    	"properties":{
    		"balloonContent":"<address><strong>Адрес: </strong>'.addslashes($ResultItem[Name]).'</address><div class=\"desc\"><strong>Проезд: </strong>'.str_replace("\n", "<br>",addslashes($ResultItem[drivingdescription])).'</div><div class=\"schedule\"><strong>График работы: </strong>'.addslashes($ResultItem[timetable]).'</div>",';
    $str2 .='"clusterCaption": "'.addslashes($ResultItem[Name]).'", "hintContent": "'.addslashes($ResultItem[Name]).'"}},';
    $num++;
}
$str2 = substr($str2, 0, strLen($str2)-1);









?>

<!DOCTYPE html>
<html>
<head>
    <title>Примеры. Добавление на карту большого числа объектов</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Если вы используете API локально, то в URL ресурса необходимо указывать протокол в стандартном виде (http://...)-->
    <script src="//api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript"></script>
    <script src="//yandex.st/jquery/2.2.3/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    	ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map('map',
        { center: [55.76, 37.64], zoom: 3 },
        { searchControlProvider: 'yandex#search' }),
        objectManager1 = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 60
        }),
        objectManager2 = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32
        });
    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.

    objectManager1.objects.options.set( {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/images/icons/logo-boxberry.png',
			iconImageSize: [30, 30],
			iconImageOffset: [-15, -15]
    });
    objectManager1.clusters.options.set('clusterIconColor', '#ee1750');
	myMap.geoObjects.add(objectManager1);


	objectManager2.objects.options.set( {
            iconLayout: 'default#image',
            iconImageHref: '/bitrix/images/icons/grastin_logo.png',
			iconImageSize: [30, 30],
			iconImageOffset: [-15, -15]
    });
	objectManager2.clusters.options.set('clusterIconColor', '#292929');
    myMap.geoObjects.add(objectManager2);


 		points1 = {
            "type": "FeatureCollection",
            "features": [

			    <?echo $str1;?>
			]
        };
        myMap.geoObjects.add(objectManager1);
        objectManager1.add(points1);

        points2 = {
            "type": "FeatureCollection",
            "features": [

			    <?echo $str2;?>
			]
        };
        myMap.geoObjects.add(objectManager2);
        objectManager2.add(points2);



}
    </script>
    <style>
        html, body, #map {
            width: 100%; height: 100%; padding: 0; margin: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>
</body>
</html>