<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");

$arSectionList = Array();
$dbSect = CIBlockSection::GetTreeList(Array('IBLOCK_ID' => 4), Array("ID", "NAME", "SECTION_PAGE_URL"));
while($arSect = $dbSect->GetNext()) $arSectionList[$arSect["ID"]] = $arSect;
//prn($arSectionList);








function get_section_list($arFields)
{
	$PROPERTY_POL_ID = 139;					// ИД свойства ПОЛ
	$PROPERTY_POL_VAL_BOY = 2836;			// ИД значение свойства ПОЛ = МАЛЬЧИК
	$PROPERTY_POL_VAL_GIRL = 2971;			// ИД значение свойства ПОЛ = ДЕВОЧКА
	$PROPERTY_POL_VAL_DET = 4480; 			// ИД значение свойства ПОЛ = ДЕТСКИЙ
	
	$PROPERTY_RISUNOK_ID = 253;				// ИД свойства РИСУНОК
	$PROPERTY_RISUNOK_VAL_OLENI = 4471;		// ИД значения С ОЛЕНЯМИ
	
	$PROPERTY_SEZON_ID = 64;				// ИД свойства СЕЗОН
	$PROPERTY_SEZON_VAL_ZIMA = 24;			// ИД значения ОСЕНЬ-ЗИМА
	$PROPERTY_SEZON_VAL_LETO = 23;			// ИД значения ВЕСНА-ЛЕТО
	
	$PROPERTY_TIP_ID = 254;					// ИД свойства ТИП ИЗДЕЛИЯ
	$PROPERTY_TIP_VAL_PALTO = 4532;			// ИД значения ПАЛЬТО
	$PROPERTY_TIP_VAL_SHARF = 4533;			// ИД значения ШАРФ
	$PROPERTY_TIP_VAL_SNUD = 4534;			// ИД значения СНУД
	$PROPERTY_TIP_VAL_KARDIGAN = 4535;		// ИД значения КАРДИГАН
	$PROPERTY_TIP_VAL_SARAFAN = 4536;		// ИД значения САРАФАН
	
	// текущее значение свойства Пол
	if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_POL_ID]["n0"])) $PROP_POL_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_POL_ID]["n0"]["VALUE"]);
	else $PROP_POL_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_POL_ID][0]["VALUE"]);   

	// текущее значение свойства Тип изделия
	if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_TIP_ID]["n0"])) $PROP_TIP_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_TIP_ID]["n0"]["VALUE"]);
	else $PROP_TIP_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_TIP_ID][0]["VALUE"]);   

	// текущее значение свойства Сезон
	if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_SEZON_ID]["n0"])) $PROP_SEZON_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_SEZON_ID]["n0"]["VALUE"]);
	else $PROP_SEZON_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_SEZON_ID][0]["VALUE"]);   
	
	// текущее значение свойства Рисунок
	if(isset($arFields["PROPERTY_VALUES"][$PROPERTY_RISUNOK_ID]["n0"])) $PROP_RISUNOK_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_RISUNOK_ID]["n0"]["VALUE"]);
	else $PROP_RISUNOK_VALUE = intVal($arFields["PROPERTY_VALUES"][$PROPERTY_RISUNOK_ID][0]["VALUE"]);   

	
	// перекладываем основные разделы детского трикотажа
	
	$arReplace = Array(
		54 => Array("BOY" => 160, "GIRL" => 167), // VAY KIDS / Жилеты детские
		56 => Array("BOY" => 161, "GIRL" => 168), // VAY KIDS / Рейтузы детские
		81 => Array("BOY" => 162, "GIRL" => 169), // VAY KIDS / Жакеты детские
		82 => Array("BOY" => 163, "GIRL" => 170), // VAY KIDS / Платья
		83 => Array("BOY" => 178, "GIRL" => 171), // VAY KIDS / Джемперы детские
		84 => Array("BOY" => 179, "GIRL" => 172), // VAY KIDS / Вискоза детская
		59 => Array("BOY" => 180, "GIRL" => 173), // Веснушки / Платья детские
		60 => Array("BOY" => 181, "GIRL" => 174), // Веснушки / Рейтузы
		86 => Array("BOY" => 182, "GIRL" => 175), // Веснушки / Жакеты детские
		87 => Array("BOY" => 183, "GIRL" => 176), // Веснушки / Жилеты детские
		88 => Array("BOY" => 184, "GIRL" => 177), // Веснушки / Джемперы детские
		
		186 => Array("BOY" => 202, "GIRL" => 195), // Школьный трикотаж / Жилеты
		187 => Array("BOY" => 203, "GIRL" => 196), // Школьный трикотаж / Жакеты, кардиганы
		188 => Array("BOY" => 204, "GIRL" => 197), // Школьный трикотаж / Джемперы
		189 => Array("BOY" => '', "GIRL" => 198),  // Школьный трикотаж / Платья, сарафаны
		190 => Array("BOY" => 205, "GIRL" => 199), // Школьный трикотаж / Водолзаки, блузы
		191 => Array("BOY" => '', "GIRL" => 200)   // Школьный трикотаж / Юбки
		//192 => Array("BOY" => , "GIRL" => )  // Школьный трикотаж / Брюки
		//89 => Array("BOY" => 0, "GIRL" => 0)    // Веснушки / Вискоза детская
	);
	
	$arNewSect = Array();
	foreach($arFields["IBLOCK_SECTION"] as $sectID) 
	{
		if(isset($arReplace[$sectID]))
		{
			if($PROP_POL_VALUE == $PROPERTY_POL_VAL_BOY) 	$arNewSect[] = $arReplace[$sectID]["BOY"];
			if($PROP_POL_VALUE == $PROPERTY_POL_VAL_GIRL) 	$arNewSect[] = $arReplace[$sectID]["GIRL"];
			if($PROP_POL_VALUE == $PROPERTY_POL_VAL_DET) 	$arNewSect = array_values($arReplace[$sectID]);
		}
	}

	if(count($arNewSect) > 0)
	{
		$arFields["IBLOCK_SECTION"] = $arNewSect;
	}
		
	// обработка свойства с Оленями
	
	if($PROP_RISUNOK_VALUE == $PROPERTY_RISUNOK_VAL_OLENI)
	{
		$arReplaceOleni = Array(
			159 => Array(180, 181, 182, 183, 184, 160, 161, 162, 163, 178, 179),  		// для мальчиков
			166 => Array(167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177),		// для девочек
			228 => Array(49, 227),														// с оленями женские
			233 => Array(150, 152, 231, 232)											// с оленями мужские
			);
		$arNewSect = Array();
		foreach($arFields["IBLOCK_SECTION"] as $sectID)
		{
			foreach($arReplaceOleni as $newSectID => $ids)
			{
				if(in_array($sectID, $ids)) $arNewSect[] = $newSectID;
			}
		}
		if(count($arNewSect) > 0)
		{
			$arFields["IBLOCK_SECTION"] = $arNewSect;
		}
	}
	
	
	
	// обработка некоторых свойств

	$arNewSect = Array();
	foreach($arFields["IBLOCK_SECTION"] as $sectID)
	{
		// Жакеты женские распределяем на теплые и легкие
		if(in_array($sectID, Array(77, 222))) 
		{
			if($PROP_SEZON_VALUE == $PROPERTY_SEZON_VAL_LETO) $arNewSect[] = 224;
			if($PROP_SEZON_VALUE == $PROPERTY_SEZON_VAL_ZIMA) $arNewSect[] = 223;
			if($PROP_TIP_VALUE == $PROPERTY_TIP_VAL_KARDIGAN) $arNewSect = Array(225);		// кардиганы
			if($PROP_TIP_VALUE == $PROPERTY_TIP_VAL_PALTO) $arNewSect = Array(226);			// пальто
		}
		
		
		if($sectID == 216) $arNewSect = Array(216, 230);	// дублируем платья из швейных изделий в летние платья
		if($sectID == 217) $arNewSect = Array(217, 230);	// дублируем сарафаны из швейных изделий в летние платья
		
	}
	if(count($arNewSect) > 0)
	{
		$arFields["IBLOCK_SECTION"] = $arNewSect;
	}
	
}







$dbRes = CIBlockElement::GetList(
	Array(), 
	Array("IBLOCK_ID" => 4, "ACTIVE" => "Y"), 
	false, 
	false, 
	Array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_POL","PROPERTY_SEZON", "PROPERTY_TIP_IZDELIYA")
	);
while($arRes = $dbRes->GetNext())
{
	$arGoods[$arRes["ID"]] = Array(
		"NAME" => $arRes["NAME"],
		"IBLOCK_SECTION_ID" => $arRes["IBLOCK_SECTION_ID"],
		"PROPERTY_POL_VALUE" => $arRes["PROPERTY_POL_VALUE"],
		"PROPERTY_POL_ENUM_ID" => $arRes["PROPERTY_POL_ENUM_ID"],
		"PROPERTY_TIP_IZDELIYA_VALUE" => $arRes["PROPERTY_TIP_IZDELIYA_VALUE"],
		"PROPERTY_TIP_IZDELIYA_ENUM_ID" => $arRes["PROPERTY_TIP_IZDELIYA_ENUM_ID"],
		"PROPERTY_SEZON_VALUE" => $arRes["PROPERTY_SEZON_VALUE"],
		"PROPERTY_SEZON_ENUM_ID" => $arRes["PROPERTY_SEZON_ENUM_ID"]
		);
}
//$arGroupList = Array();
$dbGroups = CIBlockElement::GetElementGroups(array_keys($arGoods), true);
while($arGroups = $dbGroups->GetNext())
{
	//$arGroupList[$arGroups["IBLOCK_ELEMENT_ID"]][$arGroups["ID"]] = $arGroups["SECTION_PAGE_URL"];
	if(is_array($arGoods[$arGroups["IBLOCK_ELEMENT_ID"]])) $arGoods[$arGroups["IBLOCK_ELEMENT_ID"]]["IBLOCK_SECTION"][] = $arGroups["ID"];
}
//prn($arGoods);









foreach($arGoods as $id => $arGood)
{
	prn($arGood["NAME"]." | ".implode("/", $arGood["IBLOCK_SECTION"]));
	get_section_list($t);
	
	
}












?>