<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



// ============================================================================= 
// Входные данные 
// ============================================================================= 
$PRODUCTS_IBLOCK_ID = 4;															// ID инфоблока товаров
$OFFERS_IBLOCK_ID 	= 5;															// ID инфоблока предложений
$PRICETYPE 			= "1875e475-1516-11e3-88a1-002618a8d26d";   					// внешний код типа цены интернет магазин из настроек магазина
$fileOffers 		= $_SERVER["DOCUMENT_ROOT"]."/temp/importManual/offers.xml";	// адрес файла для импорта [offers.xml]
$fileAction 		= $_SERVER["DOCUMENT_ROOT"]."/temp/importManual/act.txt";		// адрес файла действий
$stepAction 		= 100;															// сколько действий делать за 1 шаг



// ============================================================================= 
// Подключение необходимых модулей
// ============================================================================= 
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");



// ============================================================================= 
// Несколько функций
// ============================================================================= 
function wFile($ar, $fname)
{
	$f = fopen($fname, "w");
	foreach($ar as $k=>$v) fwrite($f, $k."||".serialize($v)."\r\n");
	fclose($f);
}

function rFile($fname)
{
	$arr = Array();
	$f = file($fname);
	for($i=0; $i<count($f); $i++)
	{
		$t = explode("||", $f[$i]);
		$arr[$t[0]] = unserialize($t[1]);
	}
	return $arr;
}

function logger($str)
{
	echo date("d.m.Y H:i")." - ".$str."<br/>";
}






/*
$ID = 10484;
$ACTIVE = "Y";

$el = new CIBlockElement;
$resGood = $el->Update($ID, Array("ACTIVE" => $ACTIVE));
if($resGood) logger("PRODUCT ".$ID." - успешно обновлен");
else logger("PRODUCT ".$ID." - ОШИБКА обновления");
 
die();
*/


// ============================================================================= 
// Если нет, то создаем файл действий $fileAction 
// ============================================================================= 

if(!file_exists($fileAction) && file_exists($fileOffers))
{
	logger("Формируем файл действий");
	
	$arOffersXML = Array();
	$arGoodsXML = Array();
	$arOffersBD = Array();
	$arGoodsBD = Array();
	
	$arOffersList = Array();	// массив справочный XML_ID -> ID
	$arGoodsList = Array();		// массив справочный XML_ID -> ID
	
	
	// ====== формируем $arGoodsXML и $arOffersXML - парсим XML и берем только то, что нам нужно
	
	$xml = simplexml_load_file($fileOffers); 
	$arPriceTypes = Array();
	
	// парсим типы цен
	foreach($xml->ПакетПредложений->ТипыЦен->ТипЦены as $arPriceType)
	{
		$arPriceTypes[trim($arPriceType->Ид)] = trim($arPriceType->Наименование);
	}
	
	// парсим торговые предложения
	foreach($xml->ПакетПредложений->Предложения->Предложение as $k=>$arItem)
	{
		if(intVal(trim($arItem->Количество)) > 0) 
		{
			$arPrices = Array();
			foreach($arItem->Цены->Цена as $arPrice)
			{
				$arPrices[trim($arPrice->ИдТипаЦены)] = trim($arPrice->ЦенаЗаЕдиницу);
			}
			$offerID = trim($arItem->Ид);
			$t = explode("#", $offerID);
			$goodID = $t[0];
			$arOffersXML[$offerID] = Array(
				"NAME" => trim($arItem->Наименование),
				"COUNT" => round(trim($arItem->Количество) == "" ? "0" : trim($arItem->Количество)),
				"PRICE" => $arPrices,
				"ACTIVE" => "Y"
				);
			$arGoodsXML[$goodID] = "Y";
		}
	}


	
	// ======= формируем $arGoodsBD и $arOffersBD - читаем из базы данных
	// ======= и заполняем справочные массивы $arOffersList и $arGoodsList (соответствие XML_ID->ID)
	
	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $OFFERS_IBLOCK_ID/*, "ACTIVE"=>"Y"*/), false, false, Array("XML_ID", "ID", "NAME", "CATALOG_GROUP_1", "CATALOG_QUANTITY", "ACTIVE"));
	while($arRes = $dbRes->Fetch())
	{
		$num++;
		$arOffersList[$arRes["XML_ID"]] = Array(
			"ID" => $arRes["ID"],
			"NAME" => $arRes["NAME"]
		);
		$arOffersBD[$arRes["XML_ID"]] = Array(
			"NAME" => $arRes["NAME"],
			"COUNT" => $arRes["CATALOG_QUANTITY"],
			"PRICE" => Array($PRICETYPE => $arRes["CATALOG_PRICE_1"]),
			"ACTIVE" => $arRes["ACTIVE"]
			);
	}

	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PRODUCTS_IBLOCK_ID/*, "ACTIVE"=>"Y"*/), false, false, Array("XML_ID", "ID", "NAME", "ACTIVE"));
	while($arRes = $dbRes->Fetch())
	{
		$arGoodsBD[$arRes["XML_ID"]] = $arRes["ACTIVE"];
		$arGoodsList[$arRes["XML_ID"]] = Array(
			"ID" => $arRes["ID"],
			"NAME" => $arRes["NAME"]
		);
	}



	// ====== сравниваем, пишем в файл действий

	$arGoodsAction = Array();
	$arOffersAction = Array();
	$arAction = Array();
	//
	
	// сравним товары
	
	//$xml = "73ccfb81-ea71-11e5-afab-d067e5f9e4b1";
	//prn($arGoodsBD[$xml]);
	//prn($arGoodsXML[$xml]);

	foreach($arGoodsBD as $xmlID=>$arItem)
	{
		if($arGoodsBD[$xmlID] == $arGoodsXML[$xmlID]) 					continue;								// если одинаковы, ничего делать не надо
		if(($arGoodsBD[$xmlID] == "Y")&&(!isset($arGoodsXML[$xmlID]))) 	$arGoodsAction[$xmlID] = "DEACT";		// активен на сайте, но нет в выгрузке --- деактивировать (DEACT)
		if(($arGoodsBD[$xmlID] == "N")&&($arGoodsXML[$xmlID] == "Y")) 	$arGoodsAction[$xmlID] = "ACT";			// НЕактивен на сайте, но есть в выгрузке ----- активировать (ACT)
	}
	foreach($arGoodsXML as $xmlID=>$arItem)
	{
		if(($arGoodsXML[$xmlID] == "Y")&&(!isset($arGoodsBD[$xmlID]))) $arGoodsAction[$xmlID] = "ADD";			// есть в выгрузке, но нет на сайте ----- добавить (ADD)
	}

	// закинем в общий массив действий
	foreach($arGoodsAction as $k => $v)
		if(($v == "ACT") || ($v == "DEACT")) $arAction[$arGoodsList[$k]["ID"]] = $v;

	// сравним торговые предложения
	
	//$xml = "b6d2fd0d-0cdb-11df-800a-002197d362b4#e1023781-21e1-11df-bbc9-0021856e10e1";
	//prn($arOffersBD[$xml]);
	//prn($arOffersXML[$xml]);
	/*
	prn($arOffersBD[$xml]["NAME"]." - ".strLen($arOffersBD[$xml]["NAME"]));
	prn($arOffersXML[$xml]["NAME"]." - ".strLen($arOffersXML[$xml]["NAME"])); 
	if(trim($arOffersBD[$xml]["NAME"]) == trim($arOffersXML[$xml]["NAME"])) prn("NAME SYNC"); else prn("NAME NOT SYNC");
	if($arOffersBD[$xml]["COUNT"] == $arOffersXML[$xml]["COUNT"]) prn("COUNT SYNC"); else prn("COUNT NOT SYNC");
	if($arOffersBD[$xml]["PRICE"] == $arOffersXML[$xml]["PRICE"]) prn("PRICE SYNC"); else prn("PRICE NOT SYNC");
	if($arOffersBD[$xml]["ACTIVE"] == $arOffersXML[$xml]["ACTIVE"]) prn("ACTIVE SYNC"); else prn("ACTIVE NOT SYNC");
	if($arOffersBD[$xml] == $arOffersXML[$xml]) prn("SYNC"); else prn("NOT SYNC");
	*/

	foreach($arOffersBD as $xmlID=>$arItem) 
	{
		if($arOffersBD[$xmlID] == $arOffersXML[$xmlID]) continue; 													// если одинаковы, ничего делать не надо
		if(($arOffersBD[$xmlID]["ACTIVE"] == "Y") && ($arOffersXML[$xmlID]["ACTIVE"] == "Y"))						// есть и там и там но не равны ---- UPDATE
		{
			$arOffersAction[$xmlID] = $arOffersXML[$xmlID];
			$arOffersAction[$xmlID]["STATUS"] = "UPD";
		}
		if(($arOffersBD[$xmlID]["ACTIVE"] == "N") && ($arOffersXML[$xmlID]["ACTIVE"] == "Y"))						// есть и там и там но не равны ---- UPDATE
		{
			$arOffersAction[$xmlID] = $arOffersXML[$xmlID];
			$arOffersAction[$xmlID]["STATUS"] = "UPD";
		}
		if( !isset($arOffersXML[$xmlID]) || ($arOffersXML[$xmlID]["ACTIVE"] == "N"))
		{
			if($arOffersBD[$xmlID]["ACTIVE"] == "Y")
			{
				$arOffersAction[$xmlID] = $arOffersBD[$xmlID];
				$arOffersAction[$xmlID]["ACTIVE"] = "N";	
				$arOffersAction[$xmlID]["STATUS"] = "DEACT";
			}	
		}
	}
	foreach($arOffersXML as $xmlID=>$arItem)
	{
		if(!isset($arOffersBD[$xmlID])) 
		{
			$arOffersAction[$xmlID] = $arOffersXML[$xmlID];
			$arOffersAction[$xmlID]["STATUS"] = "ADD";
		}
	}

	// запишем в общий массив действий
	//prn(count($arOffersGoodsAction));
	foreach($arOffersAction as $k => $v)
		if(($v["STATUS"] == "UPD") || ($v["STATUS"] == "DEACT")) $arAction[$arOffersList[$k]["ID"]] = $v;;
	
	
	// массив пишем в файл
	wFile($arAction, $fileAction);
}




die();

// ============================================================================= 
// Выполняем действия по файлу действий
// ============================================================================= 

if(file_exists($fileAction))
{
	logger("Выполняем заданные обновления");
	$arAction = rFile($fileAction);	

	if(count($arAction) > 0)
	{
		//prn($arAction);
		$num = 0;
		foreach($arAction as $id => $arItem)
		{
			if(is_array($arItem)) 
			{
				// обновление offer
				$resPrice = CPrice::SetBasePrice($id, $arItem["PRICE"][$PRICETYPE], "RUB");
				$resCount = CCatalogProduct::Update($id, Array("QUANTITY" => $arItem["COUNT"]));
				$el = new CIBlockElement;
				$resActive = $el->Update($id, Array("ACTIVE" => $arItem["ACTIVE"], "NAME" => $arItem["NAME"]));
				if($resPrice && $resCount && $resActive) logger("OFFER ".$id." - успешно обновлено");
				else logger("OFFER ".$id." - ОШИБКА обновления");
			}
			else 
			{
				// обновление product
				$el = new CIBlockElement;
				if($arItem == "ACT") $cmd = "Y";
				if($arItem == "DEACT") $cmd = "N";
				$resGood = $el->Update($id, Array("ACTIVE" => $cmd));
				if($resGood) logger("PRODUCT ".$id." - успешно обновлен");
				else logger("PRODUCT ".$id." - ОШИБКА обновления");
			}
			unset($arAction[$id]);
			
			$num++;
			if($num>=$stepAction) break;
		}
		logger("Осталось еще ".count($arAction)." элементов");
		wFile($arAction, $fileAction);
	}

	if(count($arAction) < 1)
	{
		//if(unlink($fileOffers)) logger("Файл выгрузки удален [".$fileOffers."]"); else logger("Не могу удалить файл ".$fileOffers);
		if(unlink($fileAction)) logger("Файл действий удален [".$fileAction."]"); else logger("Не могу удалить файл ".$fileAction);
		logger("Синхронизация завершена");
	}
	
}
else
{
	logger("Недостаточно файлов для импорта");
}





?>

