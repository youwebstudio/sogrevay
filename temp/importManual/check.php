<?
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



// ============================================================================= 
// Входные данные
// ============================================================================= 
$PRODUCTS_IBLOCK_ID = 4;															// ID инфоблока товаров
$OFFERS_IBLOCK_ID 	= 5;															// ID инфоблока предложений
$PRICETYPE 			= "1875e475-1516-11e3-88a1-002618a8d26d";   					// внешний код типа цены интернет магазин из настроек магазина
$fileOffers 		= $_SERVER["DOCUMENT_ROOT"]."/temp/importManual/offers.xml";	// адрес файла для импорта [offers.xml]
$fileAction 		= $_SERVER["DOCUMENT_ROOT"]."/temp/importManual/act.txt";		// адрес файла действий
$stepAction 		= 1;															// сколько действий делать за 1 шаг



// ============================================================================= 
// Подключение необходимых модулей
// ============================================================================= 
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");



// ============================================================================= 
// Несколько функций
// ============================================================================= 
function wFile($ar, $fname)
{
	$f = fopen($fname, "w");
	foreach($ar as $k=>$v) fwrite($f, $k."||".serialize($v)."\r\n");
	fclose($f);
}

function rFile($fname)
{
	$arr = Array();
	$f = file($fname);
	for($i=0; $i<count($f); $i++)
	{
		$t = explode("||", $f[$i]);
		$arr[$t[0]] = unserialize($t[1]);
	}
	return $arr;
}

function logger($str)
{
	echo date("d.m.Y H:i")." - ".$str."<br/>";
}




// ============================================================================= 
// 
// ============================================================================= 

if(file_exists($fileOffers))
{
	//logger("Формируем файл действий");
	
	$arOffersXML = Array();
	$arGoodsXML = Array();
	//$arOffersBD = Array();
	//$arGoodsBD = Array();
	
	//$arOffersList = Array();	// массив справочный XML_ID -> ID
	//$arGoodsList = Array();		// массив справочный XML_ID -> ID
	
	
	// ====== формируем $arGoodsXML и $arOffersXML - парсим XML и берем только то, что нам нужно
	
	$xml = simplexml_load_file($fileOffers); 
	$arPriceTypes = Array();
	
	// парсим типы цен
	foreach($xml->ПакетПредложений->ТипыЦен->ТипЦены as $arPriceType)
	{
		$arPriceTypes[trim($arPriceType->Ид)] = trim($arPriceType->Наименование);
	}
	
	// парсим торговые предложения
	foreach($xml->ПакетПредложений->Предложения->Предложение as $k=>$arItem)
	{
		if(intVal(trim($arItem->Количество)) > 0) 
		{
			$arPrices = Array();
			foreach($arItem->Цены->Цена as $arPrice)
			{
				$arPrices[trim($arPrice->ИдТипаЦены)] = trim($arPrice->ЦенаЗаЕдиницу);
			}
			$offerID = trim($arItem->Ид);
			$t = explode("#", $offerID);
			$goodID = $t[0];
			$arOffersXML[$offerID] = Array(
				"NAME" => trim($arItem->Наименование),
				"COUNT" => round(trim($arItem->Количество) == "" ? "0" : trim($arItem->Количество)),
				"PRICE" => $arPrices,
				"ACTIVE" => "Y"
				);
			$arGoodsXML[$goodID] = "Y";
		}
	}


	
	// ======= формируем $arGoodsBD и $arOffersBD - читаем из базы данных
	// ======= и заполняем справочные массивы $arOffersList и $arGoodsList (соответствие XML_ID->ID)
	
	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $OFFERS_IBLOCK_ID/*, "ACTIVE"=>"Y"*/), false, false, Array("XML_ID", "ID", "NAME", "CATALOG_GROUP_1", "CATALOG_QUANTITY", "ACTIVE"));
	while($arRes = $dbRes->Fetch())
	{
		//prn($arRes);
		//prn($arOffersXML[$arRes["XML_ID"]]);
		if(
			($arRes["NAME"] == $arOffersXML[$arRes["XML_ID"]])&&
			(round($arRes["COUNT"]) == round($arOffersXML[$arRes["CATALOG_QUANTITY"]]))&&
			(round($arRes["PRICE"]) == round($arOffersXML[$arRes["CATALOG_PRICE_1"]]))
			)
		continue;
		
		if($arRes["ACTIVE"]["N"] && !isset($arOffersXML[$arRes["XML_ID"]])) continue;
		
		echo $arRes["XML_ID"]."<br/>";
		/*
		$num++;
		$arOffersList[$arRes["XML_ID"]] = Array(
			"ID" => $arRes["ID"],
			"NAME" => $arRes["NAME"]
		);
		$arOffersBD[$arRes["XML_ID"]] = Array(
			"NAME" => $arRes["NAME"],
			"COUNT" => $arRes["CATALOG_QUANTITY"],
			"PRICE" => Array($PRICETYPE => $arRes["CATALOG_PRICE_1"]),
			"ACTIVE" => $arRes["ACTIVE"]
			);
		*/
		//break;
	}

	$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $PRODUCTS_IBLOCK_ID/*, "ACTIVE"=>"Y"*/), false, false, Array("XML_ID", "ID", "NAME", "ACTIVE"));
	while($arRes = $dbRes->Fetch())
	{
		$arGoodsBD[$arRes["XML_ID"]] = $arRes["ACTIVE"];
		$arGoodsList[$arRes["XML_ID"]] = Array(
			"ID" => $arRes["ID"],
			"NAME" => $arRes["NAME"]
		);
	}
}
else
{
	logger("Недостаточно файлов для импорта");
}





?>

