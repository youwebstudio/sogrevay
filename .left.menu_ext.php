<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

require($_SERVER["DOCUMENT_ROOT"]."/.bottom.menu.php");
$bottom_menu = Array(
	"Условия",
    "",
    Array(""),
    Array(
        "FROM_IBLOCK" => "",
        "IS_PARENT" => 1,
		"DEPTH_LEVEL" => 1
        )
	);
	
foreach($aMenuLinks as &$arItem)
{
	$arItem[3] = Array(
        "FROM_IBLOCK" => "",
        "IS_PARENT" => "",
		"DEPTH_LEVEL" => 2
		);
}

$bottom_menu = array_merge(Array($bottom_menu), $aMenuLinks);

$aMenuLinks = Array();

global $APPLICATION;
$aMenuLinksExt = array();

if(CModule::IncludeModule('iblock'))
{
	$arFilter = array(
		"TYPE" => "1c_catalog",
		"SITE_ID" => SITE_ID,
	);

	$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
	$dbIBlock = new CIBlockResult($dbIBlock);

	if ($arIBlock = $dbIBlock->GetNext())
	{
		if(defined("BX_COMP_MANAGED_CACHE"))
			$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_".$arIBlock["ID"]);

		if($arIBlock["ACTIVE"] == "Y")
		{
			$aMenuLinksExt = $APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
				"IS_SEF" => "Y",
				"SEF_BASE_URL" => "",
				"SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
				"DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
				"IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
				"IBLOCK_ID" => $arIBlock['ID'],
				"DEPTH_LEVEL" => "5",
				"CACHE_TYPE" => "N",
			), false, Array('HIDE_ICONS' => 'Y'));
		}
		//pra($aMenuLinksExt);
	}

	if(defined("BX_COMP_MANAGED_CACHE"))
		$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
}






$arSections = Array();
$dbRes = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 4, "ACTIVE" => "Y"), false, false, Array("ID", "NAME", "IBLOCK_SECTION_ID"));
while($arRes = $dbRes->GetNext())
{
	$arSections[$arRes["IBLOCK_SECTION_ID"]]++;
}
//pra($arSections);


$dbRes = CIBlockSection::GetList(Array(), Array("ID" => array_keys($arSections)), false, Array("ID", "NAME", "SECTION_PAGE_URL"));
while($arRes = $dbRes->GetNext())
{
	$arSections[$arRes["ID"]] = $arRes["SECTION_PAGE_URL"];
}



$arSections = array_flip($arSections);

$ar = $bottom_menu;


foreach($aMenuLinksExt as $k1=>$arItem1)
{
	if($arItem1[3]["IS_PARENT"] > 0) continue;
	if(!isset($arSections[$arItem1[1]])) 
	{
		//pra($arItem1[1]);
		unset($aMenuLinksExt[$k1]);	
	}
}


//pra($bottom_menu);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);

/*
if($USER->isAdmin())
{
	// Добавляем FAMILY-LOOK
	$aMenuLinks[] = Array(
		"FAMILY LOOK",
		"/family-look/",
		Array(
			"FROM_IBLOCK" => "",
			"IS_PARENT" => "",
			"DEPTH_LEVEL" => 1
			)
		);
}
*/
//pra($aMenuLinks);

$aMenuLinks = array_merge($aMenuLinks, $bottom_menu);


// Добавим Распродажу перед Мужским
//if($USER->isAdmin())
//{
	$newArr = Array();
	foreach($aMenuLinks as $k => $arItem)
	{
		if($arItem[0] == "Мужской трикотаж") 
		{
			$newArr[] = Array(
				"Распродажа",
				"/sale/",
				"",
				Array(
					"FROM_IBLOCK" => "",
					"IS_PARENT" => "",
					"DEPTH_LEVEL" => 2
				)
			);
		}
		$newArr[] = $arItem;
	}
	$aMenuLinks = $newArr;
//}
//pra($aMenuLinksExt);




//pra($aMenuLinks);


?>