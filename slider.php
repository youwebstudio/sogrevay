<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новая страница");
?>

<?

// выясним в каких заказах нужно изменять статус и отправлять уведомление ($arToUpdate)
// те которые не отменены и в истории изменений есть ORDER_1C_IMPORT и нынешний статус N
$arOrdersInfo = Array();
$arIDs = Array();
$dbOrders = CSaleOrder::GetList(Array("ID" => "asc"), Array("STATUS_ID" => "N"));
while($arOrder = $dbOrders->Fetch())
{
	$arIDs[] = $arOrder["ID"];
	$arOrdersInfo[$arOrder["ID"]]["DATE_INSERT_FORMAT"] = $arOrder["DATE_INSERT_FORMAT"];
}


if(count($arIDs) > 0)
{
	$arToUpdate = Array();
	$dbChanges = CSaleOrderChange::GetList(Array("ORDER_ID" => "ASC"), Array("ORDER_ID" => $arIDs));
	while ($arChange = $dbChanges->Fetch())
	{
		if($arChange["TYPE"] == "ORDER_1C_IMPORT") $arToUpdate[$arChange["ORDER_ID"]] = 1;
	}

	if(count($arToUpdate) > 0)
	{
		// Найдем общие данные для почтового шаблона
		$sale_email = COption::GetOptionString("sale", "order_email", "");
		$server_name = COption::GetOptionString("main", "server_name", "");
		$site_name = COption::GetOptionString("main", "site_name", "");
		$st = CSaleStatus::GetByID("C");
		$order_status = $st["NAME"];
		$order_desc = $st["DESCRIPTION"];

		// Находим еще недостающие данные для шаблона (EMAIL) из свойств заказа, меняем статус и отправляем уведомление
		foreach($arToUpdate as $ID => $val)
		{
			$arEventFields = Array();
			
			$db_props = CSaleOrderPropsValue::GetOrderProps($ID);
			while ($arProps = $db_props->Fetch())
			{
				if($arProps["CODE"] == "EMAIL") 
				{
					$email = $arProps["VALUE"];	
					break;
				}
			}
			
			$arEventFields = array( 
				"SALE_EMAIL" => $sale_email,
				"EMAIL" => $email,
				"SERVER_NAME" => $server_name,
				"ORDER_ID" => $ID,
				"SITE_NAME" => $site_name,
				"ORDER_DATE" => $arOrdersInfo[$ID]["DATE_INSERT_FORMAT"],
				"ORDER_STATUS" => $order_status,
				"ORDER_DESCRIPTION" => $order_desc,
				"TEXT" => ""
			);
			
			$arFields = Array("STATUS_ID" => "C");
			
			$status_res = CSaleOrder::Update($ID, $arFields);
			$send_res = CEvent::Send("SALE_STATUS_CHANGED_C", "s1", $arEventFields);
			
			echo $ID." updated<br/>";
			
		}
	}

}

?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>