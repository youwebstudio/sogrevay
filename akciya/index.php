<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Товары по акции");
?>

<?
$arParams["PAGE_ELEMENT_COUNT"] = 20;								// default
$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_DAY_THING";			// default
$arParams["ELEMENT_SORT_ORDER"] = "DESC";							// default
$arParams["ELEMENT_SORT_FIELD2"] = "PROPERTY_SMART_SORT";			// default
$arParams["ELEMENT_SORT_ORDER2"] = "DESC";								// default

if($_GET["sort"] == "price_asc") 		{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_MIN_PRICE"; 		$arParams["ELEMENT_SORT_ORDER"] = "ASC";}
if($_GET["sort"] == "price_desc") 		{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_MIN_PRICE"; 		$arParams["ELEMENT_SORT_ORDER"] = "DESC";}
if($_GET["sort"] == "artikul_asc") 		{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_CML2_ARTICLE"; 	$arParams["ELEMENT_SORT_ORDER"] = "ASC";}
if($_GET["sort"] == "artikul_desc") 	{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_CML2_ARTICLE"; 	$arParams["ELEMENT_SORT_ORDER"] = "DESC";}
if($_GET["count"] == 40) 				{$arParams["PAGE_ELEMENT_COUNT"] = 40;}
if($_GET["count"] == 60) 				{$arParams["PAGE_ELEMENT_COUNT"] = 60;}
?>


<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<div class="div-sort">
			<span class="sort-title">Сортировать:</span>
			<span class="sort-var">по цене</span>
			<a title="По возрастанию цены"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_MIN_PRICE")&&($arParams["ELEMENT_SORT_ORDER"] == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
			<a title="По убыванию цены"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_MIN_PRICE")&&($arParams["ELEMENT_SORT_ORDER"] == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a>
			<span class="sort-var">по артикулу</span>
			<a title="По возрастанию артикула"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_CML2_ARTICLE")&&($arParams["ELEMENT_SORT_ORDER"] == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
			<a title="По убыванию артикула"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_CML2_ARTICLE")&&($arParams["ELEMENT_SORT_ORDER"] == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a> 
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="div-count">
			<span class="count-title">Показывать:</span>
			<a <?if($arParams["PAGE_ELEMENT_COUNT"]==20):?>class="active" <?endif?> class="<?=$sortedByCount20?>" href="<?=$APPLICATION->GetCurPageParam("", array("count"))?>">20</a>
			<a <?if($arParams["PAGE_ELEMENT_COUNT"]==40):?>class="active" <?endif?> href="<?=$APPLICATION->GetCurPageParam("count=40", array("count"))?>">40</a>
			<a <?if($arParams["PAGE_ELEMENT_COUNT"]==60):?>class="active" <?endif?> href="<?=$APPLICATION->GetCurPageParam("count=60", array("count"))?>">60</a>
		</div>
	</div>
</div>




<?
$GLOBALS["arrFilterAkciya"] = Array("PROPERTY_AKCIYA_ON_SITE" => "Y");
?>



<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"board1", 
	//"",
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/korzina/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "600",
		"CACHE_TYPE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER"],
		"FILTER_NAME" => "arrFilterAkciya",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "1c_catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "NOVIZNA",
		"LINE_ELEMENT_COUNT" => "4",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "SIZE",
			1 => "COLOR",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "COLOR",
			2 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR",
			1 => "SIZE",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "vay",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "интернет-магазин",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "MORE_PHOTO",
			2 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "black",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "board1"
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>