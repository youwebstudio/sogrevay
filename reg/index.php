<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация нового пользователя");
?>


<?/*?>
<p style="text-align:center; padding: 10px 20px; margin: 15px 0 30px 0; background: #f9f98c; color: red; font-weight: bold; border-top: 2px solid red; border-bottom: 2px solid red;">В субботу и воскресенье 5-6 августа 2017 года на сервере проводятся технические работы. Корзина и оформление заказа временно отключены.<br>Приносим свои извинения за доставленные неудобства.</p>
<?*/?>


<?
$APPLICATION->IncludeComponent(
	"bitrix:main.register", 
	"rozn", 
	array(
		"COMPONENT_TEMPLATE" => "rozn",
		"SHOW_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "LAST_NAME",
			3 => "PERSONAL_BIRTHDAY",
			4 => "PERSONAL_PHONE",
		),
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "PERSONAL_PHONE",
		),
		"AUTH" => "Y",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "",
		"SET_TITLE" => "Y",
		"USER_PROPERTY" => array(
			0 => "UF_SUBSCRIBE",
		),
		"USER_PROPERTY_NAME" => ""
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>