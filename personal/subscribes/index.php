<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Управление рассылкой");
?>

<?if($USER->isAuthorized()):?>
	<ul class="tabs">
		<li><a href="/personal/">Личные данные</a></li>
		<li><a href="/personal/order/">Заказы</a></li>
		<li><a href="/personal/korzina/">Корзина</a></li>
		<li class="active"><a href="/personal/subscribes/">Подписки</a></li>
	</ul>
<?endif?>

<?$APPLICATION->IncludeComponent(
	"bitrix:subscribe.edit", 
	"rozn", 
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALLOW_ANONYMOUS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"SET_TITLE" => "Y",
		"SHOW_AUTH_LINKS" => "Y",
		"SHOW_HIDDEN" => "N",
		"COMPONENT_TEMPLATE" => "rozn"
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>