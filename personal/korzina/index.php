<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>


<?/*?>
<p style="text-align:center; padding: 10px 20px; margin: 15px 0 30px 0; background: #f9f98c; color: red; font-weight: bold; border-top: 2px solid red; border-bottom: 2px solid red;">В субботу и воскресенье 5-6 августа 2017 года на сервере проводятся технические работы. Корзина и оформление заказа временно отключены.<br>Приносим свои извинения за доставленные неудобства.</p>
<?*/?>


<?if($USER->isAuthorized()):?>
<ul class="tabs">
	<li><a href="/personal/">Личные данные</a></li>
	<li><a href="/personal/order/">Заказы</a></li>
	<li class="active"><a href="/personal/korzina/">Корзина</a></li>
	<li><a href="/personal/subscribes/">Подписки</a></li>
</ul>
<?endif?>


<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "rozn", Array(
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",	// Рассчитывать скидку для каждой позиции (на все количество товара)
		"COLUMNS_LIST" => array(	// Выводимые колонки
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "PRICE",
			3 => "QUANTITY",
			4 => "SUM",
			5 => "PROPS",
			6 => "DELETE",
			7 => "DELAY",
		),
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
		"HIDE_COUPON" => "N",	// Спрятать поле ввода купона
		"QUANTITY_FLOAT" => "N",	// Использовать дробное значение количества
		"PRICE_VAT_SHOW_VALUE" => "Y",	// Отображать значение НДС
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"AJAX_OPTION_ADDITIONAL" => "",
		"OFFERS_PROPS" => array(	// Свойства, влияющие на пересчет корзины
			0 => "SIZES_SHOES",
			1 => "SIZES_CLOTHES",
			2 => "COLOR_REF",
		)
	),
	false
);?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>