<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>

<?
function checkProfileData()
{
	$myErr = "";
	if(strLen(strVal($_REQUEST["NAME"])) < 1) $myErr .= 'Поле "Имя" обязательно для заполнения<br>'; 
	if(strLen(strVal($_REQUEST["EMAIL"])) < 1) $myErr .= 'Поле "E-Mail" обязательно для заполнения<br>';
	if(strLen(strVal($_REQUEST["PERSONAL_BIRTHDAY"])) < 1) $myErr .= 'Поле "Дата рождения" обязательно для заполнения<br>'; 
	if(!check_email(strVal($_REQUEST["EMAIL"]))) $myErr .= 'Неправильный E-Mail<br>'; 
	if( ( (strLen(strVal($_REQUEST["NEW_PASSWORD"])) > 0) || (strLen(strVal($_REQUEST["NEW_PASSWORD_CONFIRM"])) > 0) ) &&
		(strVal($_REQUEST["NEW_PASSWORD"]) != strVal($_REQUEST["NEW_PASSWORD_CONFIRM"])) 
		)
		$myErr .= 'Не совпадают поля "Пароль" и "Подтверждение пароля"<br>';	
	return $myErr;
}
?>

<?if(isset($_REQUEST["save"])):?>
	<?$GLOBALS["myErr"] = checkProfileData();?>
	<?if(strLen($GLOBALS["myErr"]) > 0):?>
		<div class="alert alert-danger" style="width:50%"><?=$GLOBALS["myErr"]?></div>
		<?
		unset($_POST["save"]);
		unset($_REQUEST["save"]);
		?>
	<?endif?>
<?endif?>



<?$APPLICATION->IncludeComponent(
	"bitrix:main.profile", 
	"rozn", 
	array(
		"SET_TITLE" => "Y",
		"COMPONENT_TEMPLATE" => "rozn",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "undefined",
		"USER_PROPERTY" => array(
			0 => "UF_SUBSCRIBE",
		),
		"SEND_INFO" => "N",
		"CHECK_RIGHTS" => "N",
		"USER_PROPERTY_NAME" => ""
	),
	false
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>