<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты компании | SOGREVAY.RU");
$APPLICATION->SetPageProperty("keywords", "контакты адреса телефоны компании sogrevay.ru");
$APPLICATION->SetPageProperty("description", "Контакты интернет магазина одежды от производителя. Качественный российский трикотаж недорого.");
$APPLICATION->SetTitle("Контакты");
?><div class="row">
	<div class="col-xs-12" itemscope="" itemtype="http://schema.org/Organization">
		<p>
 <span class="strong" itemprop="name">ООО «КУПИТРИКОТАЖ»</span> <br>
 <br>
 <span class="strong">Телефоны:</span><br>
			 - <span itemprop="telephone">8 (495) 502-90-99</span> (для Москвы)<br>
			 - <span itemprop="telephone">8 (800) 770-05-33</span> (для регионов бесплатно)<br>
			 - <span itemprop="telephone">8 (905) 544-24-14</span>&nbsp;
		</p>
		<p>
 <b>Онлайн-заказы принимаются круглосуточно. Менеджеры на связи ПН-ПТ 09:00-18:00.&nbsp;</b><br>
		</p>
		<p>
 <b>Электронная почта:&nbsp;</b><a href="mailto:info@sogrevay.ru" style="background-color: #ffffff;"><span style="color: #005824;">info@sogrevay.ru</span></a>
		</p>
		<p>
 <span class="strong"><br>
 </span>
		</p>
		<p>
 <span class="strong">Адрес офиса:</span> <span itemprop="address">г. Москва, ул. Уткина, д. 48/8 (вход с ул. Гаражная, проходная завода "Электропривод")</span>
		</p>
		<p style="padding: 4px 30px; background: #D12A2D; color: white; display: inline-block; font-size: 12px;">
			 Самовывоза по данному адресу нет. &nbsp; Выберите удобный для Вас пункт при оформлении заказа.
		</p>
		 &nbsp; &nbsp;<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4489.8467047714885!2d37.74250628742566!3d55.75983367285838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b53552c7b10dcf%3A0x267b3805d76e6186!2z0YPQuy4g0KPRgtC60LjQvdCwLCA0OCwg0JzQvtGB0LrQstCwLCAxMDUyNzU!5e0!3m2!1sru!2sru!4v1462287653345" width="600" height="450" frameborder="0" style="border:0; display:block;" allowfullscreen></iframe>
		<p>
			 &nbsp;
		</p>
		<table border="2" width="53%">
		<tbody>
		<tr>
			<td style=" padding: 4px;">
				Наименование организации (полное)
			</td>
			<td style=" padding: 4px;">
				Общество с ограниченной ответственностью «КУПИТРИКОТАЖ»
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Наименование организации (краткое)
			</td>
			<td style=" padding: 4px;">
				ООО «КУПИТРИКОТАЖ»
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Адрес местонахождения (в соотв. с учред. докум.)
			</td>
			<td style=" padding: 4px;">
				105275, г.Москва, ул.Уткина, д.48
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Почтовый адрес (фактический)
			</td>
			<td style=" padding: 4px;">
				105275, г.Москва, ул.Уткина, д.48
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Идентификационный номер (ИНН)
			</td>
			<td style=" padding: 4px;">
				7720314878
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Код причины постановки на учет (КПП)
			</td>
			<td style=" padding: 4px;">
				772001001
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Номер расчетного счета
			</td>
			<td style=" padding: 4px;">
				40702810138000055267
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Полное наименование учреждения банка клиента
			</td>
			<td style=" padding: 4px;">
				ПАО «Сбербанк России» г. Москва
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				 БИК
			</td>
			<td style=" padding: 4px;">
				 044525225
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Корреспондентский счет
			</td>
			<td style=" padding: 4px;">
				30101810400000000225
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				ОГРН
			</td>
			<td style=" padding: 4px;">
				 1157746829900
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Код отрасли по ОКВЭД
			</td>
			<td style=" padding: 4px;">
				52.42
			</td>
		</tr>
		<tr>
			<td style=" padding: 4px;">
				Код отрасли по ОКПО
			</td>
			<td style=" padding: 4px;">
				45859837
			</td>
		</tr>
		</tbody>
		</table>
		<p>
			 &nbsp;
		</p>
		<h2>Задать вопрос</h2>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback",
	"eshop_adapt",
	Array(
		"COMPONENT_TEMPLATE" => "eshop_adapt",
		"EMAIL_TO" => "info@sogrevay.ru",
		"EVENT_MESSAGE_ID" => array(),
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"REQUIRED_FIELDS" => array(),
		"USE_CAPTCHA" => "Y"
	)
);?>
	</div>
</div>
<p>
	 &nbsp;
</p>
<p>
	 &nbsp;
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>