<div class="row">
	<div class="col-md-12" style="text-align: center;">
		<h1 style="color:#999; font-size: 30px; margin-bottom: 15px;">ИНТЕРНЕТ-МАГАЗИН РОССИЙСКОГО ТРИКОТАЖА</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div style="width:70px; height:50px; border:1px solid gray; margin:0 auto; margin-bottom:15px;"></div>
		<ul style="margin-left:20px;">
			<li>Огромный выбор моделей и расцветок от российского производителя</li>
			<li>Фабрика в Кимрах, Трико Меланж</li>
			<li>Всегда качественно и недорого</li>
		</ul>
	</div>
	<div class="col-md-4">
		<div style="width:70px; height:50px; border:1px solid gray; margin:0 auto; margin-bottom:15px;"></div>
		<ul style="margin-left:20px;">
			<li>Предлагаем сотрудничество для юридических лиц и физ.лиц.</li>
			<li>Переход на оптовый сайт</li>
		</ul>
	</div>
	<div class="col-md-4">
		<div style="width:70px; height:50px; border:1px solid gray; margin:0 auto; margin-bottom:15px;"></div>
		<ul style="margin-left:20px;">
			<li>Всегда стремимися быть ближе к покупателю</li>
			<li>Как купить, как обменять, как оплатить, как доставить</li>
			<li>Звоните по любому вопросу</li>
		</ul>
	</div>
</div>