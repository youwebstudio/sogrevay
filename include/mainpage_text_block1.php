<div class="row">
	<div class="col-md-4" style="padding:10px 50px; margin-bottom:30px;">
		<p style="font-size:18px; font-weight:bold;">Доставка по России</p>
		<p>Мы доставим товар по Москве, России, возможен самовывоз</p>
	</div>
	<div class="col-md-4" style="padding:10px 50px; margin-bottom:30px;">
		<p style="font-size:18px; font-weight:bold;">Различные способы оплаты</p>
		<p>Наличные, наложный платеж, VISA, MasterCard, QIWI и др.</p>
	</div>
	<div class="col-md-4" style="padding:10px 50px; margin-bottom:30px;">
		<p style="font-size:18px; font-weight:bold;">Возврат в течение 14 дней</p>
		<p>Все гарантийные обязательства в соответствии с Законодательством РФ</p>
	</div>
</div>
