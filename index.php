<?
//die("sogrevay");
//echo "<pre>";
//print_r($_SERVER);
//echo "</pre>";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "Интернет-магазин SOGREVAY.RU предлагает большой выбор одежды из трикотажа: женский, мужской и детский трикотаж. Цены. Доставка по Москве и России. Тел. 8 (800) 770-05-33");
$APPLICATION->SetPageProperty("description", "Интернет-магазин SOGREVAY.RU предлагает большой выбор одежды из трикотажа: женский, мужской и детский трикотаж. Цены. Доставка по Москве и России. Тел. 8 (800) 770-05-33");
$APPLICATION->SetPageProperty("title", "Интернет-магазин российского трикотажа | SOGREVAY.RU");
$APPLICATION->SetTitle("SOGREVAY.ru - интернет-магазин российского трикотажа");

// correct Opengraph (module dev2fun)
if($_SERVER["HTTPS"] == "on") $pro = "https"; else $pro = "http";
$url = COption::GetOptionString("main", "server_name", "www.sogrevay.ru");
$APPLICATION->SetPageProperty('og:image', $pro."://".$url."/upload/logo_red.png");
$APPLICATION->SetPageProperty('og:description', "Интернет-магазин SOGREVAY.RU предлагает большой выбор одежды из трикотажа: женский, мужской и детский трикотаж. Цены. Доставка по Москве и России. Тел. 8 (800) 770-05-33");
?>
<div class="cat-slider-block">
	<div class="container">
		<div class="row">
			<p class="cattitle"><a href="/catalog/">Каталог</a></h2>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

				<div class="row">
					<div class="col-lg-12">
						<div class="row links-3">
							<div class="col-lg-12">
								<div class="row cat-list">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><h2><a href="/catalog/zhenskiy-trikotazh/">Женский трикотаж</a></h2></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><h2><a href="/catalog/muzhskoy-trikotazh/">Мужской трикотаж</a></h2></div>
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><h2><a href="/catalog/detskiy-trikotazh/">Детский трикотаж</a></h2></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12" style="padding: 15px 0 10px 0;">
						<img style="border: 1px solid rgba(255, 255, 255, 0.43)" src="/upload/postbonus1.jpg"/>
					</div>
				</div>
				

				
				
				<div class="row">
					<div class="col-lg-12">
						<div class="catalog-slider-block">
							<?
							$APPLICATION->IncludeComponent(
								"bitrix:news.list", 
								"slider-catalog", 
								array(
									"IBLOCK_TYPE" => "1c_catalog",
									"IBLOCK_ID" => "4",
									"NEWS_COUNT" => "10",
									"SORT_BY1" => "PROPERTY_SHOW_IN_TRENDS",
									"SORT_ORDER1" => "ASC",
									"SORT_BY2" => "SORT",
									"SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "arrFilterTrends",
									"FIELD_CODE" => array(
										0 => "NAME",
										1 => "DETAIL_PICTURE",
										2 => "",
									),
									"PROPERTY_CODE" => array(
										0 => "BRAND_REF",
										1 => "SHOW_IN_TRENDS",
										2 => "BREND",
										3 => "LINK",
										4 => "",
									),
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_SHADOW" => "Y",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"PREVIEW_TRUNCATE_LEN" => "120",
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"DISPLAY_PANEL" => "N",
									"SET_TITLE" => "N",
									"SET_STATUS_404" => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"ADD_SECTIONS_CHAIN" => "N",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => "",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
									"PAGER_SHOW_ALL" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"COMPONENT_TEMPLATE" => "slider-catalog",
									"SET_BROWSER_TITLE" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_LAST_MODIFIED" => "N",
									"INCLUDE_SUBSECTIONS" => "N",
									"DISPLAY_DATE" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "N",
									"MEDIA_PROPERTY" => "",
									"SEARCH_PAGE" => "/search/",
									"USE_RATING" => "N",
									"USE_SHARE" => "N",
									"PAGER_TITLE" => "Новости",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"SHOW_404" => "N",
									"MESSAGE_404" => ""
								),
								false
							);
							?>
						</div>	
					</div>
				</div>	
				
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/main_prefooter.php",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
				
			</div>
		</div>
		
		<div class="row last-reviews">
			<div class="col-xs-12">
			<?/*$APPLICATION->IncludeComponent(
				"bitrix:blog.new_comments", 
				"for-main-page", 
				array(
					"BLOG_URL" => "",
					"BLOG_VAR" => "",
					"CACHE_TIME" => "86400",
					"CACHE_TYPE" => "A",
					"COMMENT_COUNT" => "20",
					"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
					"GROUP_ID" => "1",
					"MESSAGE_LENGTH" => "200",
					"PAGE_VAR" => "",
					"PATH_TO_BLOG" => "",
					"PATH_TO_GROUP_BLOG_POST" => "",
					"PATH_TO_POST" => "",
					"PATH_TO_SMILE" => "",
					"PATH_TO_USER" => "",
					"POST_VAR" => "",
					"SEO_USER" => "N",
					"USER_VAR" => "",
					"COMPONENT_TEMPLATE" => "for-main-page",
					"COUNT_WITHOUT_ADMINS" => "5"
				),
				false
			);*/?>

	

				<?
				$arrFilterComments = Array("PROPERTY_SHOW_ON_MAINPAGE" => 4815);
				?>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"comments_on_mainpage", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "DETAIL_TEXT",
			1 => "DATE_CREATE",
			2 => "",
		),
		"FILTER_NAME" => "arrFilterComments",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "COMMENT_ID",
			1 => "URL",
			2 => "AUTHOR",
			3 => "SHOW_ON_MAINPAGE",
			4 => "PRODUCT",
			5 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "comments_on_mainpage",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>


			</div>
		</div>
		
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>