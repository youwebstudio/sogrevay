<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("op");
?>

<?
$arViewed = array();
$basketUserId = (int)CSaleBasket::GetBasketUserID(false);
if ($basketUserId > 0){
   $viewedIterator = \Bitrix\Catalog\CatalogViewedProductTable::getList(array(
      'select' => array('PRODUCT_ID', 'ELEMENT_ID'),
      'filter' => array('=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID),
      'order' => array('DATE_VISIT' => 'DESC'),
      'limit' => 10
   ));

   while ($arFields = $viewedIterator->fetch()){
      $arViewed[] = $arFields['ELEMENT_ID'];
   }
}

//prn($arViewed);
$GLOBALS["arrFilter"] = Array("ID" => $arViewed);
$GLOBALS["arrFilter"] = Array(
	"IBLOCK_ID"=> 4, 
	"ACTIVE" => "Y", 
	Array(
		"PROPERTY_AKCIYA_ON_SITE" => "Y",
		"ID" => 12022,
		"LOGIC" => "OR"
		)
	);
//prn($GLOBALS["arrFilter"]);
?>




<?$APPLICATION->IncludeComponent(
	"inmark:vaysection", 
	".default", 
	array(
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "10",
		"CACHE_TYPE" => "Y",
		"ELEMENT_SORT_TYPE" => "smartSort",
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "4",
		"IBLOCK_OFFERS_ID" => "5",
		"PAGE_ELEMENT_COUNT" => $_REQUEST["count"],
		"SECTION_ID" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>