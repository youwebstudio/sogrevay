<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Товары по акции");
?>



<?
/*
$arFields["ID"] = 2199;
$DISCOUNT_ID = 9;
CModule::IncludeModule("catalog");
$arDiscount = CCatalogDiscount::GetByID($DISCOUNT_ID);
//prn($arDiscount);
//die();
if($arDiscount["ACTIVE"] == "Y")
{
	$COUPON = CatalogGenerateCoupon()."-".$arFields["ID"];
	$arCouponFields = array(
		"DISCOUNT_ID" => $DISCOUNT_ID,
		"ACTIVE" => "Y",
		"ONE_TIME" => "O",       // на один заказ
		"COUPON" => $COUPON
	);
	$CID = new CCatalogDiscountCoupon();
	if(!$CID->Add($arCouponFields)) $COUPON = "";
}
*/
?>




<?/*?>
<script>
$(document).ready(function() {
	$.fancybox.open({
					'type'		: 'iframe',
					'href'		: '/reg_popup/',
					'padding' 	: 0,
					'maxWidth'	: 400,
					'maxHeight'	: 450,
					'tpl'		: {'wrap' : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin" style="margin:0 !important; border-radius:10px !important;"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'},
				});
});
</script>
<a class="fancybox-iframe-reg-popup" href="/reg_popup/">Зарегистрироваться</a>
<?*/?>

<?
$arParams["PAGE_ELEMENT_COUNT"] = 20;								// default
$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_DAY_THING";			// default
$arParams["ELEMENT_SORT_ORDER"] = "DESC";							// default
$arParams["ELEMENT_SORT_FIELD2"] = "PROPERTY_SMART_SORT";			// default
$arParams["ELEMENT_SORT_ORDER2"] = "DESC";								// default

if($_GET["sort"] == "price_asc") 		{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_MIN_PRICE"; 		$arParams["ELEMENT_SORT_ORDER"] = "ASC";}
if($_GET["sort"] == "price_desc") 		{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_MIN_PRICE"; 		$arParams["ELEMENT_SORT_ORDER"] = "DESC";}
if($_GET["sort"] == "artikul_asc") 		{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_CML2_ARTICLE"; 	$arParams["ELEMENT_SORT_ORDER"] = "ASC";}
if($_GET["sort"] == "artikul_desc") 	{$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_CML2_ARTICLE"; 	$arParams["ELEMENT_SORT_ORDER"] = "DESC";}
if($_GET["count"] == 40) 				{$arParams["PAGE_ELEMENT_COUNT"] = 40;}
if($_GET["count"] == 60) 				{$arParams["PAGE_ELEMENT_COUNT"] = 60;} 
?>


<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<div class="div-sort">
			<span class="sort-title">Сортировать:</span>
			<span class="sort-var">по цене</span>
			<a title="По возрастанию цены"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_MIN_PRICE")&&($arParams["ELEMENT_SORT_ORDER"] == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
			<a title="По убыванию цены"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_MIN_PRICE")&&($arParams["ELEMENT_SORT_ORDER"] == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a>
			<span class="sort-var">по артикулу</span>
			<a title="По возрастанию артикула"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_CML2_ARTICLE")&&($arParams["ELEMENT_SORT_ORDER"] == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
			<a title="По убыванию артикула"<?if(($arParams["ELEMENT_SORT_FIELD"]=="PROPERTY_CML2_ARTICLE")&&($arParams["ELEMENT_SORT_ORDER"] == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a> 
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="div-count">
			<span class="count-title">Показывать:</span>
			<a <?if($arParams["PAGE_ELEMENT_COUNT"]==20):?>class="active" <?endif?> class="<?=$sortedByCount20?>" href="<?=$APPLICATION->GetCurPageParam("", array("count"))?>">20</a>
			<a <?if($arParams["PAGE_ELEMENT_COUNT"]==40):?>class="active" <?endif?> href="<?=$APPLICATION->GetCurPageParam("count=40", array("count"))?>">40</a>
			<a <?if($arParams["PAGE_ELEMENT_COUNT"]==60):?>class="active" <?endif?> href="<?=$APPLICATION->GetCurPageParam("count=60", array("count"))?>">60</a>
		</div>
	</div>
</div>




<?
/*$GLOBALS["arrFilterAkciya"] = Array(
	"IBLOCK_ID"=> 4, 
	"ACTIVE" => "Y", 
	Array(
		"PROPERTY_AKCIYA_ON_SITE" => "Y",
		"ID" => 12022,
		"LOGIC" => "OR"
		)
	);
	*/
?>






<?
$arUserGroups = $USER->GetUserGroupArray();
?>


<?
$arGoods = Array();
$arPictures = Array();
$arFilter = Array(
	"IBLOCK_ID"=> 4, 
	"ACTIVE" => "Y", 
	Array(
		"PROPERTY_AKCIYA_ON_SITE" => "Y",
		"ID" => 12022,
		"LOGIC" => "OR"
		)
	);
$arSelect = Array(
	"ID",
	"NAME",
	"PROPERTY_MIN_PRICE",
	"DETAIL_PAGE_URL",
	"DETAIL_PICTURE",
	"PROPERTY_MORE_PHOTO",
	"PROPERTY_MORE_PHOTO_DESCRIPTION"
);
$dbRes = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$dbRes->NavStart($arParams["PAGE_ELEMENT_COUNT"]);
echo $dbRes->NavPrint();
while($arRes = $dbRes->NavNext())
{
	$arGoods[$arRes["ID"]] = $arRes;
	if($arRes["DETAIL_PICTURE"] > 0) $arPictures[] = $arRes["DETAIL_PICTURE"];
	$arPictures = array_merge($arPictures, $arRes["PROPERTY_MORE_PHOTO_VALUE"]);
	
	$arGoods[$arRes["ID"]]["DISCOUNTS"] = CCatalogDiscount::GetDiscountByProduct(
        $arRes["ID"],
        $arUserGroups,
        "N",
        1,
        SITE_ID
    );
	
	//prn($arGoods[$arRes["ID"]]);
	//prn(count($arGoods[$arRes["ID"]]["DISCOUNTS"])); 
}



$arDiscounts = CCatalogDiscount::GetDiscountByProduct(
        11591,
        $USER->GetUserGroupArray(),
        "N",
        1,
        SITE_ID
    );
$discountPrice = CCatalogProduct::CountPriceWithDiscount(
			1500,
            "RUB",
            $arDiscounts
        );
//prn($arDiscounts);
//prn($discountPrice);
//die();





//die();

$arPicts = Array();
$dbFileRes = CFile::GetList(Array(), array("@ID" => implode(",", $arPictures)));
while($arFileRes = $dbFileRes->GetNext())
{
	$arPicts[$arFileRes["ID"]] = $arFileRes;
	$t = explode(" цвет ", $arFileRes["DESCRIPTION"]);
	if(count($t) > 1)
		$color = trim($t[1]);
	else 
		$color = trim($t[0]);
	$arPicts[$arFileRes["ID"]]["COLOR"] = trim(strtoupper($color));
}









$arOffers = Array();
$arFilterOffers = Array(
	"IBLOCK_ID" => 5, 
	"ACTIVE" => "Y", 
	"PROPERTY_CML2_LINK" => array_keys($arGoods)
	);
$arSelectOffers = Array(
	"ID",
	"IBLOCK_ID",
	"NAME",
	"ACTIVE",
	"CATALOG_GROUP_1",
	"PROPERTY_CML2_LINK",
	"PROPERTY_COLOR",
	"PROPERTY_SIZE"
	);
$dbOfferRes = CIBlockElement::GetList(Array(), $arFilterOffers, false, false, $arSelectOffers);
while($arOfferRes = $dbOfferRes->GetNext())
{
	$arOffer = Array();
	$arOffer = Array(
		"ID" => $arOfferRes["ID"],
		"NAME" => $arOfferRes["NAME"],
		"ACTIVE" => $arOfferRes["ACTIVE"],
		"QUANTITY" => $arOfferRes["CATALOG_QUANTITY"],
		"PRICE" => $arOfferRes["CATALOG_PRICE_1"],
		"CURRENCY" => $arOfferRes["CATALOG_CURRENCY_1"],
		"SIZE" => $arOfferRes["PROPERTY_SIZE_VALUE"],
		"COLOR" => $arOfferRes["PROPERTY_COLOR_VALUE"]
		);
	$offer_color = trim(strtoupper($arOfferRes["PROPERTY_COLOR_VALUE"]));
	$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["NAME"] = $arOfferRes["PROPERTY_COLOR_VALUE"];
	if(empty($arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]][$offer_color]["PICTURE"]))
	{
		$found = false;
		for($i = 0; $i < count($arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"]); $i++)
		{
			if($offer_color == $arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["COLOR"])
			{
				$WIDTH = 200;
				$HEIGHT = 300;
				$file = CFile::ResizeImageGet($arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["ID"], array('width' => $WIDTH, 'height' => $HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["PICTURE"] = Array(
					"ID" => $arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["ID"],
					//"SRC" => "/upload/".$arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["SUBDIR"]."/".$arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["FILE_NAME"],
					"SRC" => $file["src"],
					"DESCRIPTION" => $arPicts[$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["PROPERTY_MORE_PHOTO_VALUE"][$i]]["DESCRIPTION"]
					);
				$found = true;
				break;
			}
		}
		if(!$found)
			$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["PICTURE"] = Array("SRC" => "default");
	}
	
	//$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]][strtoupper($arOfferRes["PROPERTY_COLOR_VALUE"])]["PICTURE"] = Array();
	$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["SIZES"][$arOfferRes["PROPERTY_SIZE_VALUE"]][$arOfferRes["ID"]] = $arOffer;
	
	if(count($arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["DISCOUNTS"]) > 0)
	{
		$discountPrice = CCatalogProduct::CountPriceWithDiscount(
			$arOffer["PRICE"],
            "RUB",
            $arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["DISCOUNTS"]
		);	
		$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["SIZES"][$arOfferRes["PROPERTY_SIZE_VALUE"]][$arOfferRes["ID"]]["DISCOUNT_PRICE"] =  $discountPrice;
		$arGoods[$arOfferRes["PROPERTY_CML2_LINK_VALUE"]]["COLORS"][$offer_color]["SIZES"][$arOfferRes["PROPERTY_SIZE_VALUE"]][$arOfferRes["ID"]]["DISCOUNT_PROC"] =  "";
		//prn($discountPrice);
	}
	
	

}




?>
<style>
.good-item {
	/*border:1px solid #EEE; */
	text-align:center; 
	width:250px;
	height:440px; 
	overflow: hidden;
	margin: 5px auto;
	}

.good-item .good-title {
	height: 25px;
    /*background: yellow;*/
    white-space: nowrap;
    padding: 3px 5px;
	overflow: hidden;
	}
	
.good-item .good-offers	{
	/*background:gray;*/
}

.good-item .good-offers	.img-block {
	/*background:#EEE;*/
	height: 310px;
}

.offer-price {
	height: 25px;
    /*background: yellow;*/
    white-space: nowrap;
    padding: 3px 5px;
	overflow: hidden;
}

.good-item .good-offers .color-name {
	height: 25px;
    /*background: orange;*/
    white-space: nowrap;
    padding: 3px 5px;
	overflow: hidden;
	}


.good-offer-item.slick-slide {
	display: none;
	}
.good-offer-item.slick-slide.active {display:block;}

.good-offer-item img {margin:0 auto;}

.color-count {
    padding: 0px 5px;
    background: white;
    color: black;
    position: relative;
    display: block;
    position: absolute;
    left: 12%;
    top: 5px;
}

.slick-slide {outline:none;}
.slick-prev:before, .slick-next:before {color:black !important;}
.slick-prev, .slick-next {z-index:100 !important;}
.slick-prev {left:25px !important;}
.slick-next {right:25px !important;}
.slick-slider {margin-bottom: 0px !important;}


ul.size-list {
	margin:5px 0 !important; 
	height: 45px;
	padding:0 !important;
	}
ul.size-list li {    display: inline-block;
    margin: 0;
    padding: 2px 4px;
    border: 1px solid #e4e4e4;
    font-size: 11px;}

</style>

<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick-theme.css"/>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/slick/slick.min.js"></script>

<script>
$(document).ready( function() {
	
	$("div.good-offers").slick({
		/*lazyLoad: 'ondemand',*/
		fade: false,
		arrows: true
		});
	
});
</script>


<?



//pra(array_keys($arGoods));
prn(count($arGoods));
//$GOOD_ID = 11255;
?>


<?$num = 0;?>
<div class="row">
<?foreach($arGoods as $GOOD_ID => $arGood):?>
	<?$num++;?>
	<?//prn($num %4);?>
	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
		<div class="good-item">
			<div class="good-title">
				<a title="<?=$arGoods[$GOOD_ID]["NAME"]?>" href="<?=$arGoods[$GOOD_ID]["DETAIL_PAGE_URL"]?>"><?=$arGoods[$GOOD_ID]["NAME"]?></a>
			</div>
			<div class="good-offers">
				
				
				<?/*foreach($arGoods[$GOOD_ID]["COLORS"] as $arColor):?> 
				<div class="good-offer-item<?if($this_active):?> active<?endif?>">
					<?if($arColor["PICTURE"]["SRC"] != "default"):?>
					<img style="width:200px; height:300px; margin:0 auto;" src="<?=$arColor["PICTURE"]["SRC"]?>">
					<?else:?>
					<img style="width:200px; height:300px; margin:0 auto;" src="/upload/no_female_big.gif">
					<?endif?>
					<div class="color-name"><?=$arColor["NAME"]?></div>
					<ul class="size-list">
					<?foreach($arColor["SIZES"] as $arSize):?> 
						<?foreach($arSize as $arOffer):?> 
							<li><?=$arOffer["SIZE"]?></li>
						<?endforeach?>
					<?endforeach?>
					</ul>
				</div>
				<?endforeach*/?>
			
				<?$color_num = 0;?>
				<?$active = false;?>
				<?foreach($arGoods[$GOOD_ID]["COLORS"] as $arColor):?> 
				<?
					$this_active = false;
					$color_num++;
					if(!$active && $arColor["PICTURE"]["SRC"] != "default") 
					{
						$active = true;
						$this_active = true;
					}
					if(!$active && ($color_num == count($arGoods[$GOOD_ID]["COLORS"])))
					{
						$this_active = true;
					}
				?>
				<div class="good-offer-item<?if($this_active):?> active<?endif?>">
					<div class="img-block">
						<div class="color-count"><?=$color_num?>/<?=count($arGoods[$GOOD_ID]["COLORS"])?></div>
						<?if($arColor["PICTURE"]["SRC"] != "default"):?>
							<img style="width:200px; height:300px;" src="<?=$arColor["PICTURE"]["SRC"]?>">
						<?else:?>
							<img style="width:200px; height:300px;" src="/upload/no_female_big.gif">
						<?endif?>
					</div>
					<?foreach($arColor["SIZES"] as $arSize):?> 
						<?foreach($arSize as $arItem):?> 
							<div class="offer-price">
								<?if(isset($arItem["DISCOUNT_PRICE"])):?>
									<s><?=$arItem["PRICE"]?></s> <?=$arItem["DISCOUNT_PRICE"]?> руб.
								<?else:?>
									<?=$arItem["PRICE"]?> руб.
								<?endif?>
							</div>
							<?break;?>
						<?endforeach?>	
						<?break;?>
					<?endforeach?>
					<div class="color-name"><?=$arColor["NAME"]?></div>
					<ul class="size-list">
					<?foreach($arColor["SIZES"] as $arSize):?> 
						<?foreach($arSize as $arOffer):?> 
							<li class="<?=$arOffer["ID"]?>"><?=$arOffer["SIZE"]?></li>
						<?endforeach?>
					<?endforeach?>
					</ul>
				</div>
				<?endforeach?>
				
				
				
			</div>
		</div>
	</div>
<?endforeach?>
</div>
	<?/*
		<h3><?=$arColor["NAME"]?></h3>
		<?if($arColor["PICTURE"]["SRC"] == "default"):?>
			NONE
		<?else:?>
			<img style="display:block; margin:0 0 15px 0; width:50px;" src="<?=$arColor["PICTURE"]["SRC"]?>">
		<?endif?>
		<p>Размеры:</p>
		<ul>
		<?foreach($arColor["SIZES"] as $arSize):?> 
			<?foreach($arSize as $arOffer):?> 
				<li>
					<?=$arOffer["SIZE"]?> - <?=$arOffer["QUANTITY"]?> шт. - <?=$arOffer["PRICE"]?> руб.
				</li>
			<?endforeach?>
		<?endforeach?>
		</ul>
	<?endforeach*/?>




<?
pra("ok");










?>




<?/*$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"boardtest", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/korzina/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "600",
		"CACHE_TYPE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER"],
		"FILTER_NAME" => "arrFilterAkciya",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "1c_catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "NOVIZNA",
		"LINE_ELEMENT_COUNT" => "4",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "SIZE",
			1 => "COLOR",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "COLOR",
			2 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR",
			1 => "SIZE",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "vay",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "интернет-магазин",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "MORE_PHOTO",
			2 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "black",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "boardtest"
	),
	false
);*/?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>