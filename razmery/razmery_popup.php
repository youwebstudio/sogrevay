<html>
<head>
	<style>
	@font-face {
		font-family: HeliosBold;
		src: url(/bitrix/templates/eshop_rozn_new/fonts/HeliosCondBold.ttf);
   }
   
	body {
		font-family: 'Open Sans', sans-serif;
		font-size: 14px;
		line-height: 1.42857143;
		color: #333;
		}
	h2, h1 {
		font-family: 'HeliosBold', sans-serif;
		font-size: 24px;
		margin:10px;
		}
	h3 {
		font-family: 'HeliosBold', sans-serif;
		font-size: 18px;
		margin:5px 10px;
		}
	
	table {
		border-collapse: collapse;
		width:98%;
		background: #EEE;
	}
	
	table tr td {
		padding:2px 10px;
		text-align: center;
		border-bottom: 1px solid #CCC;
		}
		
	table tr th {
		padding:2px 10px;
		text-align: center;
		background: #D12A2D;
		color: #FFF;
		}
		
	.bord {
		border-right: 1px solid #CCC;
		}
	
	
	</style>
</head>
 <body>

 
  <h2>Размеры женские</h2>
 
	<p style="padding:5px;">Снимите мерки. Все мерки снимаются в положении стоя. Измерительная лента должна плотно прилегать к коже, но не давить.</p>
	<ul style="margin-left:20px;">
		<li><b>Обхват груди</b> - мерка снимается по выступающим точкам груди спереди и по нижнему выступающему краю лопаток сзади.</li>
		<li><b>Обхват талии</b> - по самой узкой части туловища.</li>
		<li><b>Обхват бёдер</b> - по наиболее выступающим сбоку и сзади точкам.</li>
	</ul>
	<br/>
 
    <table> 		 
      <tbody> 
        <tr><th>Размер</th><th>Обхват груди (см)</th><th>Обхват талии (см)</th><th>Обхват бёдер (см)</th></tr>
       		 		 
        <tr><td>42</td><td>84</td><td>66</td><td>92</td></tr>
       		 
        <tr><td>44</td><td>88</td><td>70</td><td>96</td></tr>
       		 
        <tr><td>46</td><td>92</td><td>74</td><td>100</td></tr>
       		 
        <tr><td>48</td><td>96</td><td>78</td><td>104</td></tr>
       		 
        <tr><td>50</td><td>100</td><td>82</td><td>108</td></tr>
       		 
        <tr><td>52</td><td>104</td><td>86</td><td>112</td></tr>
       		 
        <tr><td>54</td><td>108</td><td>90</td><td>116</td></tr>
       		 
        <tr><td>56</td><td>112</td><td>94</td><td>120</td></tr>
       		 
        <tr><td>58</td><td>116</td><td>98</td><td>124</td></tr>
       		 
        <tr><td>60</td><td>120</td><td>102</td><td>128</td></tr>
       		 
        <tr><td>62</td><td>124</td><td>106</td><td>132</td></tr>
       		  		  		  		  		  		</tbody>
     </table>

<h2>Размеры мужские</h2>	  
 <table> 		 
      <tbody> 
        <tr><th>Размер российский</th><th>46</th><th>48</th><th>50</th><th>52</th><th>54</th><th>56</th><th>58</th></tr>
        <tr><td>Размер международный</td><td>S</td><td>M</td><td>L</td><td>XL</td><td>XXL</td><td>3XL</td><td>4XL</td></tr>
        <tr><td>Рост</td><td>176-182</td><td>176-182</td><td>176-182</td><td>176-182</td><td>176-182</td><td>176-182</td><td>176-182</td></tr>
        <tr><td>Обхват груди</td><td>100</td><td>104</td><td>108</td><td>112</td><td>116</td><td>120</td><td>124</td></tr>
     </tbody>
  </table>

 
  <h2>Размеры детские</h2>	 

	<h3>Размерная таблица ВЕСНУШКИ (в см)  для определения плечевых изделий (жилеты, жакеты, джемпера)</h3>
	<table> 			 
		<tbody> 
			<tr><th>Возраст</th><th>Рост</th><th>Двойные значения</th><th>Российский размер</th><th>Обхват груди, см</th><th>Обхват талии, см</th><th>Обхват бёдер, см</th></tr>	
			<tr><td>2 года</td><td>92</td><td>86-92</td><td>26</td><td>52-54</td><td>50-52</td><td>56-58</td></tr>
			<tr><td>3 года</td><td>98</td><td>98-104</td><td>28</td><td>54-56</td><td>52</td><td>58-60</td></tr>
			<tr><td>4 года</td><td>104</td><td>104-110</td><td>28-30</td><td>54-56</td><td>52-54</td><td>60-62</td></tr>
			<tr><td>5 года</td><td>110</td><td>110-116</td><td>30</td><td>56-58</td><td>54</td><td>62-64</td></tr>
			<tr><td>6 года</td><td>116</td><td>116-122</td><td>30-32</td><td>58-60</td><td>54-56</td><td>64-66</td></tr>
			<tr><td>7 года</td><td>120</td><td>122-128</td><td>32</td><td>60-62</td><td>56-58</td><td>66-68</td></tr>
		</tbody>
	</table>
	
	
	<br/>
	<h3>Размерная таблица VAY KIDS (в см)  для определения плечевых изделий (жилеты, жакеты, джемпера)</h3>
	<table> 			 
		<tbody> 
			<tr><th>Размер</th><th>Рост</th><th>Обхват груди</th></tr>	
			<tr><td>30</td><td>122</td><td>60</td></tr>
			<tr><td>32</td><td>128</td><td>64</td></tr>
			<tr><td>34</td><td>134</td><td>68</td></tr>
			<tr><td>36</td><td>140</td><td>72</td></tr>
			<tr><td>38</td><td>146</td><td>76</td></tr>
			<tr><td>40</td><td>152</td><td>80</td></tr>
			<tr><td>42</td><td>158</td><td>84</td></tr>
		</tbody>
	</table>
	
	
	<br/>
	<h3>Размерная таблица школьных жилетов (см). Допустимое отклонение +/-2см</h3>
	<table class="children_table"> 			 
		<tbody> 
		
			<tr><th></th><th>Размер</th><th>Рост</th><th>Длина изделия</th><th>Ширина изделия</th></tr>	
			<tr><td rowspan="11" class="bord">Мальчики</td><td>30</td><td>122</td><td>44</td><td>32</td></tr>
			<tr><td>32</td><td>128</td><td>46</td><td>34</td></tr>
			<tr><td>34</td><td>134</td><td>48</td><td>36</td></tr>
			<tr><td>36</td><td>140</td><td>50</td><td>38</td></tr>
			<tr><td>38</td><td>146</td><td>52</td><td>40</td></tr>
			<tr><td>40</td><td>152</td><td>54</td><td>42</td></tr>
			<tr><td>42</td><td>158</td><td>56</td><td>44</td></tr>
			<tr><td>44</td><td>158-164</td><td>58</td><td>46</td></tr>
			<tr><td>46</td><td>158-164</td><td>62</td><td>48</td></tr>
			<tr><td>48</td><td>158-164</td><td>64</td><td>50</td></tr>
			<tr><td>50</td><td>170-175</td><td>66</td><td>52</td></tr>
			
			<tr><td colspan="5" style="background:#CCC;">&nbsp;</td></tr>
			<tr><td rowspan="11" class="bord">Девочки</td><td>30</td><td>122</td><td>46</td><td>30</td></tr>
			<tr><td>32</td><td>128</td><td>48</td><td>32</td></tr>
			<tr><td>34</td><td>134</td><td>50</td><td>34</td></tr>
			<tr><td>36</td><td>140</td><td>52</td><td>38</td></tr>
			<tr><td>38</td><td>146</td><td>54</td><td>40</td></tr>
			<tr><td>40</td><td>152</td><td>56</td><td>42</td></tr>
			<tr><td>42</td><td>158</td><td>58</td><td>44</td></tr>
			<tr><td>44</td><td>158-164</td><td>60</td><td>46</td></tr>
			<tr><td>46</td><td>158-164</td><td>60</td><td>48</td></tr>
			<tr><td>48</td><td>158-164</td><td>60</td><td>50</td></tr>
			<tr><td>50</td><td>170-175</td><td>60</td><td>50</td></tr>
			
		</tbody>
	</table>
</body>
</html>