<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Способы оплаты товаров | SOGREVAY.RU");
$APPLICATION->SetPageProperty("keywords", "способы оплаты порядок как оплатить sogrevay.ru");
$APPLICATION->SetPageProperty("description", "Как оплатить выбранный товар в нашем интернет магазине. Большой выбор одежды по низким ценам.");
$APPLICATION->SetTitle("Способы оплаты");
?><ol>
	<li>Наличными курьеру</li>
	<li>В пунктах самовывоза&nbsp;наличными, либо по карте, при наличии терминала в данном пункте*</li>
	<li>Оплата по квитанции в отделении Сбербанка&nbsp;</li>
	<li>Оплата через интернет-банк любого банка с использованием наших реквизитов&nbsp;</li>
	<li>Онлайн-платеж через личный кабинет пользователя:</li>
</ol>
<ul style="margin-left:30px;">
	<li>Пластиковая карта</li>
	<li>Yandex-деньги</li>
	<li>WebMoney</li>
</ul>
<br>
 &nbsp; &nbsp; &nbsp; Электронное письмо, содержащее индивидуальную ссылку на оплату, приходит после подтверждения заказа менеджером по телефону.<br>
 &nbsp; &nbsp;&nbsp;<br>
<p>
</p>
<p>
	 &nbsp; &nbsp; &nbsp;
</p>
<p>
	 &nbsp; &nbsp; &nbsp; * Самовывоз оплачивается только по факту получения заказа в ПВЗ, предоплата заказа невозможна.
</p>
<p>
 <br>
</p>
<p>
 <br>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>