<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Правила продажи товаров | SOGREVAY.RU");
$APPLICATION->SetPageProperty("keywords", "правила продажи большой выбор одежды sogrevay.ru");
$APPLICATION->SetPageProperty("description", "Условия и правила продажи нашего трикотажа. Большой выбор одежды с быстрой доставкой");
$APPLICATION->SetTitle("Правила продажи");
?><p align="center" style="text-align: justify;">
 <span style="line-height: 1.42857;">Правила продажи товаров в&nbsp;интер­нет-мага­зине <span style="color: #000000;"><span class="strong">SOGREVAY.RU</span></span>.<br>
 </span>
</p>
<p align="center" style="text-align: justify;">
 <span style="line-height: 1.42857;">1.&nbsp;&nbsp;&nbsp; Товары, раз­ме­щен­ные на&nbsp;сайте, пред­на­зна­чены для&nbsp;личного потреб­ле­ния.</span>
</p>
<p>
	 2.&nbsp;&nbsp;&nbsp; 
	<!--noindex--><a href="/upload/O-zaschite-prav-potrebiteley.pdf" rel="nofollow" target="_blank"><span style="color: #4b0048;"><span class="strong"><span style="color: #005824;">Закон о&nbsp;защите прав потребителей</span></span></span></a><!--/noindex--></p>
<p>
	 3.&nbsp;&nbsp;&nbsp; Граж­дан­ский кодекс Рос­сий­ской Феде­ра­ции ( 
	<!--noindex--><a href="/upload/GK-chast2.pdf" rel="nofollow" target="_blank"><span style="color: #4b0048;"><span class="strong"><span style="color: #005824;">Часть вторая</span></span></span></a><!--/noindex--><span style="color: #005824;">)</span>, подроб­ная инфор­ма­ция указана в&nbsp;статье 497&nbsp;Граж­дан­ского кодекса Рос­сий­ской Феде­ра­ции.
</p>
<p>
	 4.&nbsp;&nbsp;&nbsp; Поста­нов­ле­ние Пра­ви­тель­ства Рос­сий­ской Феде­ра­ции от&nbsp;27.09.2007 № 612&nbsp;« 
	<!--noindex--><a href="/upload/Pravila-prodazhi-distancionnym-sposobom-612.pdf" rel="nofollow" target="_blank"><span style="color: #4b0048;"><span class="strong"><span style="color: #005824;">Об утвер­жде­нии правил продажи товаров дистан­ци­он­ным спо­со­бом</span></span></span></a><!--/noindex-->».
</p>
<p>
	 5.&nbsp;&nbsp;&nbsp; « 
	<!--noindex--><a href="/upload/Perechen-neprodovolstvennyh-tovarov.pdf" rel="nofollow" target="_blank"><span style="color: #4b0048;"><span class="strong"><span style="color: #005824;">Пере­чень непро­до­воль­ствен­ных товаров над­ле­жа­щего каче­ства, не&nbsp;под­ле­жа­щих воз­врату или&nbsp;обмену на&nbsp;ана­ло­гич­ный товар других раз­ме­ра, формы, габа­ри­та, фасона, рас­цветки или&nbsp;ком­плек­та­ции</span></span></span></a><!--/noindex-->», утвер­жден­ный поста­нов­ле­нием Пра­ви­тель­ства РФ&nbsp;от 19.01.1998 № 55
</p>
<p>
	 6.&nbsp;&nbsp;&nbsp; Заказ фор­ми­ру­ется через интер­нет-сайт&nbsp;ООО «КУПИТРИКОТАЖ» (<a href="http://www.sogrevay.ru/"><span style="color: #4b0048;">sogrevay.ru</span></a>).
</p>
<p>
	 7.&nbsp;&nbsp;&nbsp; Потре­би­тель вправе опла­тить Товар налич­ными день­гами или&nbsp;путем без­на­лич­ных рас­че­тов. Оплата налич­ными осу­ществ­ля­ется курьеру или&nbsp;сотруд­нику пункта само­вы­воза в&nbsp;момент получения заказа. Платежные терминалы у курьеров не предусмотрены.
</p>
<p>
	 8.&nbsp;&nbsp;&nbsp; Без­на­лич­ная оплата может быть осу­ществ­лена путем бан­ков­ского пере­во­да, с&nbsp;исполь­зо­ва­нием бан­ков­ской карты, либо электронного кошелька в&nbsp;соот­вет­ствии с&nbsp;поряд­ком, ука­зан­ным на&nbsp;сайте&nbsp;<a href="/"><span style="color: #4b0048;">www.sogrevay.ru</span></a><span style="color: #4b0048;">&nbsp;</span>в&nbsp;разделе «<a href="/magazin/oplata/"><span class="strong"><span style="color: #005824;">Способы оплаты</span></span></a>». Расходы за&nbsp;пере­чис­ле­ние денеж­ных средств на&nbsp;счет Про­давца несет Поку­па­тель.
</p>
<p>
	 9.&nbsp;&nbsp;&nbsp; Потре­би­тель вправе само­сто­я­тельно полу­чить Товар в ПВЗ компании Boxberry, инфор­ма­ция о&nbsp;которых пред­став­лена в&nbsp;разделе «<a href="/magazin/samovyvoz/"><span class="strong"><span style="color: #045f20;">Самовывоз</span></span></a>» и на сайте Boxberry.ru в разделе «<a rel="nofollow" href="http://boxberry.ru/find_an_office/"><span class="strong"><span style="color: #045f20;">Найти свое отделение</span></span></a>»
</p>
<p>
	 10. &nbsp;Доставка Товара на&nbsp;тер­ри­то­рии России платная&nbsp;. Cтоимость доставки рассчитывается автоматически при оформлении заказа, в зависимости от способа и адреса доставки. Более подроб­ная инфор­ма­ция по стоимости доставки раз­ме­щена в&nbsp;разделе «<a href="/magazin/dostavka/"><span class="strong"><span style="color: #045f20;">Доставка</span></span></a>».&nbsp;
</p>
<p>
	 11. &nbsp;Стоимость почтового пересыла рассчитывается менеджером. В случае отправки Почтой требуется предоплата 100%.
</p>
<p>
	12. При общей сумме заказа от 7000 рублей, почтовая пересылка бесплатная. В случае, если Покупателем производится возврат товара, и общая сумма полученных товаров&nbsp;становится менее 7000 рублей, то с Покупателя удерживается стоимость почтовой пересылки в зависимости от веса и объявленной ценности посылки.
</p>
<p>
	 13. &nbsp;В случае, если возврат осу­ществ­ля­ется посред­ством вызова курьер­ской службы, Покупатель оплачивает стоимость курьерской доставки.
</p>
<p>
	 14. &nbsp;Подробные правила и условия возврата товаров указаны в разделе «<span class="strong"><a href="/magazin/vozvrat/"><span style="color: #045f20;">Возврат</span></a></span>»
</p>
<p>
	 15. &nbsp;Правила и&nbsp;условия эффек­тив­ного и&nbsp;без­опас­ного исполь­зо­ва­ния Товаров указаны на&nbsp;ярлыках Товара.
</p>
<div class="bx_page">
	<p>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>