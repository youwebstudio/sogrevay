<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Самовывоз заказанных товаров | SOGREVAY.RU");
$APPLICATION->SetPageProperty("keywords", "самовывоз товара информация порядок sogrevay.ru");
$APPLICATION->SetPageProperty("description", "Информация о самостоятельном получении товаров. Большой выбор одежды с быстрой доставкой.");
$APPLICATION->SetTitle("Самовывоз");
?><p style="text-align: justify;">
	 Самостоятельное получение заказов Покупателем осуществляется из пунктов выдачи заказов <b><span style="color: #005824;">Грастин</span></b> и <span style="color: #005824;"><b>Boxberry</b></span> (в дальнейшем "ПВЗ"), после подтверждения заказа и согласования конкретной даты самовывоза с менеджером по телефону. На данный момент доступно 22 пунктов Грастин и более 1000 пунктов Boxberry на территории РФ. Удобный для Вас пункт можно выбрать в Корзине при оформлении заказа.&nbsp;
</p>
<p style="text-align: justify;">
	 Часы работы необходимо уточнять индивидуально для каждого ПВЗ у Вашего менеджера, либо на официальном сайте Boxberry.ru в разделе 
	<!--noindex--><a href="http://boxberry.ru/find_an_office/" rel="nofollow" target="_blank"><span style="color: #005824;">«</span><b><span style="color: #005824;">Найти свое отделение</span></b><span style="color: #005824;">»</span></a><!--/noindex-->.
</p>
<p style="text-align: justify;">
	 Стоимость самовывоза рассчитывается автоматически при оформлении заказа,&nbsp; в зависимости от выбранного пункта.
</p>
<p style="text-align: justify;">
	 Предоплата заказов, планируемых к получению в ПВЗ, невозможна! Оплата в пунктах производится наличными, либо пластиковой картой Visa,&nbsp; MasterCard, и только на месте, по факту получения заказа.
</p>
<p style="text-align: justify;">
	 Ограничение по количеству вещей &nbsp;- <b>3 штуки на одну дату</b>.
</p>
<p style="text-align: justify;">
	 Хранение товара осуществляется в течение 7 календарных дней с момента поступления его в ПВЗ. Затем товар возвращается на склад интернет-магазина.
</p>
<p style="text-align: justify;">
	 Вы можете вскрыть посылку и тщательно проверить комплектность заказа в присутствии сотрудника пункта выдачи заказов. Дополнительные консультации по товару сотрудниками ПВЗ не производятся.
</p>
<p style="text-align: justify;">
	 Во большинстве пунктов предусмотрена возможность полного, либо частичного отказа от заказа при самовывозе, в случае полного отказа &nbsp;все равно необходимо оплатить стоимость услуги самовывоза.
</p>
<p style="text-align: justify;">
	 Денежные средства, внесенные через кассу пункта выдачи заказов, могут быть возвращены только непосредственно интернет-магазином Sogrevay.ru.&nbsp;С более подробной информацией Вы можете ознакомиться в разделе <a href="http://sogrevay.ru/magazin/vozvrat/"><span style="color: #005824;">«</span><b><span style="color: #005824;">Возврат</span></b><span style="color: #005824;">»</span></a>.
</p>
<p style="text-align: justify;">
	 Интернет-магазин несет ответственность за соответствие товара и заявленных свойств Вашему заказу.
</p>
<p style="text-align: justify;">
	 Все претензии по качеству товара принимаются менеджерами интернет-магазина <b><a href="http://sogrevay.ru"><span style="color: #005824;">Sogrevay.ru</span></a></b> по телефону и электронной почте <script type="text/javascript">eval(unescape('%64%6f%63%75%6d%65%6e%74%2e%77%72%69%74%65%28%27%3c%61%20%68%72%65%66%3d%22%6d%61%69%6c%74%6f%3a%69%6e%66%6f%40%73%6f%67%72%65%76%61%79%2e%72%75%22%3e%69%6e%66%6f%40%73%6f%67%72%65%76%61%79%2e%72%75%3c%2f%61%3e%27%29%3b'))</script>
</p>
<p style="text-align: justify;">
 <br>
</p>
<p style="text-align: justify;">
 <b>Адреса пунктов самовывоза:</b>
</p>
<p style="text-align: justify;">
	 <?$APPLICATION->IncludeComponent(
	"inmark:deliveryMap", 
	".default", 
	array(
		"CACHE_TIME" => "600",
		"CACHE_TYPE" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"MAP_CENTER" => "60.55, 76,34",
		"MAP_HEIGHT" => "400px",
		"MAP_WIDTH" => "100%",
		"MAP_ZOOM" => "3"
	),
	false
);?><br>
</p>
<p style="text-align: justify;">
 <br>
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>