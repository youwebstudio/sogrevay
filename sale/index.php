<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Распродажа");
?>

<?
$PAGE_ELEMENT_COUNT = 20;							// default
$ELEMENT_SORT_FIELD = "PROPERTY_DAY_THING";			// default
$ELEMENT_SORT_ORDER = "DESC";						// default
$ELEMENT_SORT_FIELD2 = "PROPERTY_SMART_SORT";		// default
$ELEMENT_SORT_ORDER2 = "DESC";						// default

if($_GET["sort"] == "price_asc") 		{$ELEMENT_SORT_FIELD = "PROPERTY_MIN_PRICE"; 		$ELEMENT_SORT_ORDER = "ASC";}
if($_GET["sort"] == "price_desc") 		{$ELEMENT_SORT_FIELD = "PROPERTY_MIN_PRICE"; 		$ELEMENT_SORT_ORDER = "DESC";}
if($_GET["sort"] == "artikul_asc") 		{$ELEMENT_SORT_FIELD = "PROPERTY_CML2_ARTICLE"; 	$ELEMENT_SORT_ORDER = "ASC";}
if($_GET["sort"] == "artikul_desc") 	{$ELEMENT_SORT_FIELD = "PROPERTY_CML2_ARTICLE"; 	$ELEMENT_SORT_ORDER = "DESC";}
if($_GET["count"] == 40) 				{$PAGE_ELEMENT_COUNT = 40;}
if($_GET["count"] == 60) 				{$PAGE_ELEMENT_COUNT = 60;}
?>


<?
$arID = Array();
$arFil = Array(
	"IBLOCK_ID" => 4, 
	"ACTIVE" => "Y", 
	Array(
		"PROPERTY_NOVIZNA" => 22,
		"PROPERTY_NAZVANIE_AKTSII" => Array(5013, 5014),
		"LOGIC" => "OR"
	)
);
$dbRes = CIBlockElement::GetList(Array(), $arFil, false, false, Array("ID"));
while($arRes = $dbRes->Fetch()) $arID[] = $arRes["ID"];
if(count($arID)<1) $arID = Array(-1);
$GLOBALS["arrFilter"] = Array("ID" => $arID);
?>



<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
		<div class="div-sort">
			<span class="sort-title">Сортировать:</span>
			<span class="sort-var">по цене</span>
			<a title="По возрастанию цены"<?if(($ELEMENT_SORT_FIELD == "PROPERTY_MIN_PRICE")&&($ELEMENT_SORT_ORDER == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
			<a title="По убыванию цены"<?if(($ELEMENT_SORT_FIELD == "PROPERTY_MIN_PRICE")&&($ELEMENT_SORT_ORDER == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=price_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a>
			<span class="sort-var">по артикулу</span>
			<a title="По возрастанию артикула"<?if(($ELEMENT_SORT_FIELD == "PROPERTY_CML2_ARTICLE")&&($ELEMENT_SORT_ORDER == "ASC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_asc", array("sort"))?>"><i class="fa fa-caret-up"></i></a> 
			<a title="По убыванию артикула"<?if(($ELEMENT_SORT_FIELD == "PROPERTY_CML2_ARTICLE")&&($ELEMENT_SORT_ORDER == "DESC")):?> class="active"<?endif?> href="<?=$APPLICATION->GetCurPageParam("sort=artikul_desc", array("sort"))?>"><i class="fa fa-caret-down"></i></a> 
		</div>
	</div>
</div>



<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"board1", 
	array(
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => "4",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => $ELEMENT_SORT_FIELD,
		"ELEMENT_SORT_ORDER" => $ELEMENT_SORT_ORDER,
		"ELEMENT_SORT_FIELD2" => $ELEMENT_SORT_FIELD2,
		"ELEMENT_SORT_ORDER2" => $ELEMENT_SORT_ORDER2,
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => $PAGE_ELEMENT_COUNT,
		"LINE_ELEMENT_COUNT" => "4",
		"PROPERTY_CODE" => array(
			0 => "BLOG_POST_ID",
			1 => "MORE_PHOTO",
			2 => "NOVIZNA",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "TONE",
			1 => "SIZE",
			2 => "COLOR",
			3 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "0",
		"SECTION_URL" => "/#SECTION_CODE_PATH#/",
		"DETAIL_URL" => "/#SECTION_CODE_PATH#/#ELEMENT_CODE#.html",
		"BASKET_URL" => "/personal/korzina/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "600",
		"CACHE_GROUPS" => "N",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"CACHE_FILTER" => "Y",
		"PRICE_CODE" => array(
			0 => "интернет-магазин",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "TONE",
			1 => "SIZE",
		),
		"PAGER_TEMPLATE" => "vay",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "board1",
		"BACKGROUND_IMAGE" => "-",
		"TEMPLATE_THEME" => "black",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "NOVIZNA",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR",
			1 => "SIZE",
		),
		"PRODUCT_SUBSCRIPTION" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_CLOSE_POPUP" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"SEF_RULE" => "",
		"SECTION_CODE_PATH" => ""
	),
	false
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>